#!/usr/bin/python


""" imgcnv building framework
"""

__author__    = "Dmitry Fedorov"
__version__   = "2.5"
__copyright__ = "Center for BioImage Informatics, University California, Santa Barbara"

import sys
import os
from copy import deepcopy
from subprocess import Popen, call, PIPE
import time
import urllib
import posixpath
import tarfile
import zipfile

# TODO:
#  add fetching libtiff from our location: https://gitlab.com/viqi/libtiff
#

###############################################################
# misc
###############################################################

def run_command(command, cwd=None):
    try:
        p = Popen (command, stdout=PIPE, stderr=PIPE, cwd=cwd)
        o,e = p.communicate()
        if p.returncode!=0:
            log.info ("BAD non-0 return code for %s", command)
            return None
        return o or e
    except OSError:
        print ('Command not found [%s]', command[0])
    except Exception:
        print ('Exception during execution [%s]', command )
    return None

def fetch_git(url, foldername):
    repos_name = url.split('/')[-1].replace('.git', '')
    if os.path.exists(os.path.join(foldername, repos_name)):
        print 'Already downloaded %s'%url
        return
    print 'Cloning %s'%url
    command = ['git', 'clone', url]
    run_command(command, cwd=foldername)

def fetch_hg(url, foldername):
    repos_name = url.split('/')[-1]
    if os.path.exists(os.path.join(foldername, repos_name)):
        print 'Already downloaded %s'%url
        return
    print 'Cloning %s'%url
    command = ['hg', 'clone', url]
    run_command(command, cwd=foldername)


#**************************************************************
# download deps
#**************************************************************

if __name__ == "__main__":
    if len(sys.argv) > 1:
        fetch_git(sys.argv[1], 'libsrc')
    else:
        #fetch_git('https://gitlab.com/viqi_public/bioimageconvert/libtiff.git', 'libsrc')
        fetch_git('https://github.com/dimin/libCZI.git', 'libsrc')
        fetch_git('https://bitbucket.org/bioimageconvert/libhdf5.git', 'libsrc')
        fetch_git('https://gitlab.com/viqi_public/bioimageconvert/openslide.git', 'libsrc')
    sys.exit(0)
