#!/usr/bin/python

# The following command line paratemeters can be used in any combination:
# all     - execute all tests
# reading - execute reading tests
# writing - execute writing tests
# meta    - execute metadata reading tests
# video   - execute video reading and metadata tests

""" imgcnv testing framework
"""
from __future__ import print_function

from builtins import str
from builtins import range
from past.builtins import basestring
from builtins import object
__author__    = "Dmitry Fedorov"
__version__   = "2.9.5"
__copyright__ = "ViQi Inc; Center for BioImage Informatics, University California, Santa Barbara"

import sys
import os
from copy import deepcopy
from subprocess import Popen, call, PIPE
import time
import urllib.request, urllib.parse, urllib.error
import posixpath
import datetime
import pytz
from dateutil.parser import parse as date_parse
import collections
from distutils.util import strtobool

IMGCNV = os.getenv('BIM_TEST_PATH_TO_IMGCNV', './imgcnv')
if os.name == 'nt':
    IMGCNV = os.getenv('BIM_TEST_PATH_TO_IMGCNV', './imgcnv.exe')
IMGCNVVER = __version__
url_image_store = os.getenv('BIM_TEST_IMAGESTORE_DOWNLOAD_URL', 'https://s3-us-west-2.amazonaws.com/viqi-test-images/')
local_store_images  = os.getenv('BIM_TEST_PATH_TO_LOCAL_IMAGESTORE', 'images')
local_store_tests   = os.getenv('BIM_TEST_PATH_TO_LOCAL_TEST_FILES', 'tests')
offline_mode        = bool(strtobool(os.getenv('BIM_TEST_OFFLINE_MODE', 'False')))

if not os.path.exists(IMGCNV):
    print ('!!! Could not find imgcnv at: "%s" !!!'%IMGCNV)
    sys.exit (1)
if offline_mode and not os.path.exists(local_store_images):
    print ('!!! Could not find the local image store at: "%s" !!!'%local_store_images)
    sys.exit (1)


failed = 0
passed = 0
results = []


###############################################################
# misc
###############################################################
def version ():
    imgcnvver = Popen ([IMGCNV, '-v'],stdout=PIPE).communicate()[0]
    for line in imgcnvver.splitlines():
        line = safedecode(line)
        if not line or line.startswith('Input'): return False
        return line.replace('\n', '')

def check_version ( needed ):
    inst = version()
    if not inst:
        raise Exception('imgcnv was not found')

    inst_ver = inst.split('.')
    need_ver = needed.split('.')
    if int(inst_ver[0])<int(need_ver[0]) or int(inst_ver[0])==int(need_ver[0]) and int(inst_ver[1])<int(need_ver[1]):
        raise Exception('Imgcnv needs update! Has: '+inst+' Needs: '+needed)

def safedecode(s, encoding='utf-8'):
    if isinstance(s, str) is True:
        return s
    try:
        return s.decode(encoding)
    except UnicodeDecodeError:
        try:
            return s.decode('utf-8')
        except UnicodeDecodeError:
            try:
                return s.decode('latin-1')
            except UnicodeDecodeError:
                return s.decode('ascii', 'replace')

def safe_filename(fn):
    return fn.replace('/', '.').replace('\\', '.').replace(':', '.')

def parse_imgcnv_info(s):
    d = {}
    for l in s.splitlines():
        l = safedecode(l)
        kk = l.split(': ', 1)
        if len(kk)<2: continue
        k = kk[0]
        v = kk[1]
        k = safedecode(k, 'utf-8').replace('%3A', ':')
        v = safedecode(v, 'utf-8').replace('\n', '').replace('%3E', '>').replace('%3C', '<').replace('%3A', ':').replace('%22', '"').replace('%0A', '\n')
        d[k] = v
    return d

def print_failed(s, f='-', command=''):
    print ('X FAILED %s (%s)'%(s, str(command)))
    global failed
    global results
    failed += 1
    results.append( '%s: %s (%s)'%(f, s, str(command)) )

def print_passed(s):
    print ('PASSED %s'%(s))
    global passed
    passed += 1

def copy_keys(dict_in, keys_in):
    dict_out = {}
    for k in keys_in:
        if k in dict_in:
            dict_out[k] = dict_in[k]
    return dict_out
    #return { ?zip(k,v)? for k in keys_in if k in dict_in }

def fetch_file(filename):
    url = posixpath.join(url_image_store, urllib.parse.quote_plus( filename ) )
    path = os.path.join(local_store_images, filename)
    if not offline_mode:
        if not os.path.exists(path):
            print("{}...missing...downloading".format(path))
            urllib.request.urlretrieve(url, path)
        else:
            print("{}...exists locally".format(path))
    if not os.path.exists(path):
        print ('!!! Could not find required test image: "%s" !!!'%path)
    return path

def compare_datetime(dt1, dt2):
    try:
        tz = ('%+06.2f'%(time.timezone/-3600.0)).replace('.', '')

        if isinstance(dt1, (datetime.datetime, datetime.time, datetime.date)) is not True:
            dt1 = date_parse('%s%s'%(dt1, tz))

        if isinstance(dt2, (datetime.datetime, datetime.time, datetime.date)) is not True:
            dt2 = date_parse('%s%s'%(dt2, tz))

        dt1 = dt1.astimezone(tz=pytz.utc)
        dt2 = dt2.astimezone(tz=pytz.utc)
        return (dt1==dt2)
    except Exception:
        return False

def compare_iterables(v1, v2):
    #print v1
    #print v2
    if isinstance(v1, list) is not True:
        v1 = v1.split(',')
    if isinstance(v2, list) is not True:
        v2 = v2.split(',')
    #print v1
    #print v2
    if len(v1) != len(v2): return False
    for i in range(len(v1)):
        #print v1[i]
        #print v2[i]
        if compare_values(v1[i], v2[i]) is not True:
            return False
    return True

def compare_values(iv, tv):
    try:
        if isinstance(tv, int):
            return (int(iv)==tv)
        if isinstance(tv, (float)):
            return (float(iv)==tv)
        if isinstance(tv, (datetime.datetime, datetime.time, datetime.date)):
            return compare_datetime(iv, tv)
        if isinstance(tv, list):
            return compare_iterables(iv, tv)
    except:
        pass
    return (iv==tv)

###############################################################
# info comparisons
###############################################################

class InfoComparator(object):
    '''Compares two info dictionaries'''
    def compare(self, iv, tv):
        return False
    def fail(self, k, iv, tv, filename='', command='', inv=False):
        if inv is True:
            print_failed('%s "%s" failed comparison: (expected) [%s] [%s] (computed)'%(filename, k, iv, tv), command=command)
        print_failed('%s "%s" failed comparison: (computed) "%s" != "%s" (expected)'%(filename, k, iv, tv), command=command)

class InfoEquality(InfoComparator):

    def compare(self, iv, tv):
        return compare_values(iv, tv)

    def fail(self, k, iv, tv, filename='', command='', inv=False):
        if inv is True:
            print_failed('%s "%s" failed comparison: (expected) [%s] [%s] (computed)'%(filename, k, iv, tv), command=command)
        print_failed('%s "%s" failed comparison: (computed) "%s" != "%s" (expected)'%(filename, k, iv, tv), command=command)

class InfoNumericLessEqual(InfoComparator):
    def compare(self, iv, tv):
        return (int(iv)<=int(tv))
    def fail(self, k, iv, tv, filename='', command='', inv=False):
        if inv is True:
            print_failed('%s "%s" failed comparison: (expected) [%s] [%s] (computed)'%(filename, k, iv, tv), command=command)
        print_failed('%s "%s" failed comparison: (computed) "%s" != "%s" (expected)'%(filename, k, iv, tv), command=command)


def compare_info(info, test, cc=InfoEquality(), command='', inv=False ):
    filename = info.get('filename', '')
    success = True
    for tk in test:
        if tk not in info:
            print_failed('%s "%s" not found in info'%(filename, tk), command=command)
            success = False
        if not cc.compare(info.get(tk), test.get(tk)):
            cc.fail( tk, info.get(tk), test.get(tk), filename, command=command, inv=inv)
            success = False
    return success

def standardize_meta_fields(meta):
    if meta.get('pixel_resolution_unit_x') == 'um':
        meta['pixel_resolution_unit_x'] = 'microns'
    if meta.get('pixel_resolution_unit_y') == 'um':
        meta['pixel_resolution_unit_y'] = 'microns'
    return meta

###############################################################
# TESTS
###############################################################

def test_image_read( format, filename ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(format, filename))
    print('---------------------------------------')


    #------------------------------------------------------------------------
    # reading and converting into TIFF
    #------------------------------------------------------------------------
    out_name = '%s/_test_converting_%s.tif'%(local_store_tests, filename)
    thumb_name = '%s/_test_thumbnail_%s.jpg'%(local_store_tests, filename)
    out_fmt = 'tiff'
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_org = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_org)<=0 or 'width' not in info_org or int(info_org['width'])<1:
        print_failed('loading info', format, command=command)
        return
    else:
        print_passed('loading info')

    # convert the file into TIFF
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_cnv)<=0:
        print_failed('loading converted info', format, command=command)
        return
    else:
        print_passed('loading converted info')

    # test if converted file has same info
    info_test = copy_keys(info_cnv, ('pages', 'channels', 'width', 'height', 'depth'))
    if compare_info(info_org, info_test, command=command)==True:
        print_passed('geometry')

    #------------------------------------------------------------------------
    # Writing thumbnail
    #------------------------------------------------------------------------
    command = [IMGCNV, '-i', filename, '-o', thumb_name, '-t', 'jpeg', '-depth', '8,d', '-page', '1', '-display', '-resize', '128,128,BC,AR']
    r = Popen (command, stdout=PIPE).communicate()[0]

    command = [IMGCNV, '-i', thumb_name, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_thb = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_thb)<=0:
        print_failed('loading thumbnail info', format, command=command)
        return
    else:
        print_passed('loading thumbnail info')

    if compare_info(info_thb, {'pages':1, 'channels':3, 'depth':8}, command=command, inv=True )==True:
        if compare_info(info_thb, {'width':'128', 'height':'128'}, InfoNumericLessEqual(), command=command, inv=True)==True:
            print_passed('thumbnail geometry')

    print()


def test_image_write( format, filename ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(format, filename))
    print('---------------------------------------')

    out_name = '%s/_test_writing_%s.%s'%(local_store_tests, filename, format)
    out_fmt = format
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_org = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_org)<=0:
        print_failed('loading input info', format, command=command)
        return

    # convert the file into format
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_cnv)<=0:
        print_failed('loading written info', format, command=command)
        return
    else:
        print_passed('loading written info')

    # test if converted file has same info
    info_test = copy_keys(info_cnv, ('pages', 'channels', 'width', 'height', 'depth'))
    if compare_info(info_org, info_test, command=command)==True:
        print_passed('written geometry')

    print()


def test_video_write( format, filename, fps_test=15 ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(format, filename))
    print('---------------------------------------')

    out_name = '%s/_test_writing_%s.%s'%(local_store_tests, filename, format)
    out_fmt = format
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_org = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_org)<=0:
        print_failed('loading input info', format, command=command)
        return

    # convert the file into format
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt, '-depth', '8,d,u', '-options', 'fps 15']
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-meta']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_cnv)<=0:
        print_failed('loading written info', format, command=command)
        return
    else:
        print_passed('loading written meta')

    # test if converted file has same info
    info_org['video_frames_per_second'] = str(fps_test)
    info_test = copy_keys(info_cnv, ('image_num_x', 'image_num_y', 'video_frames_per_second'))
    if compare_info(info_org, info_test, command=command)==True:
        print_passed('written geometry')

    print()


def test_image_metadata( format, filename, meta_test, meta_test_cnv=None  ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(format, filename))
    print('---------------------------------------')

    out_name = '%s/_test_metadata_%s.ome.tif'%(local_store_tests, filename)
    out_fmt = 'ome-tiff'
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_org = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(meta_org)<=0:
        print_failed('loading metadata', format, command=command)
        return

    #print str(meta_org)

    # test if converted file has same info
    if compare_info(meta_org, meta_test, command=command)==True:
        print_passed('reading metadata')

    # convert the file into format
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(meta_cnv)<=0:
        print_failed('loading written metadata', format, command=command)
        return
    else:
        print_passed('loading written metadata')

    #print str(meta_cnv)
    if meta_test_cnv is None: meta_test_cnv=meta_test
    meta_test_cnv = standardize_meta_fields(meta_test_cnv)
    if compare_info(meta_cnv, meta_test_cnv, command=command)==True:
        print_passed('writing metadata')

    print()

def test_metadata_read( format, filename, meta_test, extra=None ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(format, filename))
    print('---------------------------------------')

    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta']
    if extra is not None:
        command.extend(extra)
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_org = parse_imgcnv_info(r)
    r = safedecode(r)
    meta_org['filename'] = filename

    if r is None or r.startswith('Input format is not supported') or len(meta_org)<=0:
        print_failed('reading meta-data', format, command=command)
        return

    # test if converted file has same info
    if compare_info(meta_org, meta_test, command=command)==True:
        print_passed('reading meta-data')

    print()

def test_image_video( format, filename, meta_test  ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(format, filename))
    print('---------------------------------------')

    out_name = '%s/_test_metadata_%s.ome.tif'%(local_store_tests, filename)
    out_fmt = 'ome-tiff'
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_org = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(meta_org)<=0:
        print_failed('reading video', format, command=command)
        return

    #print str(meta_org)

    # test if converted file has same info
    if compare_info(meta_org, meta_test, command=command)==True:
        print_passed('reading video info')

    print()

def test_image_transforms( transform_type, transform, filename, meta_test  ):

    print()
    print('---------------------------------------')
    print('%s - %s - %s'%(transform_type, transform, filename))
    print('---------------------------------------')

    format = 'tiff'
    out_name = '%s/_test_transform_%s%s_%s.%s'%(local_store_tests, filename, transform_type, transform, format)
    out_fmt = format
    filename = '%s/%s'%(local_store_images, filename)

    # convert the file into transform
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt, transform_type, transform]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_cnv)<=0:
        print_failed('loading written info for', transform, command=command)
        return

    # test if converted file has same info
    if compare_info(info_cnv, meta_test, command=command)==True:
        print_passed('reading transformed info')

    print()


def test_image_commands( extra, filename, meta_test  ):

    print()
    print('---------------------------------------')
    print('%s - %s'%(extra, filename))
    print('---------------------------------------')

    extra = [str(i) for i in extra]
    format = 'tiff'

    out_name = '%s/_test_command_%s_%s.%s'%(local_store_tests, filename, safe_filename('_'.join(extra)), format)
    out_fmt = format
    filename = '%s/%s'%(local_store_images, filename)

    # convert the file into transform
    command = [IMGCNV, '-i', filename, '-o', out_name]
    if '-t' not in extra:
        command.extend(['-t', out_fmt])
    command.extend(extra)
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command2 = [IMGCNV, '-i', out_name, '-meta-parsed']
    r = Popen (command2, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    if r is None or r.startswith('Input format is not supported') or len(info_cnv)<=0:
        print_failed('loading written info for ', extra, command=command)
        return

    # test if converted file has same info
    if compare_info(info_cnv, meta_test, command=command)==True:
        print_passed('reading command info')

    print()


###############################################################
# run tests
###############################################################

check_version ( IMGCNVVER )

if not os.path.exists(local_store_images):
    os.mkdir(local_store_images)
if not os.path.exists(local_store_tests):
    os.mkdir(local_store_tests)

mode = sys.argv
if len(mode) <= 1: mode.append('all')

#**************************************************************
# ensure test files are present
#**************************************************************

fetch_file('0022.zvi')
fetch_file('040130Topography001.tif')
fetch_file('107_07661.bmp')
fetch_file('112811B_5.oib')
fetch_file('122906_3Ax(inverted).avi')
fetch_file('161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
fetch_file('23D3HA-cy3 psd-gfp-488 Homer-647 DIV 14 - 3.zvi')
fetch_file('241aligned.avi')
fetch_file('3Dstack.tif.3D.mov')
fetch_file('7.5tax10.stk')
fetch_file('A01.jpg')
fetch_file('autocorrelation.tif')
fetch_file('AXONEME.002')
fetch_file('B01C0201.M2T')
fetch_file('B4nf.RS.RE.z1.5.15.06.3DAnimation.avi')
fetch_file('bigtiff.ome.btf')
fetch_file('cells.ome.tif')
fetch_file('combinedsubtractions.lsm')
fetch_file('CRW_0136.CRW')
fetch_file('CRW_0136_COMPR.dng')
fetch_file('CSR 4 mo COX g M cone r PNA b z1.oib')
fetch_file('DSCN0041.NEF')
fetch_file('EleanorRigby.mpg')
fetch_file('flowers_24bit_hsv.tif')
fetch_file('flowers_24bit_nointr.png')
fetch_file('flowers_8bit_gray.png')
fetch_file('flowers_8bit_palette.png')
fetch_file('Girsh_path3.m2ts')
fetch_file('HEK293_Triple_Dish1_set_9.lsm')
fetch_file('IMG_0040.CR2')
fetch_file('IMG_0184.JPG')
fetch_file('IMG_0488.JPG')
fetch_file('IMG_0562.JPG')
fetch_file('IMG_0593.JPG')
fetch_file('IMG_1003.JPG')
fetch_file('K560-tax-6-7-7-1.stk')
fetch_file('Live_10-3-2009_6-44-12_PM.tif')
fetch_file('MB_10X_20100303.oib')
fetch_file('MDD2-7.stk')
fetch_file('MF Mon 2x2.tif')
fetch_file('Muller cell z4.oib.3D.mov')
fetch_file('MZ2.PIC')
fetch_file('out.avi')
fetch_file('out.flv')
fetch_file('out.m4v')
fetch_file('out.mjpg')
fetch_file('out.mkv')
fetch_file('out.mov')
fetch_file('out.mpg')
fetch_file('out.ogg')
fetch_file('out.swf')
fetch_file('out.vcd')
fetch_file('out.webm')
fetch_file('out.wmv')
fetch_file('out_h264.mp4')
fetch_file('out_h265.mp4')
fetch_file('P1110010.ORF')
fetch_file('PENTAX_IMGP1618.JPG')
fetch_file('PICT1694.MRW')
fetch_file('radiolaria.avi')
fetch_file('Retina 4 top.oib')
fetch_file('Step_into_Liquid_1080.wmv')
fetch_file('sxn3_w1RGB-488nm_s1.stk')
fetch_file('test z1024 Image0004.oib')
fetch_file('test.ogv')
fetch_file('test16bit.btf')
fetch_file('tubule20000.ibw')
fetch_file('Untitled_MMImages_Pos0.ome.tif')
fetch_file('wta.ome.tif')
fetch_file('monument_imgcnv.256.tif')
fetch_file('monument_imgcnv.ome.tif')
fetch_file('monument_imgcnv_subdirs.tif')
fetch_file('monument_imgcnv_topdirs.tif')
fetch_file('monument_photoshop.tif')
fetch_file('monument_vips.tif')
fetch_file('10')
fetch_file('MR-MONO2-8-16x-heart')
fetch_file('US-MONO2-8-8x-execho')
fetch_file('US-PAL-8-10x-echo')
fetch_file('0015.DCM')
fetch_file('0020.DCM')
fetch_file('ADNI_002_S_0295_MR_3-plane_localizer__br_raw_20060418193538653_1_S13402_I13712.dcm')
fetch_file('BetSog_20040312_Goebel_C2-0001-0001-0001.dcm')
fetch_file('IM-0001-0001.dcm')
fetch_file('test4.tif')

fetch_file('16_day1_1_patient_29C93FK6_1.nii')
fetch_file('filtered_func_data.nii')
fetch_file('newsirp_final_XML.nii')
fetch_file('avg152T1_LR_nifti.hdr')
fetch_file('avg152T1_LR_nifti.img')
fetch_file('T1w.nii.gz')
fetch_file('219j_q050.jxr')
fetch_file('219j_q100.jxr')
fetch_file('219j_q080.webp')
fetch_file('219j_q100.webp')
fetch_file('219j.jp2')

fetch_file('IMG_1913_16bit_prophoto_ts1024_q90.jp2')
fetch_file('retina.jp2')
fetch_file('IMG_1913_16bit_prophoto_q90.jxr')
fetch_file('IMG_1913_prophoto_q90.webp')

fetch_file('6J0A3548.CR2')
fetch_file('IMG_0184_RGBA.png')

fetch_file('20150917_05195_DNA-TET-25k-DE20_raw.region_000.sum-all_003-072.mrc')
fetch_file('29kx_30epi_058_aligned.mrc')
fetch_file('golgi.mrc')
fetch_file('Tile_19491580_0_1.mrc')
fetch_file('dual.rec')
fetch_file('EMD-3001.map')
fetch_file('EMD-3197.map')

# pyramids
fetch_file('17.czi')
fetch_file('MultiResolution-Mosaic.czi')
fetch_file('CMU-1.svs')
fetch_file('CMU-1.ndpi')
fetch_file('2017SM09941_3_EVG.tiff')

#stacks
fetch_file('16Bit-ZStack.czi')
fetch_file('40x_RatBrain-AT-2ch-Z-wf.czi')
fetch_file('8Bit-ZStack.czi')
fetch_file('BPAE-cells-bin2x2_3chTZ(WF).czi')
fetch_file('CZT-Stack-Anno.czi')
fetch_file('Mouse_stomach_20x_ROI_3chZTiles(WF).czi')
fetch_file('Z-Stack.czi')
fetch_file('Z-Stack-Anno(RGB).czi')

# hdf5, Dream3d, Imaris, VQI
fetch_file('ex_image2.h5')
fetch_file('ex_image3.h5')
fetch_file('NEONDSImagingSpectrometerData.h5')
fetch_file('murks_lens.h5')
fetch_file('2016-07-26_17.11.03_Sample Prefix_5P298C2_Protocol.ims')
fetch_file('R18Demo.ims')
fetch_file('retina_large.ims')
fetch_file('sparse_dense.VQI.h5')

fetch_file('old_cells_02.nd2')
fetch_file('sample_image.nd2')

#MD HTD
fetch_file('MD_HTD_OLD_w1.TIF')
fetch_file('MD_HTD_XML_w1.TIF')


#**************************************************************
# run tests
#**************************************************************

start = time.time()

if 'all' in mode or 'reading' in mode:
    print('\n')
    print('***************************************************')
    print('"reading" - Reading formats, converting and creating thumbnails')
    print('***************************************************')

    test_image_read( "BMP", "107_07661.bmp" )
    test_image_read( "JPEG", "A01.jpg" )
    test_image_read( "PNG", "flowers_24bit_nointr.png" )
    test_image_read( "PNG", "flowers_8bit_gray.png" )
    test_image_read( "PNG", "flowers_8bit_palette.png" )
    test_image_read( "BIORAD-PIC", "MZ2.PIC" )
    test_image_read( "FLUOVIEW", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_image_read( "Zeiss LSM", "combinedsubtractions.lsm" )
    test_image_read( "Zeiss LSM 1 ch", "HEK293_Triple_Dish1_set_9.lsm" )
    test_image_read( "OME-TIFF", "wta.ome.tif" )
    test_image_read( "TIFF float", "autocorrelation.tif" )
    test_image_read( "STK", "K560-tax-6-7-7-1.stk" )
    test_image_read( "STK", "MDD2-7.stk" )
    test_image_read( "STK", "sxn3_w1RGB-488nm_s1.stk" )
    test_image_read( "STK", "7.5tax10.stk" )
    test_image_read( "OIB", "test z1024 Image0004.oib" )
    test_image_read( "OIB", "MB_10X_20100303.oib" )
    test_image_read( "NANOSCOPE", "AXONEME.002" )
    test_image_read( "IBW", "tubule20000.ibw" )
    test_image_read( "PSIA", "040130Topography001.tif" )
    test_image_read( "BigTIFF", "test16bit.btf" )
    test_image_read( "OME-BigTIFF", "bigtiff.ome.btf" )
    test_image_read( "ZVI", "23D3HA-cy3 psd-gfp-488 Homer-647 DIV 14 - 3.zvi" )
    test_image_read( "ZVI", "0022.zvi" )
    test_image_read( "Adobe DNG", "CRW_0136_COMPR.dng" )

    if not 'no_jpegxr' in mode:
        test_image_read( "JPEG-XR", "219j_q050.jxr" )
        test_image_read( "JPEG-XR LOSSLESS", "219j_q100.jxr" )

    test_image_read( "JPEG-2000", "219j.jp2" )

    if not 'no_webp' in mode:
        test_image_read( "WEBP", "219j_q080.webp" )
        test_image_read( "WEBP LOSSLESS", "219j_q100.webp" )

    # DICOM
    if not 'no_dicom' in mode:
        test_image_read( "DICOM", "10" )
        test_image_read( "DICOM", "0015.DCM" )
        test_image_read( "DICOM", "IM-0001-0001.dcm" )

    # NIFTI
    if not 'no_nifti' in mode:
        test_image_read( "NIFTI", "16_day1_1_patient_29C93FK6_1.nii" )
        test_image_read( "NIFTI", "filtered_func_data.nii" )
        test_image_read( "NIFTI", "newsirp_final_XML.nii" )
        test_image_read( "NIFTI", "avg152T1_LR_nifti.hdr" )
        test_image_read( "NIFTI", "T1w.nii.gz" )

    # video formats
    if not 'no_ffmpeg' in mode:
        test_image_read( "QuickTime", "3Dstack.tif.3D.mov" )
        test_image_read( "AVI", "radiolaria.avi" )
        test_image_read( "OGG", "test.ogv" )
        test_image_read( "FLV", "out.flv" )
        test_image_read( "WMV", "out.wmv" )
        test_image_read( "VCD", "out.vcd" )
        test_image_read( "MJPEG", "out.mjpg" )
        test_image_read( "Matroska", "out.mkv" )
        #test_image_read( "MPEG1", "EleanorRigby.mpg" )
        test_image_read( "MPEG4", "out.m4v" )
        test_image_read( "H264", "out_h264.mp4" )
        test_image_read( "H265", "out_h265.mp4" )
        test_image_read( "WebM", "out.webm" )

    # MRC
    test_image_read( "MRC", "Tile_19491580_0_1.mrc" )
    test_image_read( "MRC", "golgi.mrc" )

    # HDF5,Imaris
    if not 'no_hdf5' in mode:
        test_image_read( "HDF5", "ex_image2.h5" )
        test_image_read( "HDF5", "ex_image3.h5" )
        test_image_read( "Dream3D", "murks_lens.h5" )
        test_image_read( "Imaris", "R18Demo.ims" )


if 'all' in mode or 'writing' in mode:
    print('\n')
    print('***************************************************')
    print('"writing" - Writing formats')
    print('***************************************************')

    test_image_write( "JPEG", "flowers_24bit_nointr.png" )
    test_image_write( "PNG", "flowers_24bit_nointr.png" )
    test_image_write( "BMP", "flowers_24bit_nointr.png" )
    test_image_write( "TIFF", "flowers_24bit_nointr.png" )
    test_image_write( "OME-TIFF", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_image_write( "BigTIFF", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_image_write( "OME-BigTIFF", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )

    if not 'no_jpegxr' in mode:
        test_image_write( "JXR", "flowers_24bit_nointr.png" )

    if not 'no_webp' in mode:
        test_image_write( "WEBP", "flowers_24bit_nointr.png" )

    test_image_write( "JP2", "flowers_24bit_nointr.png" )


if not 'no_ffmpeg' in mode and ('all' in mode or 'writingvideo' in mode):
    print('\n')
    print('***************************************************')
    print('"writingvideo" - Writing video')
    print('***************************************************')

    test_video_write( "AVI", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "QuickTime", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "MPEG4", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "WMV", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "OGG", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "FLV", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "H264", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )
    test_video_write( "H265", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )

    if os.name == 'nt': #dima: reported as 120 but plays as proper 15
        test_video_write( "Matroska", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=120 ) #dima: reported as 120 but plays as proper 15
        test_video_write( "WEBM", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=120 ) #dima: reported as 120 but plays as proper 15
        test_video_write( "WEBM9", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=120 ) #dima: reported as 120 but plays as proper 15
    else:
        test_video_write( "Matroska", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=120)
        test_video_write( "WEBM", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=120)
        test_video_write( "WEBM9", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=120)


if 'all' in mode or 'meta' in mode:
    print('\n')
    print('***************************************************')
    print('"meta" - Reading and converting metadata')
    print('***************************************************')

    meta_test = {}
    meta_test['image_num_z'] = 6
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.192406
    meta_test['pixel_resolution_y'] = 0.192406
    meta_test['pixel_resolution_z'] = 1.185
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    test_image_metadata( "PIC", "MZ2.PIC", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 13
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.20716
    meta_test['pixel_resolution_y'] = 0.20716
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['channels/channel_00000/name'] = 'FITC'
    meta_test['channels/channel_00001/name'] = 'Cy3'
    # the following fields will not be tested for conversion into OME-TIFF
    meta_test_full = deepcopy(meta_test)
    meta_test_full['objectives/objective:0/name'] = 'UPLAPO 40XO'
    meta_test_full['objectives/objective:0/magnification'] = 40
    test_image_metadata( "TIFF Fluoview", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", meta_test_full, meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 30
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.138661
    meta_test['pixel_resolution_y'] = 0.138661
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    # the following fields will not be tested for conversion into OME-TIFF
    meta_test_full = deepcopy(meta_test)
    meta_test_full['objectives/objective:0/name'] = 'Achroplan 63x/0.95 W'
    meta_test_full['objectives/objective:0/magnification'] = 63.0
    meta_test_full['objectives/objective:0/numerical_aperture'] = 0.95
    meta_test_full['channels/channel_00000/color'] = '0,255,0'
    meta_test_full['channels/channel_00000/excitation_wavelength'] = 514.0
    meta_test_full['channels/channel_00000/pinhole_radius'] = 185.0
    meta_test_full['channels/channel_00000/name'] = 'Ch3-T3'
    meta_test_full['channels/channel_00000/power'] = 38.8
    meta_test_full['channels/channel_00001/color'] = '255,0,0'
    meta_test_full['channels/channel_00001/excitation_wavelength'] = 458.0
    meta_test_full['channels/channel_00001/pinhole_radius'] = 255.5
    meta_test_full['channels/channel_00001/name'] = 'Ch2-T4'
    meta_test_full['channels/channel_00001/power'] = 63.5
    test_image_metadata( "TIFF Zeiss LSM", "combinedsubtractions.lsm", meta_test_full, meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 152
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.124
    meta_test['pixel_resolution_y'] = 0.124
    meta_test['pixel_resolution_z'] = 0.35
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test_full['channels/channel_00000/name'] = 'Alexa Fluor 488 - 488nm'
    meta_test_full['channels/channel_00000/fluor'] = 'Alexa Fluor 488'
    meta_test_full['channels/channel_00000/modality'] = 'Epifluorescence'
    meta_test_full['channels/channel_00000/emission_wavelength'] = 520
    meta_test_full['channels/channel_00000/excitation_wavelength'] = 488
    meta_test_full['channels/channel_00001/name'] = 'Alexa Fluor 546 - 543nm'
    meta_test_full['channels/channel_00001/fluor'] = 'Alexa Fluor 546'
    meta_test_full['channels/channel_00001/modality'] = 'Epifluorescence'
    meta_test_full['channels/channel_00001/emission_wavelength'] = 572
    meta_test_full['channels/channel_00001/excitation_wavelength'] = 543
    meta_test_full['channels/channel_00002/name'] = 'DRAQ5 - 633nm'
    meta_test_full['channels/channel_00002/fluor'] = 'DRAQ5'
    meta_test_full['channels/channel_00002/modality'] = 'Epifluorescence'
    meta_test_full['channels/channel_00002/emission_wavelength'] = 683
    meta_test_full['channels/channel_00002/excitation_wavelength'] = 633

    # the following fields will not be tested for conversion into OME-TIFF
    meta_test_full = deepcopy(meta_test)
    meta_test_full['objectives/objective:0/name'] = 'UPLFLN    40X O  NA:1.30'
    meta_test_full['objectives/objective:0/magnification'] = 40
    meta_test_full['objectives/objective:0/numerical_aperture'] = 1.3
    test_image_metadata( "OME-TIFF", "wta.ome.tif", meta_test_full, meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 31
    meta_test['pixel_resolution_t'] = 4
    test_image_metadata( "STK", "K560-tax-6-7-7-1.stk", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 105
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.43
    meta_test['pixel_resolution_y'] = 0.43
    meta_test['pixel_resolution_z'] = 0.488
    meta_test['pixel_resolution_unit_x'] = 'um'
    meta_test['pixel_resolution_unit_y'] = 'um'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    # the following fields will not be tested for conversion into OME-TIFF
    meta_test_full = deepcopy(meta_test)
    #meta_test_full['stage_distance_z'] = 0.488 # converted these to full arrays, need to test them
    #meta_test_full['stage_position_x'] = 3712.2
    #meta_test_full['stage_position_y'] = -2970.34
    #meta_test_full['stage_position_z'] = 25.252
    test_image_metadata( "STK", "sxn3_w1RGB-488nm_s1.stk", meta_test_full, meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 16
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 2.48341
    meta_test['pixel_resolution_y'] = 2.48019
    meta_test['pixel_resolution_z'] = 0.9375
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['channel_0_name'] = 'Alexa Fluor 405'
    meta_test['channel_1_name'] = 'Cy3'
    test_image_metadata( "OIB", "MB_10X_20100303.oib", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.177539
    meta_test['pixel_resolution_y'] = 0.177539
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    test_image_metadata( "PSIA", "040130Topography001.tif", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    #meta_test['image_num_p'] = '2'
    meta_test['pixel_resolution_x'] = 0.0585938
    meta_test['pixel_resolution_y'] = 0.0585938
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    test_image_metadata( "NANOSCOPE", "AXONEME.002", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    #meta_test['image_num_p'] = 3
    test_image_metadata( "IBW", "tubule20000.ibw", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 13
    meta_test['image_num_t'] = 1
    meta_test['pixel_resolution_x'] = 0.20716
    meta_test['pixel_resolution_y'] = 0.20716
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['channel_0_name'] = 'FITC'
    meta_test['channel_1_name'] = 'Cy3'
    test_image_metadata( "OME-BigTIFF", "bigtiff.ome.btf", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_x'] = 256
    meta_test['image_num_y'] = 256
    meta_test['image_num_c'] = 1
    meta_test['image_pixel_depth'] = 32
    meta_test['image_pixel_format'] = 'floating point'
    test_image_metadata( "TIFF float", "autocorrelation.tif", meta_test )


if 'all' in mode or 'readmeta' in mode:
    print('\n')
    print('***************************************************')
    print('"readmeta" - Reading and parsing metadata')
    print('***************************************************')

    # testing extracting GPS tags from EXIF
    meta_test = {}
    meta_test['Exif/GPSInfo/GPSLatitude']    = '34deg 26.27000\''
    meta_test['Exif/GPSInfo/GPSLatitudeRef'] = 'North'
    test_metadata_read( "JPEG EXIF", "IMG_0488.JPG", meta_test )

    # testing extracting IPTC tags
    meta_test = {}
    meta_test['Iptc/Application2/City']    = 'Santa Barbara'
    meta_test['Iptc/Application2/ProvinceState'] = 'CA'
    test_metadata_read( "JPEG IPTC", "IMG_0184.JPG", meta_test )

    # testing reading RGBA PNG image
    meta_test = {}
    meta_test['image_mode'] = 'RGBA'
    meta_test['ColorProfile/color_space'] = 'RGB'
    meta_test['ColorProfile/description'] = 'sRGB IEC61966-2.1'
    meta_test['ColorProfile/size'] = '3144'
    meta_test['channel_color_0'] = '255,0,0'
    meta_test['channel_color_1'] = '0,255,0'
    meta_test['channel_color_2'] = '0,0,255'
    meta_test['channel_color_3'] = '0,0,0'
    meta_test['channel_0_name'] = 'Red'
    meta_test['channel_1_name'] = 'Green'
    meta_test['channel_2_name'] = 'Blue'
    meta_test['channel_3_name'] = 'Alpha'
    test_metadata_read( "PNG RGBA", "IMG_0184_RGBA.png", meta_test )

    # reading metadata from OIB v 2.0.0.0
    meta_test = {}
    meta_test['date_time'] = '2010-06-04 08:26:06'
    meta_test['pixel_resolution_x'] = 0.206798
    meta_test['pixel_resolution_y'] = 0.206798
    meta_test['pixel_resolution_z'] = 0.428571
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['channel_0_name'] = 'Cy2'
    meta_test['channel_1_name'] = 'Cy3'
    meta_test['channel_2_name'] = 'Cy5'
    meta_test['display_channel_blue'] = '2'
    meta_test['display_channel_cyan'] = '-1'
    meta_test['display_channel_gray'] = '-1'
    meta_test['display_channel_green'] = '0'
    meta_test['display_channel_magenta'] = '-1'
    meta_test['display_channel_red'] = 1
    meta_test['display_channel_yellow'] = '-1'
    test_metadata_read( "OIB.2", "CSR 4 mo COX g M cone r PNA b z1.oib", meta_test )

    # reading metadata from large OIB
    meta_test = {}
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 1024
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 528
    meta_test['pixel_resolution_x'] = 1.24079
    meta_test['pixel_resolution_y'] = 1.24079
    meta_test['pixel_resolution_z'] = 1.23765
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['date_time'] = '2012-01-25 15:35:02'
    meta_test['channel_0_name'] = 'None'
    test_metadata_read( "OIB.Large", "112811B_5.oib", meta_test )

    # reading metadata from OIB with strange Z planes and only actual one Z image
    meta_test = {}
    meta_test['image_num_c'] = 4
    meta_test['image_num_p'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 640
    meta_test['image_pixel_depth'] = 16
    meta_test['date_time'] = '2010-11-19 14:32:33'
    meta_test['pixel_resolution_x'] = 0.496223
    meta_test['pixel_resolution_y'] = 0.496223
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['channel_0_name'] = 'Alexa Fluor 405'
    meta_test['channel_1_name'] = 'Cy2'
    meta_test['channel_2_name'] = 'Cy3'
    meta_test['channel_3_name'] = 'Cy5'
    meta_test['display_channel_red']     = '2'
    meta_test['display_channel_green']   = 1
    meta_test['display_channel_blue']    = 3
    meta_test['display_channel_cyan']    = '-1'
    meta_test['display_channel_magenta'] = '-1'
    meta_test['display_channel_yellow']  = '-1'
    meta_test['display_channel_gray']    = '0'
    test_metadata_read( "OIB 2.0 One plane", "Retina 4 top.oib", meta_test )


    # reading metadata from Zeiss ZVI
    meta_test = {}
    if os.name == 'nt':
        meta_test['date_time'] = date_parse('2006-06-22T08:27:13-0800')
    else:
        meta_test['date_time'] = '2006-06-22 15:27:13'
    meta_test['pixel_resolution_x'] = 0.157153
    meta_test['pixel_resolution_y'] = 0.157153
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['channel_0_name'] = 'Cy5'
    meta_test['channel_1_name'] = 'TRITC'
    meta_test['channel_2_name'] = 'FITC'
    meta_test['channel_3_name'] = 'DAPI'
    meta_test['objectives/objective:0/name'] = 'Plan Neofluar 40x/1.30 Oil Ph3 (DIC III) (440451)'
    meta_test['objectives/objective:0/magnification'] = 40.0
    meta_test['objectives/objective:0/numerical_aperture'] = 1.3
    test_metadata_read( "Zeiss ZVI", "23D3HA-cy3 psd-gfp-488 Homer-647 DIV 14 - 3.zvi", meta_test )

    # reading metadata from Zeiss ZVI
    meta_test = {}
    meta_test['image_num_c'] = 2
    meta_test['image_num_p'] = 14
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 14
    meta_test['image_pixel_depth'] = 16
    meta_test['image_num_x'] = 1388
    meta_test['image_num_y'] = 1040
    meta_test['date_time'] = date_parse('2010-01-06T03:53:37-0800')
    meta_test['pixel_resolution_x'] = 0.102381
    meta_test['pixel_resolution_y'] = 0.102381
    meta_test['pixel_resolution_z'] = 0.32
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['channel_0_name'] = 'DsRed'
    meta_test['channel_1_name'] = 'eGFP'
    meta_test['display_channel_red'] = '-1'
    meta_test['display_channel_green'] = '0'
    meta_test['display_channel_blue'] = 1
    meta_test['display_channel_yellow'] = '-1'
    meta_test['display_channel_magenta'] = '-1'
    meta_test['display_channel_cyan'] = '-1'
    meta_test['display_channel_gray'] = '-1'
    meta_test['objectives/objective:0/name'] = 'C-Apochromat 63x/1.20 W Korr UV VIS IR'
    meta_test['objectives/objective:0/magnification'] = 63.0
    meta_test['objectives/objective:0/numerical_aperture'] = 1.2
    test_metadata_read( "Zeiss ZVI", "0022.zvi", meta_test )


    # reading metadata from Andor
    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_p'] = 12
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['date_time'] = '2010-06-08 10:10:36'
    meta_test['pixel_resolution_x'] = 0.479042
    meta_test['pixel_resolution_y'] = 0.479042
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['stage_position/0/x'] = 923.497
    meta_test['stage_position/0/y'] = -1660.5
    meta_test['stage_position/0/z'] = 49.99
    test_metadata_read( "Andor", "MF Mon 2x2.tif", meta_test )


    # reading metadata from MicroManager OME-TIFF
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 3
    meta_test['image_num_z'] = 3
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['pixel_resolution_x'] = 1
    meta_test['pixel_resolution_y'] = 1
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['MicroManager/Objective-Label'] = 'Nikon 10X S Fluor'
    meta_test['MicroManager/Camera-CameraName'] = 'DemoCamera-MultiMode'

    meta_test['channels/channel_00000/name'] = 'Cy5'
    meta_test['channels/channel_00000/fluor'] = 'Cy5'
    meta_test['channels/channel_00000/color'] = '255,255,0'

    meta_test['channels/channel_00001/name'] = 'DAPI'
    meta_test['channels/channel_00001/fluor'] = 'DAPI'
    meta_test['channels/channel_00001/color'] = '255,0,204'

    meta_test['channels/channel_00002/name'] = 'FITC'
    meta_test['channels/channel_00002/fluor'] = 'FITC'
    meta_test['channels/channel_00002/color'] = '255,255,0'

    test_metadata_read( "MicroManager OME-TIFF", "Untitled_MMImages_Pos0.ome.tif", meta_test )

    # reading metadata from DCRAW - Adobe DNG
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 2616
    meta_test['image_num_y'] = 1960
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['Exif/Image/Make'] = 'Canon'
    meta_test['Exif/Image/Model'] = 'Canon PowerShot G5'
    meta_test['Exif/Photo/ExposureTime'] = '1/160 s'
    meta_test['Exif/Photo/FNumber'] = 'F4'
    meta_test['Exif/Photo/FocalLength'] = '7.2 mm'
    meta_test['Exif/Photo/ISOSpeedRatings'] = '50'
    test_metadata_read( "Adobe DNG", "CRW_0136_COMPR.dng", meta_test )

    # reading metadata from DCRAW - Canon CR2
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 3522
    meta_test['image_num_y'] = 2348
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['Exif/Photo/ISOSpeedRatings'] = '100'
    meta_test['Exif/Photo/ExposureTime'] = '1/3 s'
    meta_test['Exif/Photo/FNumber'] = 'F5.6'
    test_metadata_read( "Canon CR2", "IMG_0040.CR2", meta_test )

    # reading metadata from DCRAW - Canon 5DSr
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 8736
    meta_test['image_num_y'] = 5856
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['format'] = 'CANON-RAW'
    meta_test['image_mode'] = 'RGB'
    meta_test['Exif/Photo/Flash'] = 'No, compulsory'
    meta_test['Exif/Photo/FocalLength'] = '13.0 mm'
    meta_test['Exif/Photo/FNumber'] = 'F10'
    meta_test['DCRAW/aperture'] = '10.000000'
    meta_test['DCRAW/focal_length'] = 13
    meta_test['DCRAW/iso_speed'] = 100
    meta_test['DCRAW/make'] = 'Canon'
    meta_test['DCRAW/model'] = 'EOS 5DS R'
    meta_test['DCRAW/shutter'] = 0.005
    meta_test['date_time'] = '2016-05-24 15:16:35'
    meta_test['DCRAW/aperture'] = 10
    test_metadata_read( "Canon 5DSr CR2", "6J0A3548.CR2", meta_test )

    # reading metadata from DCRAW - Canon CRW
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 2616
    meta_test['image_num_y'] = 1960
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['Exif/Image/Make'] = 'Canon'
    meta_test['Exif/Image/Model'] = 'Canon PowerShot G5'
    meta_test['Exif/Photo/ExposureTime'] = '1/156 s'
    meta_test['Exif/Photo/FNumber'] = 'F4'
    test_metadata_read( "Canon CRW", "CRW_0136.CRW", meta_test )

    # reading metadata from DCRAW - Pentax
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 3008
    meta_test['image_num_y'] = 2000
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 8
    meta_test['Exif/Photo/ExposureTime'] = '1/125 s'
    meta_test['Exif/Photo/FNumber'] = 'F8'
    meta_test['Exif/Photo/ISOSpeedRatings'] = 200
    test_metadata_read( "Pentax", "PENTAX_IMGP1618.JPG", meta_test )

    # reading metadata from DCRAW - Minolta
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 3016
    meta_test['image_num_y'] = 2008
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['Exif/Photo/ExposureTime'] = '1/80 s'
    meta_test['Exif/Photo/FNumber'] = 'F20'
    meta_test['Exif/Photo/ISOSpeedRatings'] = 100
    test_metadata_read( "Minolta", "PICT1694.MRW", meta_test )


    # reading metadata from DCRAW - Nikon
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 2576
    meta_test['image_num_y'] = 1924
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['Exif/Photo/ExposureTime'] = '1 s'
    meta_test['Exif/Photo/FNumber'] = 'F4.2'
    test_metadata_read( "Nikon", "DSCN0041.NEF", meta_test )

    # reading metadata from DCRAW - Olympus
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 3088
    meta_test['image_num_y'] = 2310
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['Exif/Photo/ExposureTime'] = '1/30 s'
    meta_test['Exif/Photo/FNumber'] = 'F2.8'
    meta_test['Exif/Photo/ISOSpeedRatings'] = 125
    test_metadata_read( "Olympus", "P1110010.ORF", meta_test )

    # GeoTIFF
    meta_test = {}
    meta_test['image_num_x'] = 1281
    meta_test['image_num_y'] = 1037
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 8
    meta_test['Geo/Tags/ModelPixelScaleTag'] = '0.179859484777536,0.179826422372659,0'
    meta_test['Geo/Tags/ModelTiepointTag'] = '0,0,0;719865.328538339,9859359.12604843,0'
    meta_test['Geo/Model/projection'] = '16136 (UTM zone 36S)'
    meta_test['Geo/Model/proj4_definition'] = '+proj=tmerc +lat_0=0.000000000 +lon_0=33.000000000 +k=0.999600 +x_0=500000.000 +y_0=10000000.000 +ellps=WGS84 +units=m'
    meta_test['Geo/Coordinates/center'] = '-1.2725008,34.9769992'
    meta_test['Geo/Coordinates/center_model'] = '719980.529,9859265.886'
    meta_test['Geo/Coordinates/upper_left'] = '-1.2716586,34.9759636'
    meta_test['Geo/Coordinates/upper_left_model'] = '719865.329,9859359.126'
    test_metadata_read( "GeoTIFF", "test4.tif", meta_test )


    if not 'no_jpegxr' in mode:
        meta_test = {}
        meta_test['image_num_x'] = 1200
        meta_test['image_num_y'] = 1650
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        test_metadata_read( "JPEG-XR", "219j_q050.jxr", meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 1200
    meta_test['image_num_y'] = 1650
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_pixel_depth'] = 8
    test_metadata_read( "JPEG-2000", "219j.jp2", meta_test )

    if not 'no_webp' in mode:
        meta_test = {}
        meta_test['image_num_x'] = 1200
        meta_test['image_num_y'] = 1650
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 8
        test_metadata_read( "WEBP", "219j_q080.webp", meta_test )

    # DICOM
    if not 'no_dicom' in mode:
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 0.439453
        meta_test['pixel_resolution_y'] = 0.439453
        meta_test['pixel_resolution_z'] = 5.0
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Rescale Intercept'] = -1024
        meta_test['DICOM/Rescale Slope'] = 1
        meta_test['DICOM/Rescale Type'] = 'HU'
        meta_test['DICOM/Scan Options'] = 'AXIAL MODE'
        meta_test['DICOM/Series Description'] = 'HEAD ST W/O'
        meta_test['DICOM/Window Center'] = 40
        meta_test['DICOM/Window Width'] = 80
        meta_test['DICOM/Patient\'s Age'] = '035Y'
        test_metadata_read( "DICOM CT", "10", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 16
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_z'] = 10.0
        meta_test['pixel_resolution_t'] = 69.47
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'MR'
        meta_test['DICOM/Study Description'] = 'MRI'
        test_metadata_read( "DICOM MR", "MR-MONO2-8-16x-heart", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 128
        meta_test['image_num_y'] = 120
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 8
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        if os.name == 'nt': # older library is used under windows for now
            meta_test['pixel_resolution_x'] = 4
            meta_test['pixel_resolution_y'] = 3
        else:
            meta_test['pixel_resolution_x'] = 3
            meta_test['pixel_resolution_y'] = 4
        meta_test['pixel_resolution_t'] = 100
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'US'
        meta_test['DICOM/Study Description'] = 'Exercise Echocardiogram'
        test_metadata_read( "DICOM EXECHO", "US-MONO2-8-8x-execho", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 600
        meta_test['image_num_y'] = 430
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 10
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_t'] = 1
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'US'
        meta_test['DICOM/Study Description'] = 'Echocardiogram'
        test_metadata_read( "DICOM ECHO", "US-PAL-8-10x-echo", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 1024
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['DICOM/Modality'] = 'RF'
        meta_test['DICOM/Patient\'s Sex'] = 'F'
        test_metadata_read( "DICOM RF", "0015.DCM", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 600
        meta_test['image_num_y'] = 430
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 11
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1
        meta_test['pixel_resolution_y'] = 1
        meta_test['pixel_resolution_t'] = 1
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_t'] = 'seconds'
        meta_test['DICOM/Modality'] = 'US'
        meta_test['DICOM/Patient\'s Sex'] = 'F'
        meta_test['DICOM/Study Description'] = 'Echocardiogram'
        test_metadata_read( "DICOM US", "0020.DCM", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['pixel_resolution_x'] = 1.01562
        meta_test['pixel_resolution_y'] = 1.01562
        meta_test['pixel_resolution_z'] = 5
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Modality'] = 'MR'
        meta_test['DICOM/Patient\'s Sex'] = 'M'
        meta_test['DICOM/Study Description'] = 'ADNI RESEARCH STUDY'
        meta_test['DICOM/Slice Location'] = -46.90000153
        test_metadata_read( "DICOM ADNI", "ADNI_002_S_0295_MR_3-plane_localizer__br_raw_20060418193538653_1_S13402_I13712.dcm", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 1.09375
        meta_test['pixel_resolution_y'] = 1.09375
        meta_test['pixel_resolution_z'] = 10
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Modality'] = 'MR'
        meta_test['DICOM/Patient\'s Sex'] = 'F'
        meta_test['DICOM/Study Description'] = 'RaiGoe^standaard'
        meta_test['DICOM/Slice Location'] = 0
        test_metadata_read( "DICOM MR", "BetSog_20040312_Goebel_C2-0001-0001-0001.dcm", meta_test )

        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_c'] = 1
        meta_test['format']      = 'DICOM'
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['pixel_resolution_x'] = 0.361328
        meta_test['pixel_resolution_y'] = 0.361328
        meta_test['pixel_resolution_z'] = 0.75
        meta_test['pixel_resolution_unit_x'] = 'mm'
        meta_test['pixel_resolution_unit_y'] = 'mm'
        meta_test['pixel_resolution_unit_z'] = 'mm'
        meta_test['DICOM/Modality'] = 'CT'
        meta_test['DICOM/Window Center'] = '400\\\\700'
        meta_test['DICOM/Window Width'] = '2000\\\\4000'
        test_metadata_read( "DICOM CT", "IM-0001-0001.dcm", meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 64
    meta_test['image_num_y'] = 64
    meta_test['image_num_z'] = 21
    meta_test['image_num_t'] = 180
    meta_test['image_num_c'] = 1
    meta_test['format']      = 'NIFTI'
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'signed integer'
    meta_test['pixel_resolution_x'] = 4
    meta_test['pixel_resolution_y'] = 4
    meta_test['pixel_resolution_z'] = 6
    meta_test['pixel_resolution_unit_x'] = 'mm'
    meta_test['pixel_resolution_unit_y'] = 'mm'
    meta_test['pixel_resolution_unit_z'] = 'mm'
    test_metadata_read( "NIFTI functional", "filtered_func_data.nii", meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['image_num_z'] = 32
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['format']      = 'NIFTI'
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'unsigned integer'
    meta_test['pixel_resolution_x'] = 0.439453
    meta_test['pixel_resolution_y'] = 0.439453
    meta_test['pixel_resolution_z'] = 5
    #meta_test['pixel_resolution_unit_x'] = 'mm' # not stored in the image
    #meta_test['pixel_resolution_unit_y'] = 'mm'
    #meta_test['pixel_resolution_unit_z'] = 'mm'
    #meta_test['NIFTI/affine_transform'] = '0.439453,0.000000,0.000000,-112.060516;0.000000,0.439453,0.000000,-112.060516;0.000000,0.000000,5.000000,-75.000000'
    meta_test['NIFTI/transform_qform_to_ijk'] = '2.275556,0.000000,0.000000,0.000000;0.000000,2.275556,0.000000,0.000000;0.000000,0.000000,0.200000,0.000000;0.000000,0.000000,0.000000,1.000000'
    meta_test['NIFTI/transform_qform_to_xyz'] = '0.439453,0.000000,0.000000,0.000000;0.000000,0.439453,0.000000,0.000000;0.000000,0.000000,5.000000,0.000000;0.000000,0.000000,0.000000,1.000000'
    meta_test['NIFTI/transform_sform_to_ijk'] = '2.275556,0.000000,0.000000,255.000000;0.000000,2.275556,0.000000,255.000000;0.000000,0.000000,0.200000,15.000000;0.000000,0.000000,0.000000,1.000000'
    meta_test['NIFTI/transform_sform_to_xyz'] = '0.439453,0.000000,0.000000,-112.060516;0.000000,0.439453,0.000000,-112.060516;0.000000,0.000000,5.000000,-75.000000;0.000000,0.000000,0.000000,1.000000'
    test_metadata_read( "NIFTI transforms", "16_day1_1_patient_29C93FK6_1.nii", meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 64
    meta_test['image_num_y'] = 64
    meta_test['image_num_z'] = 35
    meta_test['image_num_t'] = 147
    meta_test['image_num_c'] = 1
    meta_test['format']      = 'NIFTI'
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'signed integer'
    meta_test['pixel_resolution_x'] = 3.4375
    meta_test['pixel_resolution_y'] = 3.4375
    meta_test['pixel_resolution_z'] = 3.99942
    #meta_test['pixel_resolution_unit_x'] = 'mm' # not stored in the image
    #meta_test['pixel_resolution_unit_y'] = 'mm'
    #meta_test['pixel_resolution_unit_z'] = 'mm'
    meta_test['pixel_resolution_unit_t'] = 'ms'
    meta_test['NIFTI/quaternion'] = '0.000000,0.025615,0.999672;108.281250,103.739151,-83.445091'
    meta_test['XCEDE/study/series/acquisition_protocol/parameters/receivecoilname'] = 'HEAD'
    meta_test['XCEDE/study/series/id'] = 1
    meta_test['XCEDE/subject/id'] = 103
    meta_test['XCEDE/study/series/scanner/manufacturer'] = 'GE'
    meta_test['XCEDE/study/series/scanner/model'] = 'LX NVi 4T'
    test_metadata_read( "NIFTI XCEDE", "newsirp_final_XML.nii", meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 260
    meta_test['image_num_y'] = 311
    meta_test['image_num_z'] = 260
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['format']      = 'NIFTI'
    meta_test['image_pixel_depth'] = 32
    meta_test['image_pixel_format'] = 'floating point'
    meta_test['pixel_resolution_x'] = 0.7
    meta_test['pixel_resolution_y'] = 0.7
    meta_test['pixel_resolution_z'] = 0.7
    meta_test['pixel_resolution_unit_x'] = 'mm'
    meta_test['pixel_resolution_unit_y'] = 'mm'
    meta_test['pixel_resolution_unit_z'] = 'mm'
    #meta_test['NIFTI/affine_transform'] = '-0.700000,0.000000,0.000000,90.000000;0.000000,0.700000,0.000000,-126.000000;0.000000,0.000000,0.700000,-72.000000'
    meta_test['NIFTI/transform_qform_to_ijk'] = '-1.428571,0.000000,-0.000000,128.571431;0.000000,1.428571,0.000000,180.000003;-0.000000,-0.000000,1.428571,102.857145;0.000000,0.000000,0.000000,1.000000'
    meta_test['NIFTI/transform_qform_to_xyz'] = '-0.700000,0.000000,-0.000000,90.000000;0.000000,0.700000,-0.000000,-126.000000;0.000000,0.000000,0.700000,-72.000000;0.000000,0.000000,0.000000,1.000000'
    meta_test['NIFTI/transform_sform_to_ijk'] = '-1.428571,-0.000000,-0.000000,128.571431;-0.000000,1.428571,-0.000000,180.000003;-0.000000,-0.000000,1.428571,102.857145;0.000000,0.000000,0.000000,1.000000'
    meta_test['NIFTI/transform_sform_to_xyz'] = '-0.700000,0.000000,0.000000,90.000000;0.000000,0.700000,0.000000,-126.000000;0.000000,0.000000,0.700000,-72.000000;0.000000,0.000000,0.000000,1.000000'
    test_metadata_read( "NIFTI GZ", "T1w.nii.gz", meta_test )

    if not 'no_jpegxr' in mode:
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5472
        meta_test['image_num_y'] = 3648
        meta_test['image_num_z'] = 1
        meta_test['image_pixel_depth'] = 16
        meta_test['Exif/Photo/ExposureTime'] = '1/400 s'
        meta_test['Exif/Photo/FNumber'] = 'F11'
        meta_test['Exif/Photo/ISOSpeedRatings'] = 100
        meta_test['Iptc/Application2/ProvinceState'] = 'California'
        meta_test['Exif/GPSInfo/GPSLatitude'] = '34deg 29.12550\''
        test_metadata_read( "JPEG-XR", "IMG_1913_16bit_prophoto_q90.jxr", meta_test )

    # JPEG-2000 and WebP metadata are not yet implemented
#    meta_test = {}
#    meta_test['image_num_c'] = 3
#    meta_test['image_num_t'] = 1
#    meta_test['image_num_z'] = 1
#    meta_test['image_num_x'] = '5472'
#    meta_test['image_num_y'] = '3648'
#    meta_test['image_num_z'] = 1
#    meta_test['image_pixel_depth'] = 16
#    meta_test['Exif/Photo/ExposureTime'] = '1/400 s'
#    meta_test['Exif/Photo/FNumber'] = 'F11'
#    meta_test['Exif/Photo/ISOSpeedRatings'] = '100'
#    meta_test['Iptc/Application2/ProvinceState'] = 'California'
#    meta_test['Exif/GPSInfo/GPSLatitude'] = '34deg 29.12550'
#    test_metadata_read( "JPEG-2000", "IMG_1913_16bit_prophoto_ts1024_q90.jp2", meta_test )
#
#    meta_test = {}
#    meta_test['image_num_c'] = 3
#    meta_test['image_num_t'] = 1
#    meta_test['image_num_z'] = 1
#    meta_test['image_num_x'] = '5472'
#    meta_test['image_num_y'] = '3648'
#    meta_test['image_num_z'] = 1
#    meta_test['image_pixel_depth'] = 16
#    meta_test['Exif/Photo/ExposureTime'] = '1/400 s'
#    meta_test['Exif/Photo/FNumber'] = 'F11'
#    meta_test['Exif/Photo/ISOSpeedRatings'] = '100'
#    meta_test['Iptc/Application2/ProvinceState'] = 'California'
#    meta_test['Exif/GPSInfo/GPSLatitude'] = '34deg 29.12550'
#    test_metadata_read( "WebP", "IMG_1913_prophoto_q90.webp", meta_test )

    # MRC

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5120
    meta_test['image_num_y'] = 3840
    meta_test['image_pixel_depth'] = 32
    meta_test['image_pixel_format'] = 'floating point'
    meta_test['pixel_resolution_x'] = 2.51
    meta_test['pixel_resolution_y'] = 2.51
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'angstroms'
    meta_test['MRC/alpha'] = 90
    meta_test['MRC/amean'] = 3.30166
    meta_test['MRC/label_00'] = 'EMAN 9/26/2015 10:08'
    test_metadata_read( "MRC", "20150917_05195_DNA-TET-25k-DE20_raw.region_000.sum-all_003-072.mrc", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 32
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 256
    meta_test['image_num_y'] = 256
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'signed integer'
    meta_test['pixel_resolution_x'] = 1
    meta_test['pixel_resolution_y'] = 1
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'angstroms'
    meta_test['MRC/alpha'] = 90
    meta_test['MRC/amean'] = 83.7333
    test_metadata_read( "MRC", "golgi.mrc", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 2048
    meta_test['image_num_y'] = 2048
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'unsigned integer'
    meta_test['pixel_resolution_x'] = 2.35294e-007
    meta_test['pixel_resolution_y'] = 2.35294e-007
    #meta_test['pixel_resolution_z'] = 2.35294e-007
    meta_test['pixel_resolution_unit_x'] = 'meters'
    meta_test['pixel_resolution_unit_y'] = 'meters'
    meta_test['MRC/alpha'] = 0
    meta_test['FEI/magnification'] = 84
    meta_test['FEI/a_tilt'] = -6.6316e-005
    meta_test['FEI/x_stage'] = -0.000343477
    meta_test['FEI/y_stage'] = -0.000862657
    meta_test['FEI/z_stage'] = -0.000210128
    test_metadata_read( "MRC", "Tile_19491580_0_1.mrc", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 78
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 572
    meta_test['image_num_y'] = 378
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'signed integer'
    meta_test['pixel_resolution_x'] = 1
    meta_test['pixel_resolution_y'] = 1
    meta_test['pixel_resolution_z'] = 1
    meta_test['pixel_resolution_unit_x'] = 'angstroms'
    meta_test['MRC/alpha'] = 90
    meta_test['MRC/label_00'] = 'Clip: 3D FFT                                            22-Aug-00  11:46:25'
    test_metadata_read( "MRC", "dual.rec", meta_test )


    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 3838
    meta_test['image_num_y'] = 3710
    meta_test['image_pixel_depth'] = 32
    meta_test['image_pixel_format'] = 'floating point'
    #meta_test['pixel_resolution_x'] = 2.51
    #meta_test['pixel_resolution_y'] = 2.51
    #meta_test['pixel_resolution_z'] = 1
    #meta_test['pixel_resolution_unit_x'] = 'angstroms'
    meta_test['MRC/alpha'] = 90
    meta_test['MRC/amean'] = 26.4308
    test_metadata_read( "MRC", "29kx_30epi_058_aligned.mrc", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 25
    meta_test['image_num_x'] = 73
    meta_test['image_num_y'] = 43
    meta_test['image_pixel_depth'] = 32
    meta_test['image_pixel_format'] = 'floating point'
    meta_test['pixel_resolution_x'] = 0.44825
    meta_test['pixel_resolution_y'] = 0.3925
    meta_test['pixel_resolution_z'] = 0.45875
    meta_test['pixel_resolution_unit_x'] = 'angstroms'
    meta_test['pixel_resolution_unit_y'] = 'angstroms'
    meta_test['pixel_resolution_unit_z'] = 'angstroms'
    meta_test['MRC/alpha'] = 90
    meta_test['MRC/amean'] = 0.000532967
    meta_test['MRC/label_00'] = '::::EMDATABANK.org::::EMD-3001::::'
    test_metadata_read( "MRC", "EMD-3001.map", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 20
    meta_test['image_num_x'] = 20
    meta_test['image_num_y'] = 20
    meta_test['image_pixel_depth'] = 32
    meta_test['image_pixel_format'] = 'floating point'
    meta_test['pixel_resolution_x'] = 11.4
    meta_test['pixel_resolution_y'] = 11.4
    meta_test['pixel_resolution_z'] = 11.4
    meta_test['pixel_resolution_unit_x'] = 'angstroms'
    meta_test['pixel_resolution_unit_y'] = 'angstroms'
    meta_test['pixel_resolution_unit_z'] = 'angstroms'
    meta_test['MRC/alpha'] = 90
    meta_test['MRC/amean'] = 0.783612
    meta_test['MRC/label_00'] = '::::EMDATABANK.org::::EMD-3197::::'
    test_metadata_read( "MRC", "EMD-3197.map", meta_test )

    # # Zeiss CZI
    if not 'no_czi' in mode:
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 97
        meta_test['image_num_x'] = 400
        meta_test['image_num_y'] = 400
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 1
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'multichannel'
        meta_test['fov_height'] = 400
        meta_test['fov_width'] = 400
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['pixel_resolution_x'] = 0.16125
        meta_test['pixel_resolution_y'] = 0.16125
        meta_test['pixel_resolution_z'] = 0.2
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['date_time'] = '2013-01-22T10:30:36.8295292+01:00'
        meta_test['document/application'] = 'ZEN 2012 (blue edition) v1.1.0.0'
        meta_test['document/username'] = 'zhmne'
        meta_test['document/description'] = 'Rat brain section, DAPI, GFAP-Alexa488'
        meta_test['objectives/objective:0/magnification'] = 40
        meta_test['objectives/objective:0/numerical_aperture'] = 1.4
        meta_test['objectives/objective:0/name'] = 'Plan-Apochromat 40x/1.4 Oil DIC (UV) VIS-IR M27'
        meta_test['channels/channel_00001/acquisition_mode'] = 'StructuredIllumination'
        meta_test['channels/channel_00001/color'] = '0.00,0.00,1.00'
        meta_test['channels/channel_00001/contrast_method'] = 'Fluorescence'
        meta_test['channels/channel_00001/exposure'] = 22
        meta_test['channels/channel_00001/exposure_units'] = 'ms'
        meta_test['channels/channel_00001/name'] = 'DAPI'
        test_metadata_read( "Zeiss CZI", "40x_RatBrain-AT-2ch-Z-wf.czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 41
        meta_test['image_num_x'] = 692
        meta_test['image_num_y'] = 520
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 1
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'multichannel'
        meta_test['fov_height'] = 520
        meta_test['fov_width'] = 692
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['pixel_resolution_x'] = 0.204762
        meta_test['pixel_resolution_y'] = 0.204762
        meta_test['pixel_resolution_z'] = 0.24
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['date_time'] = '2012-07-09T11:13:42.816077+02:00'
        meta_test['document/username'] = 'zhmne'
        meta_test['document/description'] = 'Bovine Pulmonary Artery Endothelial Cells; Mitotracker Red CMXRos (Mitochrondria), Alexa Fluore 488-Phalloidine (Actin filaments), DAPI (Nuclei)'
        meta_test['document/keywords'] = 'binning 2x2, Image: Markus Neumann, CZ Microscopy 2011'
        meta_test['objectives/objective:0/magnification'] = 63
        meta_test['objectives/objective:0/numerical_aperture'] = 1.4
        meta_test['objectives/objective:0/name'] = 'Plan-Apochromat 63x/1.40 Oil DIC M27'
        meta_test['channels/channel_00001/acquisition_mode'] = 'WideField'
        meta_test['channels/channel_00001/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel_00001/contrast_method'] = 'Fluorescence'
        meta_test['channels/channel_00001/exposure'] = 80
        meta_test['channels/channel_00001/exposure_units'] = 'ms'
        meta_test['channels/channel_00001/name'] = 'Actin (SYTOX Green)'
        test_metadata_read( "Zeiss CZI", "BPAE-cells-bin2x2_3chTZ(WF).czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 18
        meta_test['image_num_x'] = 995
        meta_test['image_num_y'] = 993
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 4
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'multichannel'
        meta_test['fov_height'] = 512
        meta_test['fov_width'] = 512
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['pixel_resolution_x'] = 0.3225
        meta_test['pixel_resolution_y'] = 0.3225
        meta_test['pixel_resolution_z'] = 0.6
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['image_num_resolution_levels'] = 7
        meta_test['image_num_resolution_levels_actual'] = 3
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625]
        meta_test['image_resolution_level_scales_actual'] = [1.000000,0.333984,0.111328]
        meta_test['date_time'] = '2012-07-09T11:50:20.28377+02:00'
        meta_test['document/username'] = 'zhmne'
        meta_test['document/keywords'] = 'Image: Markus Neumann, CZ Microscopy 2011, acquired with ZEN 2011 Betarelease (FIT)'
        #meta_test['channels/channel_00001/acquisition_mode'] = 'WideField'
        meta_test['channels/channel_00001/color'] = '1.00,0.34,0.00'
        #meta_test['channels/channel_00001/contrast_method'] = 'Fluorescence'
        meta_test['channels/channel_00001/emission_wavelength'] = 603
        meta_test['channels/channel_00001/excitation_wavelength'] = 577
        meta_test['channels/channel_00001/name'] = 'Actin sceleton (Alexa Fluor 568)'
        test_metadata_read( "Zeiss CZI", "Mouse_stomach_20x_ROI_3chZTiles(WF).czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 20
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 0.106822
        meta_test['pixel_resolution_y'] = 0.106822
        meta_test['pixel_resolution_z'] = 0.5
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_unit_y'] = 'um'
        meta_test['pixel_resolution_unit_z'] = 'um'
        meta_test['channels/channel_00001/color'] = '1.00,0.82,0.00'
        meta_test['channels/channel_00001/emission_wavelength'] = 580
        meta_test['channels/channel_00001/excitation_wavelength'] = 550
        meta_test['channels/channel_00001/name'] = 'Rhodamin'
        test_metadata_read( "Zeiss CZI", "16Bit-ZStack.czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 104
        meta_test['image_num_x'] = 364
        meta_test['image_num_y'] = 365
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 0.282
        meta_test['pixel_resolution_y'] = 0.282
        meta_test['pixel_resolution_z'] = 0.9
        meta_test['channels/channel_00000/color'] = '0.25,1.00,0.00'
        meta_test['channels/channel_00000/emission_wavelength'] = 525
        meta_test['channels/channel_00000/excitation_wavelength'] = 490
        meta_test['channels/channel_00000/name'] = 'FITC'
        test_metadata_read( "Zeiss CZI", "8Bit-ZStack.czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 3
        meta_test['image_num_z'] = 3
        meta_test['image_num_x'] = 200
        meta_test['image_num_y'] = 250
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['channels/channel_00001/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel_00001/name'] = 'Green CHANNEL_3'
        test_metadata_read( "Zeiss CZI", "CZT-Stack-Anno.czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 140
        meta_test['image_num_x'] = 348
        meta_test['image_num_y'] = 345
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        test_metadata_read( "Zeiss CZI", "Z-Stack.czi", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 4
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 10
        meta_test['image_num_x'] = 768
        meta_test['image_num_y'] = 576
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGBA'
        meta_test['channels/channel_00000/color'] = '1.00,0.00,0.00'
        meta_test['channels/channel_00001/name'] = 'Green 1'
        test_metadata_read( "Zeiss CZI", "Z-Stack-Anno(RGB).czi", meta_test )

    if not 'no_hdf5' in mode:
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 227
        meta_test['image_num_y'] = 149
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_series_paths/00000'] = '/image24bitpixel'
        meta_test['image_series_paths/00001'] = '/image8bit'
        test_metadata_read( "HDF5 image2", "ex_image2.h5", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 841
        meta_test['image_num_y'] = 721
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'indexed'
        meta_test['image_series_paths/00000'] = '/All data'
        meta_test['image_series_paths/00001'] = '/Land data'
        meta_test['image_series_paths/00002'] = '/Sea data'
        test_metadata_read( "HDF5 image2", "ex_image3.h5", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 426
        meta_test['image_num_x'] = 477
        meta_test['image_num_y'] = 502
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'signed integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_series_paths/00000'] = '/Reflectance'
        meta_test['HDF5/Reflectance/Description'] = 'Atmospherically corrected reflectance.'
        test_metadata_read( "HDF5 volume", "NEONDSImagingSpectrometerData.h5", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 80
        meta_test['image_num_x'] = 284
        meta_test['image_num_y'] = 334
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_series_paths/00000'] = '/DataContainers/ImageDataContainer/CellData/AspectRatios_cell'
        meta_test['image_series_paths/00007'] = '/DataContainers/ImageDataContainer/CellData/IPFColor'
        meta_test['HDF5/DataContainers/ImageDataContainer/CellData/AspectRatios_cell/TupleDimensions'] = '284,334,80'
        test_metadata_read( "Dream3D volume", "murks_lens.h5", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 80
        meta_test['image_num_x'] = 284
        meta_test['image_num_y'] = 334
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 3
        meta_test['pixel_resolution_y'] = 3
        meta_test['pixel_resolution_z'] = 3
        test_metadata_read( "Dream3D path 3c", "murks_lens.h5", meta_test, extra=['-path', '/DataContainers/ImageDataContainer/CellData/EulerAngles'])

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 80
        meta_test['image_num_x'] = 284
        meta_test['image_num_y'] = 334
        meta_test['image_pixel_depth'] = 32
        meta_test['image_pixel_format'] = 'floating point'
        meta_test['image_mode'] = 'multichannel'
        meta_test['pixel_resolution_x'] = 3
        meta_test['pixel_resolution_y'] = 3
        meta_test['pixel_resolution_z'] = 3
        test_metadata_read( "Dream3D path 1c", "murks_lens.h5", meta_test, extra=['-path', '/DataContainers/ImageDataContainer/CellData/Image Quality'])

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 30
        meta_test['image_num_z'] = 6
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['format'] = 'IMARIS5'
        meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_x'] = 0.817973
        meta_test['pixel_resolution_y'] = 0.817973
        meta_test['pixel_resolution_z'] = 5.38483
        meta_test['tile_num_original_x'] = 256
        meta_test['tile_num_original_y'] = 256
        meta_test['tile_num_original_z'] = 16
        meta_test['channels/channel_00000/name'] = '(name not specified)'
        meta_test['channels/channel_00000/pinhole_radius'] = 500.000
        meta_test['channels/channel_00000/emission_wavelength'] = 458.000
        test_metadata_read( "Imaris", "R18Demo.ims", meta_test )

        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 47
        meta_test['image_num_x'] = 1024
        meta_test['image_num_y'] = 1024
        meta_test['image_pixel_depth'] = 16
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['format'] = 'IMARIS5'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000]
        meta_test['image_resolution_level_structure'] = 'hierarchical'
        #meta_test['pixel_resolution_unit_x'] = 'um'
        meta_test['pixel_resolution_x'] = 0.13
        meta_test['pixel_resolution_y'] = 0.13
        meta_test['pixel_resolution_z'] = 0.0978723
        meta_test['tile_num_original_x'] = 128
        meta_test['tile_num_original_y'] = 128
        meta_test['tile_num_original_z'] = 8
        meta_test['document/acquisition_date'] = '2016-07-26 17:11:07.040'
        meta_test['objectives/objective:0/numerical_aperture'] = 1.4
        meta_test['channels/channel_00000/name'] = 'GFP iXon Confocal'
        meta_test['channels/channel_00000/pinhole_radius'] = 40
        meta_test['channels/channel_00000/emission_wavelength'] = '488 nm nm'
        meta_test['channels/channel_00000/color'] = '0.22,1.00,0.00'
        test_metadata_read( "Imaris", "2016-07-26_17.11.03_Sample Prefix_5P298C2_Protocol.ims", meta_test )

    # Nikon ND2
    meta_test = {}
    meta_test['image_num_c'] = 5
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 21
    meta_test['image_num_x'] = 1019
    meta_test['image_num_y'] = 1019
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'unsigned integer'
    meta_test['image_mode'] = 'multichannel'
    meta_test['format'] = 'ND2'
    meta_test['pixel_resolution_x'] = 1.01531
    meta_test['pixel_resolution_y'] = 1.01531
    meta_test['pixel_resolution_z'] = 5
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['document/acquisition_date'] = '06/06/2017  11:15:06'
    meta_test['document/version'] = '3.0'
    meta_test['objectives/objective:0/magnification'] = 20
    meta_test['objectives/objective:0/numerical_aperture'] = 0.75
    meta_test['objectives/objective:0/refractive_index'] = 1
    meta_test['channels/channel_00000/name'] = 'CSU far red RNA'
    meta_test['channels/channel_00000/exposure'] = 100
    meta_test['channels/channel_00000/exposure_units'] = 'ms'
    meta_test['channels/channel_00000/color'] = '1.00,1.00,1.00'
    meta_test['channels/channel_00000/fluor'] = 'CSU far red RNA'
    meta_test['channels/channel_00000/modality'] = 'Widefield Fluorescence'
    meta_test['channels/channel_00004/name'] = 'CSU BF'
    meta_test['channels/channel_00004/color'] = '1.00,1.00,1.00'
    meta_test['channels/channel_00004/modality'] = 'Brightfield, Spinning Disk Confocal'
    test_metadata_read( "Nikon ND2", "sample_image.nd2", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 42
    meta_test['image_num_z'] = 7
    meta_test['image_num_x'] = 624
    meta_test['image_num_y'] = 464
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'unsigned integer'
    meta_test['image_mode'] = 'multichannel'
    meta_test['format'] = 'ND2'
    meta_test['pixel_resolution_t'] = 300
    meta_test['pixel_resolution_z'] = 3
    meta_test['pixel_resolution_unit_t'] = 'seconds'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['document/acquisition_date'] = '16/08/2009  12:23:18'
    meta_test['document/version'] = '2.1'
    meta_test['objectives/objective:0/magnification'] = 20
    meta_test['objectives/objective:0/numerical_aperture'] = .45
    meta_test['objectives/objective:0/refractive_index'] = 1
    meta_test['channels/channel_00000/name'] = 'CFP/dapi'
    meta_test['channels/channel_00000/fluor'] = 'CFP/dapi'
    meta_test['channels/channel_00000/emission_wavelength'] = '435-485'
    meta_test['channels/channel_00000/excitation_wavelength'] = '385-415'
    meta_test['channels/channel_00000/color'] = '0.00,0.00,0.67'
    test_metadata_read( "Nikon ND2", "old_cells_02.nd2", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 361
    meta_test['image_num_y'] = 267
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'unsigned integer'
    meta_test['pixel_resolution_x'] = 0.343
    meta_test['pixel_resolution_y'] = 0.342282
    meta_test['pixel_resolution_unit_x'] = 'um'
    meta_test['pixel_resolution_unit_y'] = 'um'
    meta_test['document/acquisition_date'] = '2015-09-03T15:16:24'
    meta_test['document/vendor'] = 'Molecular Devices'
    meta_test['document/application'] = 'MetaMorph'
    meta_test['document/barcode'] = '240-65'
    meta_test['objectives/objective:0/name'] = '20X Plan Apo'
    meta_test['objectives/objective:0/magnification'] = 20
    meta_test['objectives/objective:0/numerical_aperture'] = 0.75
    meta_test['objectives/objective:0/refractive_index'] = 1
    meta_test['channels/channel_00000/name'] = 'DAPI'
    meta_test['channels/channel_00000/fluor'] = 'DAPI'
    meta_test['channels/channel_00000/emission_wavelength'] = 447
    meta_test['channels/channel_00000/exposure'] = 300
    meta_test['channels/channel_00000/exposure_units'] = 'ms'
    meta_test['channels/channel_00000/pinhole_radius'] = 60
    meta_test['channels/channel_00000/pinhole_radius_units'] = 'um'
    test_metadata_read( "MD HTD old format", "MD_HTD_OLD_w1.TIF", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 2048
    meta_test['image_num_y'] = 2048
    meta_test['image_pixel_depth'] = 16
    meta_test['image_pixel_format'] = 'unsigned integer'
    meta_test['pixel_resolution_x'] = 0.343
    meta_test['pixel_resolution_y'] = 0.342282
    meta_test['pixel_resolution_unit_x'] = 'um'
    meta_test['pixel_resolution_unit_y'] = 'um'
    meta_test['document/acquisition_date'] = '20150903 15:16:22.508'
    meta_test['document/vendor'] = 'Molecular Devices'
    meta_test['document/application'] = 'MetaMorph'
    meta_test['document/barcode'] = '240-65'
    meta_test['document/well_label'] = 'D07'
    meta_test['objectives/objective:0/name'] = '20X Plan Apo'
    meta_test['objectives/objective:0/magnification'] = 20
    meta_test['objectives/objective:0/numerical_aperture'] = 0.75
    meta_test['objectives/objective:0/refractive_index'] = 1
    meta_test['channels/channel_00000/name'] = 'DAPI'
    meta_test['channels/channel_00000/fluor'] = 'DAPI'
    meta_test['channels/channel_00000/emission_wavelength'] = 447
    meta_test['channels/channel_00000/exposure'] = 300
    meta_test['channels/channel_00000/exposure_units'] = 'ms'
    meta_test['channels/channel_00000/pinhole_radius'] = 60
    meta_test['channels/channel_00000/pinhole_radius_units'] = 'um'
    test_metadata_read( "MD HTD XML format", "MD_HTD_XML_w1.TIF", meta_test )

if not 'no_ffmpeg' in mode and ('all' in mode or 'video' in mode):
    print('\n')
    print('***************************************************')
    print('"video" - Reading video meta')
    print('***************************************************')

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 101
    meta_test['image_num_p'] = 101
    meta_test['image_num_x'] = 720
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'cinepak'
    test_image_video( "AVI CINEPAK", "122906_3Ax(inverted).avi", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 37
    meta_test['image_num_p'] = 37
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 20
    meta_test['video_codec_name'] = 'cinepak'
    test_image_video( "AVI CINEPAK", "241aligned.avi", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 199
    meta_test['image_num_p'] = 199
    meta_test['image_num_x'] = 826
    meta_test['image_num_y'] = 728
    meta_test['video_frames_per_second'] = 30
    meta_test['video_codec_name'] = 'mpeg4'
    test_image_video( "QuickTime MPEG4", "3Dstack.tif.3D.mov", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 73
    meta_test['image_num_p'] = 73
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 947
    meta_test['video_frames_per_second'] = 30.303
    meta_test['video_codec_name'] = 'rawvideo'
    test_image_video( "AVI RAW", "B4nf.RS.RE.z1.5.15.06.3DAnimation.avi", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 2879 # 2878
    meta_test['image_num_p'] = 2879 # 2878
    meta_test['image_num_x'] = 352
    meta_test['image_num_y'] = 240
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'mpeg1video'
    test_image_video( "MPEG", "EleanorRigby.mpg", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 301
    meta_test['image_num_p'] = 301
    meta_test['image_num_x'] = 884
    meta_test['image_num_y'] = 845
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'mpeg4'
    test_image_video( "QuickTime MPEG4", "Muller cell z4.oib.3D.mov", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 9
    meta_test['image_num_p'] = 9
    meta_test['image_num_x'] = 316
    meta_test['image_num_y'] = 400
    meta_test['video_frames_per_second'] = 9.00001
    meta_test['video_codec_name'] = 'rawvideo'
    test_image_video( "AVI RAW", "radiolaria.avi", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 2773
    meta_test['image_num_p'] = 2773
    meta_test['image_num_x'] = 1440
    meta_test['image_num_y'] = 1080
    meta_test['video_frames_per_second'] = 24
    meta_test['video_codec_name'] = 'wmv3'
    test_image_video( "WMV", "Step_into_Liquid_1080.wmv", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 199
    meta_test['image_num_p'] = 199
    meta_test['image_num_x'] = 826
    meta_test['image_num_y'] = 728
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'theora'
    test_image_video( "OGG", "test.ogv", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 36777 # 36776
    meta_test['image_num_p'] = 36777 # 36776
    meta_test['image_num_x'] = 1440
    meta_test['image_num_y'] = 1080
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'mpeg2video'
    test_image_video( "MPEG2 TS (1)", "B01C0201.M2T", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 1440
    meta_test['image_num_y'] = 1080
    meta_test['video_codec_name'] = 'h264'
    if os.name == 'nt':
        meta_test['video_frames_per_second'] = 59.9401
        meta_test['image_num_t'] = 17127
    else:
        meta_test['video_frames_per_second'] = 59.9401 #29.97
        meta_test['image_num_t'] = 17127 #8562
    test_image_video( "MPEG2 TS (2)", "Girsh_path3.m2ts", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 28
    meta_test['image_num_p'] = 28
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'mpeg4'
    test_image_video( "AVI MPEG4", "out.avi", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 30
    meta_test['image_num_p'] = 30
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'flv'
    test_image_video( "Flash video", "out.flv", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 28
    meta_test['image_num_p'] = 28
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'mpeg4'
    test_image_video( "MPEG4 H.263", "out.m4v", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 30
    meta_test['image_num_p'] = 30
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'h264'
    test_image_video( "Matroska MPEG4 H.264", "out.mkv", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 28
    meta_test['image_num_p'] = 28
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'h264'
    test_image_video( "Quicktime MPEG4 H.264", "out.mov", meta_test )

#    meta_test = {}
#    meta_test['image_num_z'] = 1
#    meta_test['image_num_t'] = 28
#    meta_test['image_num_p'] = 28
#    meta_test['image_num_x'] = 640
#    meta_test['image_num_y'] = 480
#    meta_test['video_frames_per_second'] = 29.97003
#    meta_test['video_codec_name'] = 'mpeg2video'
#    test_image_video( "MPEG2", "out.mpg", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 31
    meta_test['image_num_p'] = 31
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'theora'
    test_image_video( "OGG Theora", "out.ogg", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 31 # 30
    meta_test['image_num_p'] = 31 # 30
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'mpeg1video'
    test_image_video( "MPEG1", "out.vcd", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 27
    meta_test['image_num_p'] = 27
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'msmpeg4'
    test_image_video( "WMV", "out.wmv", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 30
    meta_test['image_num_p'] = 30
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'vp8'
    test_image_video( "WebM", "out.webm", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 28
    meta_test['image_num_p'] = 28
    meta_test['image_num_x'] = 640
    meta_test['image_num_y'] = 480
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'h264'
    test_image_video( "MPEG4 AVC H.264", "out_h264.mp4", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 25
    meta_test['image_num_p'] = 25
    meta_test['image_num_x'] = 1920
    meta_test['image_num_y'] = 1080
    meta_test['image_num_c'] = 3
    meta_test['video_frames_per_second'] = 29.97
    meta_test['video_codec_name'] = 'hevc'
    test_image_video( "MPEG4 AVC H.265", "out_h265.mp4", meta_test )

if not 'no_transform' in mode and ('all' in mode or 'transforms' in mode):
    print('\n')
    print('***************************************************')
    print('"transforms" - Computing transforms')
    print('***************************************************')

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 3
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'unsigned integer'
    test_image_transforms( '-filter', 'edge', "flowers_24bit_nointr.png", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'unsigned integer'
    test_image_transforms( '-filter', 'wndchrmcolor', "flowers_24bit_nointr.png", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 3
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'unsigned integer'
    test_image_transforms( '-transform_color', 'rgb2hsv', "flowers_24bit_nointr.png", meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 3
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 8
    meta_test['image_pixel_format'] = 'unsigned integer'
    test_image_transforms( '-transform_color', 'hsv2rgb', 'flowers_24bit_hsv.tif', meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 768
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 64
    meta_test['image_pixel_format'] = 'floating point'
    test_image_transforms( '-transform', 'chebyshev', 'flowers_8bit_gray.png', meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 64
    meta_test['image_pixel_format'] = 'floating point'
    test_image_transforms( '-transform', 'fft', 'flowers_8bit_gray.png', meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 1032
    meta_test['image_num_y'] = 776
    meta_test['image_pixel_depth'] = 64
    meta_test['image_pixel_format'] = 'floating point'
    test_image_transforms( '-transform', 'wavelet', 'flowers_8bit_gray.png', meta_test )

    meta_test = {}
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 180
    meta_test['image_num_y'] = 1283
    meta_test['image_pixel_depth'] = 64
    meta_test['image_pixel_format'] = 'floating point'
    test_image_transforms( '-transform', 'radon', 'flowers_8bit_gray.png', meta_test )

if 'all' in mode or 'commands' in mode:
    print('\n')
    print('***************************************************')
    print('"commands" - Computing commands')
    print('***************************************************')

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_x'] = 3264
    meta_test['image_num_y'] = 2448
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-rotate', 'guess'], "IMG_0562.JPG", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_x'] = 2448
    meta_test['image_num_y'] = 3264
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-rotate', 'guess'], "IMG_0593.JPG", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_x'] = 3264
    meta_test['image_num_y'] = 2448
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-rotate', 'guess'], "IMG_1003.JPG", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 32
    test_image_commands( ['-superpixels', 16], 'flowers_8bit_gray.png', meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 1
    meta_test['image_num_x'] = 1024
    meta_test['image_num_y'] = 768
    meta_test['image_pixel_depth'] = 32
    test_image_commands( ['-superpixels', 16], 'flowers_24bit_nointr.png', meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 2
    meta_test['image_num_x'] = 256
    meta_test['image_num_y'] = 256
    meta_test['image_num_z'] = 7
    meta_test['image_pixel_depth'] = 16
    meta_test['pixel_resolution_x'] = 0.41432
    meta_test['pixel_resolution_y'] = 0.41432
    meta_test['pixel_resolution_z'] = 1.85714
    meta_test['channel_0_name'] = 'FITC'
    meta_test['channel_1_name'] = 'Cy3'
    test_image_commands( ['-t', 'ome-tiff', '-resize3d', '256,0,0,TC'], '161pkcvampz1Live2-17-2004_11-57-21_AM.tif', meta_test )

    meta_test = {}
    meta_test['image_num_p'] = 256
    meta_test['image_num_c'] = 2
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 13
    meta_test['image_num_z'] = 256
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['pixel_resolution_x'] = 0.20716
    meta_test['pixel_resolution_y'] = 1
    meta_test['pixel_resolution_z'] = 0.41432
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['pixel_resolution_unit_t'] = 'seconds'
    meta_test['channel_0_name'] = 'FITC'
    meta_test['channel_1_name'] = 'Cy3'
    test_image_commands( ['-t', 'ome-tiff', '-rearrange3d', 'xzy'], 'cells.ome.tif', meta_test )

    meta_test = {}
    meta_test['image_num_p'] = 512
    meta_test['image_num_c'] = 2
    meta_test['image_num_x'] = 13
    meta_test['image_num_y'] = 256
    meta_test['image_num_z'] = 512
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 16
    meta_test['pixel_resolution_x'] = 1
    meta_test['pixel_resolution_y'] = 0.41432
    meta_test['pixel_resolution_z'] = 0.20716
    meta_test['pixel_resolution_unit_x'] = 'microns'
    meta_test['pixel_resolution_unit_y'] = 'microns'
    meta_test['pixel_resolution_unit_z'] = 'microns'
    meta_test['pixel_resolution_unit_t'] = 'seconds'
    meta_test['channel_0_name'] = 'FITC'
    meta_test['channel_1_name'] = 'Cy3'
    test_image_commands( ['-t', 'ome-tiff', '-rearrange3d', 'yzx'], 'cells.ome.tif', meta_test )


if 'all' in mode or 'pyramids' in mode:
    print('\n')
    print('***************************************************')
    print('"pyramids" - Testing pyramidal images')
    print('***************************************************')

    # test reading pyramidal metadata
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5000
    meta_test['image_num_y'] = 2853
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 4
    meta_test['image_resolution_level_scales'] = [1.0, 0.5, 0.25, 0.125]
    test_metadata_read( "Photoshop pyramid", "monument_photoshop.tif", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5000
    meta_test['image_num_y'] = 2853
    meta_test['tile_num_x'] = 512
    meta_test['tile_num_y'] = 512
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 5
    meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624]
    test_metadata_read( "Imagemagick pyramid", "monument_vips.tif", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5000
    meta_test['image_num_y'] = 2853
    meta_test['tile_num_x'] = 512
    meta_test['tile_num_y'] = 512
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 7
    meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
    test_metadata_read( "Top-IFD TIFF pyramid", "monument_imgcnv_topdirs.tif", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5000
    meta_test['image_num_y'] = 2853
    meta_test['tile_num_x'] = 512
    meta_test['tile_num_y'] = 512
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 7
    meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
    test_metadata_read( "Sub-IFD TIFF pyramid", "monument_imgcnv_subdirs.tif", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5000
    meta_test['image_num_y'] = 2853
    meta_test['tile_num_x'] = 512
    meta_test['tile_num_y'] = 512
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 7
    meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
    test_metadata_read( "OME-TIFF pyramid", "monument_imgcnv.ome.tif", meta_test )

    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 5000
    meta_test['image_num_y'] = 2853
    meta_test['tile_num_x'] = 256
    meta_test['tile_num_y'] = 256
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 7
    meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
    test_metadata_read( "256px tiles TIFF pyramid", "monument_imgcnv.256.tif", meta_test )

    # test reading pyramidal levels
    meta_test = {}
    meta_test['image_num_x'] = 156
    meta_test['image_num_y'] = 89
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv_subdirs.tif', meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 156
    meta_test['image_num_y'] = 89
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv_topdirs.tif', meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 156
    meta_test['image_num_y'] = 89
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv.ome.tif', meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 156
    meta_test['image_num_y'] = 89
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv.256.tif', meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 312
    meta_test['image_num_y'] = 178
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', '4'], 'monument_vips.tif', meta_test )

    meta_test = {}
    meta_test['image_num_x'] = 625
    meta_test['image_num_y'] = 357
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', 3], 'monument_photoshop.tif', meta_test )

    # test reading pyramidal tiles

    # - subifds
    meta_test = {}
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,2'], 'monument_imgcnv_subdirs.tif', meta_test ) # first tile

    meta_test = {}
    meta_test['image_num_x'] = 226
    meta_test['image_num_y'] = 201
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,2,1,2'], 'monument_imgcnv_subdirs.tif', meta_test ) # last tile

    meta_test = {}
    meta_test['image_num_x'] = 256
    meta_test['image_num_y'] = 256
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '256,1,1,2'], 'monument_imgcnv_subdirs.tif', meta_test ) # tile size different from stored

    # - vips
    meta_test = {}
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,2'], 'monument_vips.tif', meta_test ) # first tile

    meta_test = {}
    meta_test['image_num_x'] = 226
    meta_test['image_num_y'] = 201
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,2,1,2'], 'monument_vips.tif', meta_test ) # last tile

    meta_test = {}
    meta_test['image_num_x'] = 256
    meta_test['image_num_y'] = 256
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '256,1,1,2'], 'monument_vips.tif', meta_test ) # tile size different from stored

    # - ome-tiff
    meta_test = {}
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,2'], 'monument_imgcnv.ome.tif', meta_test ) # first tile

    meta_test = {}
    meta_test['image_num_x'] = 226
    meta_test['image_num_y'] = 201
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,2,1,2'], 'monument_imgcnv.ome.tif', meta_test ) # last tile

    meta_test = {}
    meta_test['image_num_x'] = 256
    meta_test['image_num_y'] = 256
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '256,1,1,2'], 'monument_imgcnv.ome.tif', meta_test ) # tile size different from stored

    # JPEG-2000

    # meta
    meta_test = {}
    meta_test['image_num_c'] = 3
    meta_test['image_num_t'] = 1
    meta_test['image_num_z'] = 1
    meta_test['image_num_x'] = 17334
    meta_test['image_num_y'] = 17457
    meta_test['tile_num_x'] = 2048
    meta_test['tile_num_y'] = 2048
    meta_test['image_pixel_depth'] = 8
    meta_test['image_num_resolution_levels'] = 8
    if os.name != 'nt': #dima: some minor precision differences, to be checked
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.007812]
    else:
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.007813]
    meta_test['image_resolution_level_structure'] = 'flat'
    test_metadata_read( "2048px tiles JPEG-2000 pyramid", "retina.jp2", meta_test )

    # levels
    meta_test = {}
    meta_test['image_num_x'] = 271
    meta_test['image_num_y'] = 273
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-res-level', '6'], 'retina.jp2', meta_test )

    # tiles
    meta_test = {}
    meta_test['image_num_x'] = 512
    meta_test['image_num_y'] = 512
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,4'], 'retina.jp2', meta_test ) # first tile

    meta_test = {}
    meta_test['image_num_x'] = 59
    meta_test['image_num_y'] = 67
    meta_test['image_num_c'] = 3
    meta_test['image_num_z'] = 1
    meta_test['image_num_t'] = 1
    meta_test['image_pixel_depth'] = 8
    test_image_commands( ['-t', 'tiff', '-tile', '512,2,2,4'], 'retina.jp2', meta_test ) # last tile

    # testing tile size different from stored is not required for flat structure

    # Zeiss CZI
    if not 'no_czi' in mode:
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 21787
        meta_test['image_num_y'] = 23863
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 256
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'RGB'
        meta_test['fov_height'] = 1200
        meta_test['fov_width'] = 1600
        meta_test['image_logical_x'] = -168462
        meta_test['image_logical_y'] = 48596
        meta_test['image_num_resolution_levels'] = 12
        meta_test['image_num_resolution_levels_actual'] = 6
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        if os.name != 'nt': #dima: some minor precision differences, to be checked
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906,0.001953,0.000977,0.000488]
        else:
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007813,0.003906,0.001953,0.000977,0.000488]
        meta_test['image_resolution_level_scales_actual'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['pixel_resolution_x'] = 0.22
        meta_test['pixel_resolution_y'] = 0.22
        meta_test['date_time'] = '2014-09-23T19:36:35.1926747+02:00'
        meta_test['document/application'] = 'ZEN 2012 (blue edition) v1.1.2.0'
        meta_test['document/username'] = 'sxq748'
        meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['objectives/objective:0/numerical_aperture'] = '0.8'
        meta_test['objectives/objective:0/name'] = 'Plan-Apochromat 20x/0.8 M27'
        meta_test['channels/channel_00001/acquisition_mode'] = 'WideField'
        meta_test['channels/channel_00001/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel_00001/contrast_method'] = 'Brightfield'
        meta_test['channels/channel_00001/exposure'] = '0.2'
        meta_test['channels/channel_00001/exposure_units'] = 'ms'
        meta_test['channels/channel_00001/name'] = 'Green TL Brightfield (Dye1)'
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['coordinates/axis_orientation'] = 'ascending,descending'
        meta_test['coordinates/axis_origin'] = 'top_left'
        meta_test['coordinates/tie_points/bottom_right'] = '-146676,72458'
        meta_test['coordinates/tie_points/center'] = '-157568,60528'
        meta_test['coordinates/tie_points/fovs/00000'] = '-158400,48600'
        test_metadata_read( "Zeiss CZI", "17.czi", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 680
        meta_test['image_num_y'] = 745
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '5'], '17.czi', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,4'], '17.czi', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 337
        meta_test['image_num_y'] = 467
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,2,2,4'], '17.czi', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,0,0,4'], '17.czi', meta_test )

        # CZI pyramid out of power of two scale
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 12000
        meta_test['image_num_y'] = 6000
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 12
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'RGB'
        meta_test['fov_height'] = 2000
        meta_test['fov_width'] = 3000
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['image_num_resolution_levels'] = 11
        meta_test['image_num_resolution_levels_actual'] = 5
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        if os.name == 'nt':
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007813,0.003906,0.001953,0.000977]
        else:
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906,0.001953,0.000977]
        meta_test['image_resolution_level_scales_actual'] = [1.000000,0.333333,0.111167,0.111111,0.037000]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['date_time'] = '2012-01-13T15:59:03.30925+01:00'
        meta_test['channels/channel_00001/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel_00001/name'] = 'Green C1 (Dye1)'
        test_metadata_read( "Zeiss CZI", "MultiResolution-Mosaic.czi", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 375
        meta_test['image_num_y'] = 187
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '5'], 'MultiResolution-Mosaic.czi', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,3'], 'MultiResolution-Mosaic.czi', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 476
        meta_test['image_num_y'] = 238
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,2,1,3'], 'MultiResolution-Mosaic.czi', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,0,0,4'], 'MultiResolution-Mosaic.czi', meta_test )


    # Imaris
    if not 'no_hdf5' in mode:
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 64
        meta_test['image_num_x'] = 2048
        meta_test['image_num_y'] = 1567
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_num_resolution_levels'] = 4
        meta_test['image_resolution_level_structure'] = 'hierarchical'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000]
        meta_test['tile_num_x'] = 256
        meta_test['tile_num_y'] = 256
        meta_test['tile_num_original_x'] = 256
        meta_test['tile_num_original_y'] = 256
        meta_test['tile_num_original_z'] = 16
        meta_test['document/acquisition_date'] = '1991-10-01 16:45:45.000'
        meta_test['channels/channel_00000/name'] = 'CollagenIV (TxRed)'
        meta_test['channels/channel_00001/name'] = 'GFAP (FITC)'
        meta_test['pixel_resolution_x'] = 0.0229058
        meta_test['pixel_resolution_y'] = 0.0228985
        meta_test['pixel_resolution_z'] = 0.2
        meta_test['pixel_resolution_unit_x'] = 'um'
        test_metadata_read( "Imaris", "retina_large.ims", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 195
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '3'], 'retina_large.ims', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'retina_large.ims', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 272
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,1,1,1'], 'retina_large.ims', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,0,0,1'], 'retina_large.ims', meta_test )

        # VQI
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 84007
        meta_test['image_num_y'] = 28030
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'mask'
        meta_test['image_num_resolution_levels'] = 9
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        if os.name == 'nt':
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007813,0.003906]
        else:
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906]
        meta_test['tile_num_x'] = 28
        meta_test['tile_num_y'] = 256
        meta_test['image_series_paths/00000'] = '/cells'
        test_metadata_read( "VQI", "sparse_dense.VQI.h5", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 1312
        meta_test['image_num_y'] = 437
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '6'], 'sparse_dense.VQI.h5', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'sparse_dense.VQI.h5', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 65
        meta_test['image_num_y'] = 364
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,5,1,5'], 'sparse_dense.VQI.h5', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], 'sparse_dense.VQI.h5', meta_test )

        # fetching densified FOV
        meta_test = {}
        meta_test['image_num_x'] = 2040
        meta_test['image_num_y'] = 2040
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-slice', 'fov:2'], 'sparse_dense.VQI.h5', meta_test )

        # fetching individual object
        meta_test = {}
        meta_test['image_num_x'] = 14
        meta_test['image_num_y'] = 18
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-path', '/cells/7'], 'sparse_dense.VQI.h5', meta_test )

    # Aperio SVS
    if not 'no_openslide' in mode:
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 46000
        meta_test['image_num_y'] = 32914
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_resolution_levels'] = 3
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        if os.name == 'nt':
            meta_test['image_resolution_level_scales'] = [1.000000,0.249992,0.062498]
        else:
            meta_test['image_resolution_level_scales'] = [1.000000,0.249992,0.062498]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['image_series_paths/00003'] = '/thumbnail'
        meta_test['pixel_resolution_x'] = 0.499
        meta_test['pixel_resolution_y'] = 0.499
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['document/quickhash'] = '30f1a38031fc0e21d81f9d01435ac4af848f6fe2bbf8f7768184336ee5d7e796'
        test_metadata_read( "Aperio", "CMU-1.svs", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 2874
        meta_test['image_num_y'] = 2057
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '4'], 'CMU-1.svs', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'CMU-1.svs', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 314
        meta_test['image_num_y'] = 9
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,5,4,4'], 'CMU-1.svs', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], 'CMU-1.svs', meta_test )

        # fetching individual object
        meta_test = {}
        meta_test['image_num_x'] = 1280
        meta_test['image_num_y'] = 431
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-path', '/preview'], 'CMU-1.svs', meta_test )

        # fetching individual object
        meta_test = {}
        meta_test['image_num_x'] = 387
        meta_test['image_num_y'] = 463
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-path', '/label'], 'CMU-1.svs', meta_test )

        # Hamamatsu NDPI
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 51200
        meta_test['image_num_y'] = 38144
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_resolution_levels'] = 9
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        if os.name == 'nt':
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007813,0.003906]
            meta_test['pixel_resolution_x'] = 0.45641259698767683
            meta_test['pixel_resolution_y'] = 0.45506257110352671
        else:
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906]
            meta_test['pixel_resolution_x'] = 0.45641259698767683
            meta_test['pixel_resolution_y'] = 0.45506257110352671
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/preview'
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['document/quickhash'] = 'f14cd7dcf9dff2b7b3551672a1f3248bd35cc4a2e57e3ca47a2b83eac9ad3987'
        test_metadata_read( "Hamamatsu", "CMU-1.ndpi", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 3200
        meta_test['image_num_y'] = 2384
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '4'], 'CMU-1.ndpi', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'CMU-1.ndpi', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 128
        meta_test['image_num_y'] = 336
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,6,4,4'], 'CMU-1.ndpi', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], 'CMU-1.ndpi', meta_test )

        # fetching individual object
        meta_test = {}
        meta_test['image_num_x'] = 1191
        meta_test['image_num_y'] = 408
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-path', '/preview'], 'CMU-1.ndpi', meta_test )


        # Philips
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 155136
        meta_test['image_num_y'] = 95744
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_resolution_levels'] = 10
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        if os.name == 'nt':
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007813,0.003906,0.001953]
        else:
            meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906,0.001953]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['pixel_resolution_x'] = 0.25
        meta_test['pixel_resolution_y'] = 0.25
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        #meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['document/quickhash'] = '9744e9ed2bd07f14b0f9f91a4e72de1c7578a1b850746979ae795ab784c603e7'
        test_metadata_read( "Philips", "2017SM09941_3_EVG.tiff", meta_test )

        # levels
        meta_test = {}
        meta_test['image_num_x'] = 4848
        meta_test['image_num_y'] = 2992
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-res-level', '5'], '2017SM09941_3_EVG.tiff', meta_test )

        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], '2017SM09941_3_EVG.tiff', meta_test ) # first tile

        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 240
        meta_test['image_num_y'] = 432
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '512,9,5,5'], '2017SM09941_3_EVG.tiff', meta_test ) # last tile

        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], '2017SM09941_3_EVG.tiff', meta_test )

        # fetching individual object
        meta_test = {}
        meta_test['image_num_x'] = 1771
        meta_test['image_num_y'] = 830
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        test_image_commands( ['-t', 'tiff', '-path', '/preview'], '2017SM09941_3_EVG.tiff', meta_test )




end = time.time()
elapsed= end - start

# print summary
print('\n\nFollowing tests took %s seconds:\n'%(elapsed))

if passed>0 and failed==0:
    print('Passed all %d tests. Congrats!!!\n'%(passed))

if passed>0 and failed>0:
    print('Passed: %d\n'%(passed))

if failed>0:
    print('Failed: %d\n'%(failed))
    for s in results:
        print('  > %s'%(s))
    sys.exit(failed)
