# BioImageConvertor

## Build

Use existing docker scripts for build environments, found in: docker

```
make -j
```

will produce:
imgcnv
libimgcnv.so

## Test

```
cd testing
python runtest.py
```
