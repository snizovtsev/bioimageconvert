#
# Sample docker file where the entrypoint is the imgcnv binary
#

FROM ubuntu:18.04 
ARG UID=1000
ARG GID=1000

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8


RUN apt-get update -qq && apt-get install -y \
  libjpeg-turbo8 \
  libavformat57 \
  libswscale4 \
  libgdcm2.8 \
  libhdf5-100 \
  libhdf5-cpp-100 \
  libsqlite3-0 \
  libraw-bin \
  libfftw3-3

COPY imgcnv /usr/local/bin

ENTRYPOINT ["/usr/local/bin/imgcnv"]
