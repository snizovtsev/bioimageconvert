/*****************************************************************************
  CZI support
  Copyright (c) 2019, ViQI Inc

  Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
  2019-05-23 11:50:40 - First creation

  ver : 1
  *****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <bim_format_misc.h>
#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xconf.h>
#include <xstring.h>
#include <xtypes.h>
#include <xunits.h>
#ifdef BIM_USE_EXIV2
#include <bim_exiv_parse.h>
#endif

#include "bim_openslide_format.h"

#include <pugixml.hpp>

//#include "openslide.h"

using namespace bim;

//****************************************************************************
// serialization
//****************************************************************************

#define BIM_FORMAT_OPENSLIDE_MAGIC_SIZE 10
const char openslide_magic_number[11] = "ZISRAWFILE";

#define BIM_OPENSLIDE_FORMAT_APERIO 0
#define BIM_OPENSLIDE_FORMAT_HAMAMATSU 1
#define BIM_OPENSLIDE_FORMAT_LEICA 2
#define BIM_OPENSLIDE_FORMAT_MIRAX 3
#define BIM_OPENSLIDE_FORMAT_PHILIPS 4
#define BIM_OPENSLIDE_FORMAT_SAKURA 5
#define BIM_OPENSLIDE_FORMAT_TRESTLE 6
#define BIM_OPENSLIDE_FORMAT_VENTANA 7

//****************************************************************************
// bim::OpenSlideParams
//****************************************************************************

bim::OpenSlideParams::OpenSlideParams() {
    i = initImageInfo();
}

bim::OpenSlideParams::~OpenSlideParams() {
    if (this->osr)
        openslide_close(this->osr);
    this->osr = NULL;
}

void bim::OpenSlideParams::open(const char *filename, bim::ImageIOModes io_mode) {
    this->osr = openslide_open(filename); // openslide takes utf8 filenames
}

//****************************************************************************
// required funcs
//****************************************************************************

int openslideValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    //if (length < BIM_FORMAT_OPENSLIDE_MAGIC_SIZE) return -1;
    //unsigned char *mag_num = (unsigned char *)magic;
    //if (memcmp(mag_num, openslide_magic_number, BIM_FORMAT_OPENSLIDE_MAGIC_SIZE) == 0) return 0;

    if (fileName) {
        xstring filename(fileName);
        filename = filename.toLowerCase();
        if (filename.endsWith(".svs")) return BIM_OPENSLIDE_FORMAT_APERIO;
        if (filename.endsWith(".ndpi")) return BIM_OPENSLIDE_FORMAT_HAMAMATSU;
        if (filename.endsWith(".vms")) return BIM_OPENSLIDE_FORMAT_HAMAMATSU;
        if (filename.endsWith(".vmu")) return BIM_OPENSLIDE_FORMAT_HAMAMATSU;
        if (filename.endsWith(".scn")) return BIM_OPENSLIDE_FORMAT_LEICA;
        if (filename.endsWith(".mrxs")) return BIM_OPENSLIDE_FORMAT_MIRAX;
        if (filename.endsWith(".svslide")) return BIM_OPENSLIDE_FORMAT_SAKURA;

        // dima: for now skip this and don't slow-down all other tiff formats
        if (filename.endsWith(".tif") || filename.endsWith(".tiff") || filename.endsWith(".bif")) {
            const char *v = openslide_detect_vendor(fileName);
            if (!v) return -1;
            xstring vendor(v);
            if (vendor == "aperio") return BIM_OPENSLIDE_FORMAT_APERIO;
            if (vendor == "philips") return BIM_OPENSLIDE_FORMAT_PHILIPS;
            if (vendor == "trestle") return BIM_OPENSLIDE_FORMAT_TRESTLE;
            if (vendor == "ventana") return BIM_OPENSLIDE_FORMAT_VENTANA;
        }
    }

    return -1;
}

FormatHandle openslideAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void openslideCloseImageProc(FormatHandle *fmtHndl);
void openslideReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    openslideCloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------

void openslideGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    *info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    if (conf)
        par->path = conf->getValue("-path");

    int64_t w;
    int64_t h;
    info->number_z = 1;
    info->number_t = 1;
    info->number_pages = 1;
    info->number_dims = 2;

    // openslide does not give many choices here
    //info->imageMode = IM_RGBA;
    //info->samples = 4;
    info->imageMode = IM_RGB;
    info->samples = 3;
    info->depth = 8;
    info->pixelType = FMT_UNSIGNED;

    info->resUnits = RES_IN;
    info->xRes = 0;
    info->yRes = 0;

    // sub images
    if (conf && conf->keyExists("-path")) {
        // paths would be like: /preview /label /thumbnail -> macro label thumbnail
        xstring path = conf->getValue("-path").replace("/", "").replace("preview", "macro");
        openslide_get_associated_image_dimensions(par->osr, path.c_str(), &w, &h);
        if (w < 1 || h < 1) return;
        info->width = w;
        info->height = h;
        par->image_width = w;
        par->image_height = h;
    }

    openslide_get_level0_dimensions(par->osr, &w, &h);
    if (w < 1 || h < 1) return;
    info->width = w;
    info->height = h;
    par->image_width = w;
    par->image_height = h;

    int32_t num_levels = openslide_get_level_count(par->osr);
    info->tileWidth = bim::OpenSlideParams::virtual_tile_width;
    info->tileHeight = bim::OpenSlideParams::virtual_tile_height;
    info->number_levels = num_levels;

    for (int32_t level = 0; level < num_levels; ++level) {
        double downsample = openslide_get_level_downsample(par->osr, level);
        par->downsamples.push_back(downsample);
        par->scales.push_back(1.0 / downsample);

        openslide_get_level_dimensions(par->osr, level, &w, &h);
        par->W.push_back(w);
        par->H.push_back(h);
    }

    const char *mpp_x = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_MPP_X);
    const char *mpp_y = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_MPP_Y);
    if (mpp_x && mpp_y) {
        info->resUnits = RES_um;
        info->xRes = xstring(mpp_x).toDouble();
        info->yRes = xstring(mpp_y).toDouble();
    }

    const char *background_color = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_BACKGROUND_COLOR);
    if (background_color) {
        par->background_color = background_color;
    }
}

void openslideCloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    if (fmtHndl->internalParams == NULL) return;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint openslideOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) openslideCloseImageProc(fmtHndl);
    bim::OpenSlideParams *par = new bim::OpenSlideParams();
    fmtHndl->internalParams = (void *)par;

    if (io_mode == IO_WRITE) {
        return 1;
    }

    try {
        par->open(fmtHndl->fileName, io_mode);
        openslideGetImageInfo(fmtHndl);
    } catch (...) {
        openslideCloseImageProc(fmtHndl);
        return 1;
    }

    return 0;
}


//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint openslideGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    return info->number_pages;
}

ImageInfo openslideGetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    ImageInfo ii = initImageInfo();
    if (fmtHndl == NULL) return ii;
    fmtHndl->pageNumber = page_num;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;
    if (conf && par->path != conf->getValue("-path")) {
        openslideGetImageInfo(fmtHndl);
    }
    return par->i;
}


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

bim::uint openslide_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (!hash) return 1;
    if (isCustomReading(fmtHndl)) return 1;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    xstring path;
    if (conf) path = conf->getValue("-path");
    if (path.size() > 0) {
        hash->set_value(bim::IMAGE_CURRENT_PATH, path);
        return 0;
    }

    const char *mpp_x = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_MPP_X);
    const char *mpp_y = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_MPP_Y);
    if (mpp_x && mpp_y) {
        hash->set_value(bim::PIXEL_RESOLUTION_X, mpp_x);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
        hash->set_value(bim::PIXEL_RESOLUTION_Y, mpp_y);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    hash->set_value(bim::IMAGE_NUM_SERIES, 1);

    hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_ARBITRARY);
    hash->set_value(bim::IMAGE_NUM_RES_L, (int)par->scales.size());
    hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(par->scales, ","));

    const char *val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_BACKGROUND_COLOR);
    if (val) {
        hash->set_value(bim::DOCUMENT_BACKGROUND_COLOR, val);
        xstring bg = val;
        if (bg == "000000")
            par->background_value = 0;
    }

    if (!conf || !conf->hasKeyWith("-meta")) return 0;

    // sub-image paths
    int sub_images = 0;
    hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), 0), "/image");
    const char *const *associated;
    for (associated = openslide_get_associated_image_names(par->osr); *associated != NULL; associated++) {
        xstring name = *associated;
        // paths would be like: /preview /label /thumbnail -> macro label thumbnail
        if (name == "label") {
            hash->set_value(bim::IMAGE_NUM_LABELS, 1);
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/label");
        } else if (name == "macro") {
            hash->set_value(bim::IMAGE_NUM_PREVIEWS, 1);
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), "/preview");
        } else {
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), ++sub_images), xstring("/") + name);
        }
    }

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_OBJECTIVE_POWER);
    if (val)
        hash->set_value(xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), 0) + bim::OBJECTIVE_MAGNIFICATION_X, val);

    // note OPENSLIDE_PROPERTY_NAME_COMMENT
    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_COMMENT);
    if (val)
        hash->set_value(bim::DOCUMENT_DESCRIPTION, val);

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_QUICKHASH1);
    if (val)
        hash->set_value(bim::DOCUMENT_QUICKHASH, val);

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_VENDOR);
    if (val) {
        hash->set_value(bim::DOCUMENT_VENDOR, val);
    }

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_BOUNDS_X);
    if (val) {
        hash->set_value(bim::DOCUMENT_BOUNDS_X, val);
    }

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_BOUNDS_Y);
    if (val) {
        hash->set_value(bim::DOCUMENT_BOUNDS_Y, val);
    }

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_BOUNDS_WIDTH);
    if (val) {
        hash->set_value(bim::DOCUMENT_BOUNDS_W, val);
    }

    val = openslide_get_property_value(par->osr, OPENSLIDE_PROPERTY_NAME_BOUNDS_HEIGHT);
    if (val) {
        hash->set_value(bim::DOCUMENT_BOUNDS_H, val);
    }

    const char *const *properties;
    for (properties = openslide_get_property_names(par->osr); *properties != NULL; properties++) {
        xstring prop = *properties;
        if (prop.startsWith("tiff.")) continue;
        if (prop.startsWith("openslide.")) continue;
        const char *value = openslide_get_property_value(par->osr, *properties);
        hash->set_value(prop.replace(".", "/"), value);
    }

    /*
    if (par->buffer_icc.size()>0)
        hash->set_value(bim::RAW_TAGS_ICC, par->buffer_icc, bim::RAW_TYPES_ICC);
    if (par->buffer_xmp.size()>0)
        hash->set_value(bim::RAW_TAGS_XMP, par->buffer_xmp, bim::RAW_TYPES_XMP);
    if (par->buffer_iptc.size()>0)
        hash->set_value(bim::RAW_TAGS_IPTC, par->buffer_iptc, bim::RAW_TYPES_IPTC);
    if (par->buffer_photoshop.size()>0)
        hash->set_value(bim::RAW_TAGS_PHOTOSHOP, par->buffer_photoshop, bim::RAW_TYPES_PHOTOSHOP);

    openslide_create_proper_exif(fmtHndl, hash); // parse and write proper EXIF block
    */

    // use LCMS2 to parse color profile
    lcms_append_metadata(fmtHndl, hash);

    // use EXIV2 to read metadata
    //#ifdef BIM_USE_EXIV2
    //exiv_append_metadata(fmtHndl, hash);
    //#endif

    return 0;
}

//----------------------------------------------------------------------------
// READ
//----------------------------------------------------------------------------

template<typename T>
void copy_ARGB_to_bitmap(bim::uint64 W, bim::uint64 H, void *buffer, ImageBitmap *bmp, bim::uint8 bgrd = 255) {
    int stride_out = W;
    int stride_in = W * 4;

    //#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (W > BIM_OMP_FOR2 && H > BIM_OMP_FOR2)
    for (bim::int64 y = 0; y < (bim::int64)H; ++y) {
        const T *pin = (const T *)((const char *)buffer + (y * stride_in));
        T *pr = ((T *)bmp->bits[0]) + (y * stride_out);
        T *pg = ((T *)bmp->bits[1]) + (y * stride_out);
        T *pb = ((T *)bmp->bits[2]) + (y * stride_out);
        //T *pa = ((T *)bmp->bits[3]) + (y*stride_out);
        for (bim::int64 x = 0; x < (bim::int64)W; ++x) {
            //pa[x] = pin[3];
            if (pin[3] == 255) {
                pr[x] = pin[2];
                pg[x] = pin[1];
                pb[x] = pin[0];
            } else if (pin[3] == 0) {
                pr[x] = bgrd;
                pg[x] = bgrd;
                pb[x] = bgrd;
            } else {
                pr[x] = bim::trim<int>(bim::round<double>(255.0 * pin[2] / pin[3]), 0, 255);
                pg[x] = bim::trim<int>(bim::round<double>(255.0 * pin[1] / pin[3]), 0, 255);
                pb[x] = bim::trim<int>(bim::round<double>(255.0 * pin[0] / pin[3]), 0, 255);
            }
            pin += 4;
        }
    } // for y
}

bim::uint openslideReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    if (conf && conf->keyExists("-path")) {
        // paths would be like: /preview /label /thumbnail -> macro label thumbnail
        xstring path = conf->getValue("-path").replace("/", "").replace("preview", "macro");
        int64_t w;
        int64_t h;
        openslide_get_associated_image_dimensions(par->osr, path.c_str(), &w, &h);
        info->width = w;
        info->height = h;
        if (w < 1 || h < 1) return 1;

        ImageBitmap *bmp = fmtHndl->image;
        if (allocImg(fmtHndl, info, bmp) != 0) return 1;

        std::vector<uint32_t> buffer(w * h * 4);
        openslide_read_associated_image(par->osr, path.c_str(), &buffer[0]);
        copy_ARGB_to_bitmap<bim::uint8>(info->width, info->height, (void *)&buffer[0], bmp, par->background_value);
    }

    /* // dima, skip reading the whole image at once
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    std::vector<uint32_t> buffer(info->width * info->height * 4);
    openslide_read_region(par->osr, &buffer[0], 0, 0, 0, info->width, info->height);
    copy_ARGB_to_bitmap<bim::uint8>(info->width, info->height, (void*)&buffer[0], bmp, par->background_value);
    */

    return 0;
}

bim::uint openslideReadImageLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    double scale = par->scales[level];

    // setting resolution level does not change image size in opj_decode, a little hack to
    // update image size based on the resolution level
    info->width = floor((double)par->image_width * scale);
    info->height = floor((double)par->image_height * scale);
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    std::vector<uint32_t> buffer(info->width * info->height * 4);
    openslide_read_region(par->osr, &buffer[0], 0, 0, level, info->width, info->height);
    copy_ARGB_to_bitmap<bim::uint8>(info->width, info->height, (void *)&buffer[0], bmp, par->background_value);

    return 0;
}

bim::uint openslideReadImageTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    double downsample = par->downsamples[level];
    double scale = par->scales[level];
    int num_tiles_x = int(ceil(par->image_width * scale / double(info->tileWidth)));
    int num_tiles_y = int(ceil(par->image_height * scale / double(info->tileHeight)));
    if (xid >= num_tiles_x || yid >= num_tiles_y) return 1;

    // compute sizes and position in the scaled space
    int scaled_iw = int(floor(par->image_width * scale));
    int scaled_ih = int(floor(par->image_height * scale));
    int scaled_x = xid * bim::OpenSlideParams::virtual_tile_width;
    int scaled_y = yid * bim::OpenSlideParams::virtual_tile_width;
    int scaled_tw = bim::min<bim::uint64>(bim::OpenSlideParams::virtual_tile_width, scaled_iw - scaled_x);
    int scaled_th = bim::min<bim::uint64>(bim::OpenSlideParams::virtual_tile_height, scaled_ih - scaled_y);

    // compute tile position in the 100% image as required by libCZI
    int64_t x = bim::round<int64_t>(scaled_x / scale);
    int64_t y = bim::round<int64_t>(scaled_y / scale);
    int64_t w = bim::round<int64_t>(scaled_tw / scale);
    int64_t h = bim::round<int64_t>(scaled_th / scale);

    // allocate output image
    info->width = scaled_tw;
    info->height = scaled_th;
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    std::vector<uint32_t> buffer(info->width * info->height * 4);
    openslide_read_region(par->osr, &buffer[0], x, y, level, info->width, info->height);
    copy_ARGB_to_bitmap<bim::uint8>(info->width, info->height, (void *)&buffer[0], bmp, par->background_value);

    return 0;
}

bim::uint openslideReadImageRegionProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::OpenSlideParams *par = (bim::OpenSlideParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    // compute sizes and position in the scaled space
    double downsample = par->downsamples[level];
    double scale = par->scales[level];
    int scaled_iw = int(floor(par->image_width * scale));
    int scaled_ih = int(floor(par->image_height * scale));
    int scaled_x = x1;
    int scaled_y = y1;
    int scaled_tw = bim::min<bim::uint64>(x2 - x1 + 1, scaled_iw - scaled_x);
    int scaled_th = bim::min<bim::uint64>(y2 - y1 + 1, scaled_ih - scaled_y);

    // compute tile position in the 100% image as required by libCZI
    int x = bim::round<int>(scaled_x / scale);
    int y = bim::round<int>(scaled_y / scale);
    int w = bim::round<int>(scaled_tw / scale);
    int h = bim::round<int>(scaled_th / scale);

    // allocate output image
    info->width = scaled_tw;
    info->height = scaled_th;
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    std::vector<uint32_t> buffer(info->width * info->height * 4);
    openslide_read_region(par->osr, &buffer[0], x, y, level, info->width, info->height);
    copy_ARGB_to_bitmap<bim::uint8>(info->width, info->height, (void *)&buffer[0], bmp, par->background_value);

    return 0;
}

//****************************************************************************
// exported
//****************************************************************************

#define BIM_OPENSLIDE_NUM_FORMATS 8

FormatItem openslideItems[BIM_OPENSLIDE_NUM_FORMATS] = {
    {                       //0
      "APERIO",             // short name, no spaces
      "Wholeslide: Aperio", // Long format name
      "svs|tif",            // pipe "|" separated supported extension list
      1,                    //canRead;      // 0 - NO, 1 - YES
      0,                    //canWrite;     // 0 - NO, 1 - YES
      1,                    //canReadMeta;  // 0 - NO, 1 - YES
      0,                    //canWriteMeta; // 0 - NO, 1 - YES
      0,                    //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                          //1
      "HAMAMATSU",             // short name, no spaces
      "Wholeslide: Hamamatsu", // Long format name
      "ndpi|vms|vmu",          // pipe "|" separated supported extension list
      1,                       //canRead;      // 0 - NO, 1 - YES
      0,                       //canWrite;     // 0 - NO, 1 - YES
      1,                       //canReadMeta;  // 0 - NO, 1 - YES
      0,                       //canWriteMeta; // 0 - NO, 1 - YES
      0,                       //canWriteMultiPage;   // 0 - NO, 1 - YES
                               //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                      //2
      "LEICA",             // short name, no spaces
      "Wholeslide: Leica", // Long format name
      "scn",               // pipe "|" separated supported extension list
      1,                   //canRead;      // 0 - NO, 1 - YES
      0,                   //canWrite;     // 0 - NO, 1 - YES
      1,                   //canReadMeta;  // 0 - NO, 1 - YES
      0,                   //canWriteMeta; // 0 - NO, 1 - YES
      0,                   //canWriteMultiPage;   // 0 - NO, 1 - YES
                           //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                      //3
      "MIRAX",             // short name, no spaces
      "Wholeslide: MIRAX", // Long format name
      "mrxs",              // pipe "|" separated supported extension list
      1,                   //canRead;      // 0 - NO, 1 - YES
      0,                   //canWrite;     // 0 - NO, 1 - YES
      1,                   //canReadMeta;  // 0 - NO, 1 - YES
      0,                   //canWriteMeta; // 0 - NO, 1 - YES
      0,                   //canWriteMultiPage;   // 0 - NO, 1 - YES
                           //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                        //4
      "PHILIPS",             // short name, no spaces
      "Wholeslide: Philips", // Long format name
      "tiff",                // pipe "|" separated supported extension list
      1,                     //canRead;      // 0 - NO, 1 - YES
      0,                     //canWrite;     // 0 - NO, 1 - YES
      1,                     //canReadMeta;  // 0 - NO, 1 - YES
      0,                     //canWriteMeta; // 0 - NO, 1 - YES
      0,                     //canWriteMultiPage;   // 0 - NO, 1 - YES
                             //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                       //5
      "SAKURA",             // short name, no spaces
      "Wholeslide: Sakura", // Long format name
      "svslide",            // pipe "|" separated supported extension list
      1,                    //canRead;      // 0 - NO, 1 - YES
      0,                    //canWrite;     // 0 - NO, 1 - YES
      1,                    //canReadMeta;  // 0 - NO, 1 - YES
      0,                    //canWriteMeta; // 0 - NO, 1 - YES
      0,                    //canWriteMultiPage;   // 0 - NO, 1 - YES
                            //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                        //6
      "TRESTLE",             // short name, no spaces
      "Wholeslide: Trestle", // Long format name
      "tif",                 // pipe "|" separated supported extension list
      1,                     //canRead;      // 0 - NO, 1 - YES
      0,                     //canWrite;     // 0 - NO, 1 - YES
      1,                     //canReadMeta;  // 0 - NO, 1 - YES
      0,                     //canWriteMeta; // 0 - NO, 1 - YES
      0,                     //canWriteMultiPage;   // 0 - NO, 1 - YES
                             //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                        //7
      "VENTANA",             // short name, no spaces
      "Wholeslide: Ventana", // Long format name
      "bif|tif",             // pipe "|" separated supported extension list
      1,                     //canRead;      // 0 - NO, 1 - YES
      0,                     //canWrite;     // 0 - NO, 1 - YES
      1,                     //canReadMeta;  // 0 - NO, 1 - YES
      0,                     //canWriteMeta; // 0 - NO, 1 - YES
      0,                     //canWriteMultiPage;   // 0 - NO, 1 - YES
                             //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } }
};

FormatHeader openslideHeader = {

    sizeof(FormatHeader),
    "3.4.1",
    "OpenSlide",
    "OpenSlide",

    BIM_FORMAT_OPENSLIDE_MAGIC_SIZE,
    { 1, BIM_OPENSLIDE_NUM_FORMATS, openslideItems },

    openslideValidateFormatProc,
    // begin
    openslideAquireFormatProc, //AquireFormatProc
    // end
    openslideReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    openslideOpenImageProc,  //OpenImageProc
    openslideCloseImageProc, //CloseImageProc

    // info
    openslideGetNumPagesProc,  //GetNumPagesProc
    openslideGetImageInfoProc, //GetImageInfoProc

    // read/write
    openslideReadImageProc,       //ReadImageProc
    NULL,                         //WriteImageProc
    openslideReadImageTileProc,   //ReadImageTileProc
    NULL,                         //WriteImageTileProc
    openslideReadImageLevelProc,  //ReadImageLevelProc
    NULL,                         //WriteImageLineProc
    NULL,                         //ReadImageThumbProc
    NULL,                         //WriteImageThumbProc
    NULL,                         //dimJpegReadImagePreviewProc, //ReadImagePreviewProc
    openslideReadImageRegionProc, //ReadImageRegionProc
    NULL,                         //WriteImageRegionProc

    // meta data
    NULL,                      //ReadMetaDataAsTextProc
    openslide_append_metadata, //AppendMetaDataProc

    NULL,
    NULL,
    ""

};

extern "C" {

FormatHeader *openslideGetFormatHeader(void) {
    return &openslideHeader;
}

} // extern C
