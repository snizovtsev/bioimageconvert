/*****************************************************************************
  Nikon ND2 support
  Copyright (c) 2019, ViQI Inc

  Author: Dmitry Fedorov <mailto:dima@viqi.org> <http://www.viqi.org/>

  History:
  2019-05-23 11:50:40 - First creation

  ver : 1
  *****************************************************************************/

#ifndef BIM_ND2_FORMAT_H
#define BIM_ND2_FORMAT_H

#include <cstdio>
#include <fstream>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *nd2GetFormatHeader(void);
}

namespace bim {

//****************************************************************************
// ND2 format definitions
//****************************************************************************

#define BIM_FORMAT_ND2_MAGIC_SIZE 4
const unsigned char nd2_magic_number[4] = { 0xDA, 0xCE, 0xBE, 0x0A };
const char nd2_signature_chunkmap[33] = "ND2 CHUNK MAP SIGNATURE 0000001!";

#pragma pack(push, 1)
struct ND2_CHUNK_METADATA {
    uint32 magic;
    uint32 relative_offset;
    uint64 data_length;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ND2_METADATA_ENTRY {
    uint8 data_type;
    uint8 name_length;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ND2_METADATA_ITEM {
    uint32 count;
    uint64 length;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ND2_CHUNKMAP_END {
    char header[32];
    uint64 offset;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ND2_LABEL {
    uint32 data_offset;
    uint64 data_length;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ND2_FILEMAP_ENTRY {
    uint64 offset;
    uint64 offset2;
};
#pragma pack(pop)

//----------------------------------------------------------------------------
// ND2Params - decoding params
//----------------------------------------------------------------------------

class ND2Params {
    enum DataType {
        dtUnknown = 0,
        dtUInt8 = 1,
        dtInt32 = 2,
        dtUInt32 = 3,
        dtInt64 = 4,
        dtUInt64 = 5,
        dtFloat64 = 6,
        dtPointer = 7, // uint64
        dtString = 8,
        dtArray = 9,            // bytes
        dtLevelDeprectaed = 10, // deprecated
        dtLevel = 11,
    };

    enum Modality {
        mWideField = 0,
        mBrightField = 1,
        mLaserScanConfocal = 2,
        mSpinningDiskConfocal = 3,
        mSweptFieldConfocal = 4,
        mMultiPhoton = 5,
    };

public:
    ND2Params();
    ~ND2Params();

    ImageInfo i;
    bim::TagMap attributes;
    FormatHandle *fmtHndl = 0;

    int supported_version = 3;
    int version_major = 0;
    int version_minor = 0;
    xstring version_str;
    bim::uint offset_first_label = 4096;
    int line_stride_bytes = 0;
    int sequence_count = 0;
    int compressionParam = 0;
    int compression = 0;
    int bpcSignificant = 0;
    int tileHeight = 0;
    int tileWidth = 0;
    int virtualComponents = 0;

    std::vector<unsigned int> number_dims; // T M(FOV) Z Unkown

    bim::TagMap label_offsets;
    std::vector<ND2_LABEL> ImageDataSeq;

    void open(FormatHandle *fmtHndl, bim::ImageIOModes mode);

    bool check_version();
    void parse_filemap();
    void read_label(const bim::uint64 &offset, ND2_LABEL &label);
    void read_label_data(const ND2_LABEL &label, std::vector<bim::uint8> &buffer);

    void read_image_metadata();
    void read_all_metadata();

    void parse_metadata_hierarchical(const xstring &name, const xstring &path);
    void parse_metadata_xml(const xstring &name, const xstring &path);
    void parse_metadata_ndcontrol_v1();

    void parse_image_metadata(std::vector<bim::uint8> &buffer, xstring path, unsigned int &pos, int max_count = 100000000);
    void parse_metadata_value(std::vector<bim::uint8> &buffer, xstring path, unsigned int &pos, int data_type);

    void read_image(const bim::uint64 &idx, ImageInfo *info, std::vector<bim::uint8> &buffer);
};

} // namespace bim

#endif // BIM_ND2_FORMAT_H
