/*****************************************************************************
  HDF5 support
  Copyright (c) 2018, Center for Bio-Image Informatics, UCSB
  Copyright (c) 2019, ViQi Inc

  Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
  2018-04-10 09:12:40 - First creation

  ver : 1
  *****************************************************************************/

#ifndef BIM_HDF5_FORMAT_H
#define BIM_HDF5_FORMAT_H

#include <cstdio>
#include <fstream>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

#include <H5Tpublic.h>
#include <H5Cpp.h>

namespace H5 {
    //class H5File;
    //class CompType;
    //class DataSet;
    //class DataType;
    //enum H5T_class_t;
}

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *hdf5GetFormatHeader(void);
}

namespace bim {

static const int HDF5_GUESS_MAX_CHANNELS = 6;
static const int VIQI_MAX_ITEMS_PER_BLOCK = 1000000;

// sub-formats
enum HDF5SubFormat {
    FMT_HDF5 = 0,
    FMT_IMS = 1,
    FMT_DREAM3D = 2,
    FMT_VIQI = 3
};

//----------------------------------------------------------------------------
// VQITable
//----------------------------------------------------------------------------

class VQITableColumn {
public:
    VQITableColumn(const H5::CompType &cmpt, int i);
    ~VQITableColumn();

    int idx=0;
    bim::xstring name;
    H5::DataType dt;
    H5T_class_t ct;
    size_t offset = 0;
    size_t size_bytes = 0;
    //H5::DataType dt;
    bool visualizable = false;
};

class VQITableMeasure {
public:
    VQITableMeasure(const VQITableColumn &col, const xstring &name, int idx = -1);
    ~VQITableMeasure();

    int idx = 0;
    int idx_column = 0;
    bim::xstring name;
    bim::xstring name_long;
    bim::xstring column;
    int class_id = -1;
    H5::DataType dt;
    H5T_class_t ct;
    size_t offset = 0;
    size_t size_bytes = 0;
    bim::Image::ResizeMethod interpolation = bim::Image::szBiCubic; // bim::Image::szNearestNeighbor //bim::Image::szLanczos
    bool values_are_class_id = false;
};

class VQITable {
public:
    VQITable();
    VQITable(H5::H5File *file, const bim::xstring &path);
    ~VQITable();

    void init(H5::H5File *file, const bim::xstring &path);
    void load_column_info(const H5::DataSet &dataset);
    void init_measures();

    H5::H5File *file;
    
    std::vector<VQITableColumn> columns;
    std::vector<VQITableMeasure> measures;
    hsize_t nrows = 0;
    hsize_t row_sz = 0;
    int column_id = -1;
    int column_measure = -1;

    bim::xstring path;
    bim::xstring col_object_id;
    bim::xstring col_class_label;
    bim::xstring col_class_confidence;
    bim::xstring name_measure;

    int class_label_offset = -1;
    H5T_class_t class_label_ct;
    int class_label_bytes=0;
    std::map<unsigned int, xstring> class_id_to_label_mapping;

    unsigned int measure_max_id = 0;
    unsigned int last_row = 0;
    std::vector<bim::uint8> measure_buffer;
    std::map<unsigned int, unsigned int> measure_index; // mapping of measure index to initial offset in the measure_buffer

    VQITableColumn* getColumn(const xstring &name) const;
    VQITableMeasure* getMeasure(const xstring &name) const;
    VQITableMeasure* getMeasure(int measure_id) const;
    bool hasMeasures() const {
        return this->measures.size() > 0;
    }

    void append_metadata(TagMap *hash) const;

    void load_measures(const xstring &measure_name, int object_id);
    void load_measures(int measure_id, int object_id);

    void parse_measures(); // update measure index map

    double get_measure_value(int object_id, const xstring &measure_name, const double &def = 0);
    double get_measure_value(int object_id, int measure_id, const double &def = 0);

    template<typename T>
    unsigned int get_row_objectid(unsigned int offset);
};

//----------------------------------------------------------------------------
// HDF5Params - decoding params
//----------------------------------------------------------------------------

class HDF5Params {
public:
    HDF5Params();
    ~HDF5Params();

    ImageInfo i;
    H5::H5File *file = NULL;
    bim::xstring file_path;
    bim::xstring path;
    bim::xstring path_requested;

    bool image_class = false;
    bool interlaced = false;

    bim::xstring viqi_image_content;
    std::vector<int> viqi_image_size;
    std::vector<bim::xstring> viqi_image_dimensions;
    std::vector<int> dimensions_image;
    std::vector<int> dimensions_tile;
    std::vector<int> dimensions_element;
    std::vector<int> dimensions_indices;
    std::vector<int> positions;
    int num_spatial_dimensions = 2;
    int image_num_samples = 1;

    int resolution_levels = 1;
    std::vector<double> zscales;
    std::vector<double> scales;
    int hdf_pixel_format = -1;

    int block_id = -1;
    int item_id = -1;

    bool sparse_mode = false;
    bool sparse_element_read = false;
    
    bool mask_mode = false;
    bool values_from_measures = false;

    std::vector<int> position_spectrum; // method to define bands of interest to produce a final image with N channels
    std::vector<int> spectral_wavelengths;
    std::string spectral_wavelength_units;
    VQITable table;

    bim::Image::ResizeMethod interpolation = bim::Image::szBiCubic; // bim::Image::szNearestNeighbor //bim::Image::szLanczos
    bool user_given_interpolation = false;

    int num_blocks = 0;
    int num_items = 0;
    std::vector<int> block_bboxs;
    int loaded_level = -1;

    //std::vector<char> buffer_icc;

    void open(const char *filename, bim::ImageIOModes mode);
    void close();
};

} // namespace bim

#endif // BIM_HDF5_FORMAT_H
