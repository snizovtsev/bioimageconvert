/*****************************************************************************
HDF5 support
Copyright (c) 2018, Center for Bio-Image Informatics, UCSB
Copyright (c) 2019, ViQi Inc

VQI is a proprietary format and requires licese from ViQi Inc

Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
2018-04-10 09:12:40 - First creation

ver : 1
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <random>

#include <Jzon.h>
//#include <pugixml.hpp>

#include <bim_format_misc.h>
#include <bim_image.h>
#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xconf.h>
#include <xstring.h>
#include <xtypes.h>
#include <xunits.h>

#include "bim_hdf5_format.h"

//#include <hdf5.h>
#include <H5Cpp.h>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

using namespace bim;


bim::xstring hdf5_get_attribute_string(H5::H5Object &dataset, const std::string &name, const bim::xstring &def = "");
void viqi_init_coordinate(FormatHandle *fmtHndl, int x = 0, int y = 0, int sample = 0);

//----------------------------------------------------------------------------
// misc
//----------------------------------------------------------------------------

bim::Image::ResizeMethod str_to_interpolation(const xstring &s) {
    Image::ResizeMethod resize_method = Image::szNearestNeighbor;
    xstring ss = s.toLowerCase();
    if (ss == "nn") resize_method = Image::szNearestNeighbor;
    else if (ss == "bl") resize_method = Image::szBiLinear;
    else if (ss == "bc") resize_method = Image::szBiCubic;
    else if (ss == "lanczos") resize_method = Image::szLanczos;
    else if (ss == "bessel") resize_method = Image::szBessel;
    else if (ss == "point") resize_method = Image::szPoint;
    else if (ss == "box") resize_method = Image::szBox;
    else if (ss == "hermite") resize_method = Image::szHermite;
    else if (ss == "hanning") resize_method = Image::szHanning;
    else if (ss == "hamming") resize_method = Image::szHamming;
    else if (ss == "blackman") resize_method = Image::szBlackman;
    else if (ss == "gaussian") resize_method = Image::szGaussian;
    else if (ss == "quadratic") resize_method = Image::szQuadratic;
    else if (ss == "catrom") resize_method = Image::szCatrom;
    else if (ss == "mitchell") resize_method = Image::szMitchell;
    else if (ss == "sinc") resize_method = Image::szSinc;
    else if (ss == "blackmanbessel") resize_method = Image::szBlackmanBessel;
    else if (ss == "blackmansinc") resize_method = Image::szBlackmanSinc;
    return resize_method;
}

xstring interpolation_to_str(bim::Image::ResizeMethod resize_method) {
    xstring ss = "nn";
    if (resize_method == Image::szNearestNeighbor) ss = "nn";
    else if (resize_method == Image::szBiLinear) ss = "bl";
    else if (resize_method == Image::szBiCubic) ss = "bc";
    else if (resize_method == Image::szLanczos) ss = "lanczos";
    else if (resize_method == Image::szBessel) ss = "bessel";
    else if (resize_method == Image::szPoint) ss = "point";
    else if (resize_method == Image::szBox) ss = "box";
    else if (resize_method == Image::szHermite) ss = "hermite";
    else if (resize_method == Image::szHanning) ss = "hanning";
    else if (resize_method == Image::szHamming) ss = "hamming";
    else if (resize_method == Image::szBlackman) ss = "blackman";
    else if (resize_method == Image::szGaussian) ss = "gaussian";
    else if (resize_method == Image::szQuadratic) ss = "quadratic";
    else if (resize_method == Image::szCatrom) ss = "catrom";
    else if (resize_method == Image::szMitchell) ss = "mitchell";
    else if (resize_method == Image::szSinc) ss = "sinc";
    else if (resize_method == Image::szBlackmanBessel) ss = "blackmanbessel";
    else if (resize_method == Image::szBlackmanSinc) ss = "blackmansinc";
    return ss;
}


//****************************************************************************
// bim::HDF5Params
//****************************************************************************

bim::HDF5Params::HDF5Params() {
    i = initImageInfo();
    this->dimensions_image.resize(BIM_MAX_DIMS, 1);
    this->dimensions_tile.resize(BIM_MAX_DIMS, 0);
    this->dimensions_element.resize(BIM_MAX_DIMS, 1);
    this->positions.resize(BIM_MAX_DIMS, 0);
    this->dimensions_indices.resize(BIM_MAX_DIMS, bim::DIM_C);
}

bim::HDF5Params::~HDF5Params() {
    this->close();
}

void bim::HDF5Params::open(const char *filename, bim::ImageIOModes io_mode) {
    H5::Exception::dontPrint();
    this->file_path = bim::fspath_get_path(filename);
    if (io_mode == IO_READ) {
        this->file = new H5::H5File(filename, H5F_ACC_RDONLY);
    } else if (io_mode == IO_WRITE) {
        //this->file = new H5::H5File(filename, H5F_ACC_RDONLY);
    }
}

void bim::HDF5Params::close() {
    if (this->file) {
        this->file->close();
        delete this->file;
        this->file = NULL;
    }
}


//----------------------------------------------------------------------------
// VQITableColumn
//----------------------------------------------------------------------------

VQITableColumn::VQITableColumn(const H5::CompType &cmpt, int i) {
    this->idx = i;
    this->ct = cmpt.getMemberClass(i);
    this->offset = cmpt.getMemberOffset(i);
    //H5std_string cmp_name = cmpt.getMemberName(i); // under windows exporting std stuff from dlls may not work due to version clashes
    char *cmp_name = H5Tget_member_name(cmpt.getId(), i);
    this->name = cmp_name;

    this->dt = cmpt.getMemberDataType(i);
    this->size_bytes = this->dt.getSize();
    this->visualizable = false;
}

VQITableColumn::~VQITableColumn() {

}

//----------------------------------------------------------------------------
// VQITableMeasure
//----------------------------------------------------------------------------

VQITableMeasure::VQITableMeasure(const VQITableColumn &col, const xstring &name, int idx) {
    this->idx = idx;
    this->idx_column = col.idx;
    this->name = name.size() > 0 ? name : col.name;
    this->column = col.name;
    this->ct = col.ct;
    this->dt = col.dt;
    this->offset = col.offset;
    this->size_bytes = col.size_bytes;
}

VQITableMeasure::~VQITableMeasure() {

}

//----------------------------------------------------------------------------
// VQITable
//----------------------------------------------------------------------------

VQITable::VQITable() {

}

VQITable::VQITable(H5::H5File *file, const bim::xstring &path) {
    this->init(file, path);
}

VQITable::~VQITable() {

}

void VQITable::init(H5::H5File *file, const bim::xstring &path) {
    this->file = file;
    this->path = path;
    xstring path_table = path + "/table";
    try {
        H5::DataSet dataset = file->openDataSet(path_table.c_str());
        H5::Group group = file->openGroup(path.c_str());
        
        this->col_object_id = hdf5_get_attribute_string(group, "viqi_columns_id");
        this->col_class_label = hdf5_get_attribute_string(group, "viqi_columns_class");
        this->col_class_confidence = hdf5_get_attribute_string(group, "viqi_columns_confidence");

        std::vector<bim::xstring> vis = hdf5_get_attribute_string(group, "viqi_columns_vis").replace(" ", "").toLowerCase().split(",");

        std::vector<bim::xstring> label_mapping = hdf5_get_attribute_string(group, "viqi_class_label_mapping").split(",");
        for (std::vector<bim::xstring>::iterator it = label_mapping.begin(); it != label_mapping.end(); ++it) {
            std::vector<bim::xstring> v = it->split(":");
            if (v.size() > 1) {
                this->class_id_to_label_mapping[v[0].toInt()] = v[1];
            }
        }
        
        H5::DataType dt = dataset.getDataType();
        H5T_class_t ct = dt.getClass();
        size_t sz = dt.getSize();
        this->row_sz = sz;

        H5::DataSpace dataspace = dataset.getSpace();
        int rank = dataspace.getSimpleExtentNdims();
        std::vector<hsize_t> dims(rank);
        dataspace.getSimpleExtentDims(&dims[0]);
        
        if (ct != H5T_COMPOUND) return;

        this->load_column_info(dataset);

        int szz = std::min(this->columns.size(), vis.size());
        for (int i = 0; i < szz; ++i) {
            this->columns[i].visualizable = (vis[i] == "true");
        }
        
        this->nrows = dims[0];
        this->init_measures();

    } catch (H5::FileIException error) {
        return;
    }
}

void VQITable::load_column_info(const H5::DataSet &dataset) {
    H5::DataType dt = dataset.getDataType();
    H5T_class_t ct = dt.getClass();
    if (ct != H5T_COMPOUND) return;

    H5::CompType cmpt = H5::CompType(dataset);
    int num_members = cmpt.getNmembers();
    this->columns.reserve(num_members);
    for (unsigned int i = 0; i < num_members; ++i) {
        VQITableColumn c(cmpt, i);
        this->columns.push_back(c);
        if (c.name == this->col_object_id)
            this->column_id = i;
    }
}

VQITableColumn* VQITable::getColumn(const xstring &name) const {
    for (unsigned int i = 0; i < this->columns.size(); ++i) {
        if (this->columns[i].name == name)
            return (VQITableColumn*) &(this->columns[i]);
    }
    return NULL;
}

VQITableMeasure* VQITable::getMeasure(const xstring &name) const {
    for (unsigned int i = 0; i < this->measures.size(); ++i) {
        if (this->measures[i].name == name)
            return (VQITableMeasure*) &(this->measures[i]);
    }
    return NULL;
}

VQITableMeasure* VQITable::getMeasure(int measure_id) const {
    if (measure_id >= this->measures.size()) return NULL;
    return (VQITableMeasure*) &(this->measures[measure_id]);
}

void VQITable::init_measures() {
    this->measures.reserve(this->columns.size());
    int measure_id = -1;

    // measures will always have an id randomizer measure
    VQITableColumn *col = this->getColumn(this->col_object_id);
    if (col) {
        VQITableMeasure m(*col, xstring::xprintf("%s_random", this->col_object_id.c_str()), ++measure_id);
        m.interpolation = bim::Image::szNearestNeighbor;
        this->measures.push_back(m);

        //VQITableMeasure m1(*col, this->col_object_id, ++measure_id);
        //this->measures.push_back(m1);
    }

    // if class label and classes present
    if (this->col_class_label.size() > 0 && this->class_id_to_label_mapping.size() > 0) {
        
        col = this->getColumn(this->col_class_label);
        if (col) {
            VQITableMeasure m(*col, "class_labels", ++measure_id);
            m.interpolation = bim::Image::szNearestNeighbor;
            m.values_are_class_id = true;
            this->measures.push_back(m);

            this->class_label_offset = col->offset;
            this->class_label_ct = col->ct;
            this->class_label_bytes = col->size_bytes;
        }

        // if confidence
        col = this->getColumn(this->col_class_confidence);
        if (col) {
            for (std::map<unsigned int, xstring>::iterator it = this->class_id_to_label_mapping.begin(); it != this->class_id_to_label_mapping.end(); ++it) {
                xstring name = xstring::xprintf("%s.class_id.%d", this->col_class_confidence.c_str(), it->first);
                VQITableMeasure m(*col, name, ++measure_id);
                m.class_id = it->first;
                this->measures.push_back(m);
            }
        }
    }

    // all measures exposed in the table
    for (unsigned int i = 0; i < this->columns.size(); ++i) {
        if (this->columns[i].visualizable) {
            VQITableMeasure m(this->columns[i], "", ++measure_id);
            this->measures.push_back(m);
        }
    }
}

void VQITable::append_metadata(TagMap *hash) const {
    if (this->measures.size() < 1) return;
    hash->set_value(bim::IMAGE_NUM_MEASURES, (int) this->measures.size());

    for (unsigned int i = 0; i < this->measures.size(); ++i) {
        int idx = this->measures[i].idx;
        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_NAME.c_str(), idx), this->measures[i].name);

        H5::DataType dt = this->measures[i].dt;
        xstring fmt;
        if (dt.getClass() == H5T_INTEGER) fmt += "int";
        if (dt.getClass() == H5T_FLOAT) fmt += "float";
        fmt += xstring::xprintf("%d", dt.getSize()*8);
        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_FORMAT.c_str(), idx), fmt);

        //DECLARE_STR(MEASURE_TEMPLATE_RANGE, "measures/measure:%d/range")
        //hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_NAME.c_str(), idx), this->measures[i].name);

        hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_INTERPOLATION.c_str(), idx), interpolation_to_str(this->measures[i].interpolation));
        if (this->measures[i].values_are_class_id)
            hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_VALUES_LABEL_IDS.c_str(), idx), true);

        if (this->measures[i].class_id >= 0) {
            hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_CLASS_ID.c_str(), idx), this->measures[i].class_id);
            
            std::map<unsigned int, xstring>::const_iterator it = this->class_id_to_label_mapping.find(this->measures[i].class_id);
            if (it != this->class_id_to_label_mapping.end()) {
                hash->set_value(xstring::xprintf(bim::MEASURE_TEMPLATE_CLASS_LABEL.c_str(), idx), it->second);
            }
        }
    }

    int i = 0;
    for (std::map<unsigned int, xstring>::const_iterator it = this->class_id_to_label_mapping.begin(); it != this->class_id_to_label_mapping.end(); ++it) {
        hash->set_value(xstring::xprintf(bim::CLASS_LABELS_TEMPLATE_NAME.c_str(), i), it->second);
        hash->set_value(xstring::xprintf(bim::CLASS_LABELS_TEMPLATE_ID.c_str(), i), it->first);
        ++i;
    }
}

void VQITable::load_measures(const xstring &measure_name, int object_id) {
    VQITableMeasure *m = this->getMeasure(measure_name);
    if (m) {
        this->load_measures(m->idx, object_id);
    }
}

void VQITable::load_measures(int measure_id, int object_id) {
    if (this->measure_max_id>0 && this->measure_max_id >= object_id) return;

    // load starting at offset
    int rows_read = std::min<int>(1000, this->nrows - this->last_row);
    if (rows_read == 0) return;
    
    xstring path_table = this->path + "/table";
    try {
        H5::DataSet dataset = this->file->openDataSet(path_table.c_str());

        H5::DataType dt = dataset.getDataType();
        //H5T_class_t ct = dt.getClass();
        //size_t sz = dt.getSize();

        H5::DataSpace dataspace = dataset.getSpace();
        int rank = dataspace.getSimpleExtentNdims();
        std::vector<hsize_t> dims(rank);
        dataspace.getSimpleExtentDims(&dims[0]);

        // Define the memory dataspace
        H5::DataSpace memspace(1, &dims[0]);

        // Define memory hyperslab
        std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
        std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
        count_out[0] = rows_read;
        memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

        std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
        std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
        offset[0] = this->last_row;
        count[0] = rows_read;
        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // read measures in blocks of 1K rows until fetched the one needed
        int inc = this->row_sz * rows_read;
        int buf_offset = this->measure_buffer.size();
        this->measure_buffer.resize(buf_offset + inc, 0);
        
        // Read data from hyperslab in the file into the hyperslab in memory
        //dataset.read(&data[0], dt, memspace, dataspace);
        dataset.read(&this->measure_buffer[0] + buf_offset, dt, memspace, dataspace);
    } catch (H5::FileIException error) {
        return;
    }

    // update objects id and offset map
    this->parse_measures();
        
    this->last_row += rows_read;
}

template<typename T>
unsigned int VQITable::get_row_objectid(unsigned int offset) {
    T object_id = *(T*) (&this->measure_buffer[0] + offset); //dima: might need to template this, based on table defs
    return object_id;
}

void VQITable::parse_measures() {
    unsigned int offset = this->last_row * this->row_sz;
    while (offset < this->measure_buffer.size()) {
        //bim::uint32 object_id = *(bim::uint32*) (&this->measure_buffer[0] + offset); //dima: might need to template this, based on table defs
        unsigned int object_id = this->get_row_objectid<bim::uint32>(offset); //dima: might need to template this, based on table defs
        this->measure_max_id = object_id;
        measure_index[object_id] = offset;
        offset += this->row_sz;
    }
}

double VQITable::get_measure_value(int object_id, const xstring &measure_name, const double &def) {
    VQITableMeasure *m = this->getMeasure(measure_name);
    if (m) {
        return this->get_measure_value(object_id, m->idx, def);
    }
    return def;
}

double VQITable::get_measure_value(int object_id, int measure_id, const double &def) {
    this->load_measures(measure_id, object_id);

    std::map<unsigned int, unsigned int>::const_iterator it = this->measure_index.find(object_id);
    if (it == this->measure_index.end()) return def;

    VQITableMeasure *m = &this->measures[measure_id];
    unsigned int offset = (*it).second;
    double v = 0;

    // if class selection is required
    if (m->class_id >= 0 && this->class_label_offset >= 0) {
        bim::uint8 *p = &this->measure_buffer[0] + offset + this->class_label_offset;

        if (this->class_label_ct == H5T_INTEGER && this->class_label_bytes == 1) {
            v = *(bim::uint8*) p;
        } else if (this->class_label_ct == H5T_INTEGER && this->class_label_bytes == 2) {
            v = *(bim::uint16*) p;
        } else if (this->class_label_ct == H5T_INTEGER && this->class_label_bytes == 4) {
            v = *(bim::uint32*) p;
        } else if (this->class_label_ct == H5T_INTEGER && this->class_label_bytes == 8) {
            v = *(bim::uint64*) p;
        } else if (this->class_label_ct == H5T_FLOAT && this->class_label_bytes == 4) {
            v = *(bim::float32*) p;
        } else if (this->class_label_ct == H5T_FLOAT && this->class_label_bytes == 8) {
            v = *(bim::float64*) p;
        }

        if (v != m->class_id) return 0;
    }

    v = def;
    bim::uint8 *p = &this->measure_buffer[0] + offset + m->offset;
    if (m->ct == H5T_INTEGER && m->size_bytes == 1) {
        v = *(bim::uint8*) p;
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 2) {
        v = *(bim::uint16*) p;
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 4) {
        v = *(bim::uint32*) p;
    } else if (m->ct == H5T_INTEGER && m->size_bytes == 8) {
        v = *(bim::uint64*) p;
    } else if (m->ct == H5T_FLOAT && m->size_bytes == 4) {
        v = *(bim::float32*) p;
    } else if (m->ct == H5T_FLOAT && m->size_bytes == 8) {
        v = *(bim::float64*) p;
    }

    // if pseudo random object id measure is requested
    if (m->idx == 0 && m->name == "object_id_random") {
        // use object id as a seed and create a non-zero pseudo random number
        // we need pseudo-random to ensure id values would always be the same for every id
        std::default_random_engine generator((int) v);
        std::uniform_int_distribution<int> distribution(1, 65000); // approximate uint16 range
        v = distribution(generator);
    }

    return v;
}


//****************************************************************************
// required funcs
//****************************************************************************

#define BIM_FORMAT_HDF5_MAGIC_SIZE 8

const char hdf5_magic_number[9] = "\x89\x48\x44\x46\x0d\x0a\x1a\x0a"; //89 48 44 46 0d 0a 1a 0a

int hdf5ValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_FORMAT_HDF5_MAGIC_SIZE) return -1;
    unsigned char *mag_num = (unsigned char *)magic;
    if (memcmp(mag_num, hdf5_magic_number, BIM_FORMAT_HDF5_MAGIC_SIZE) == 0) return 0;
    return -1;
}

FormatHandle hdf5AquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void hdf5CloseImageProc(FormatHandle *fmtHndl);
void hdf5ReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    hdf5CloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// hdf5 utils
//----------------------------------------------------------------------------

static const int BIM_STR_SIZE = 2048;

//std::shared_ptr<bim::xstring> get_attribute_name(H5::DataSet &dataset, int idx) {
bim::xstring hdf5_get_attribute_name(H5::H5Object &dataset, int idx, const bim::xstring &def = "") {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(idx);
    } catch (...) {
        return def;
    }

    bim::xstring name(BIM_STR_SIZE, 0);
    ssize_t sz = a.getName(&name[0], BIM_STR_SIZE);
    name.resize(sz);

    //return std::make_shared<bim::xstring>(name);
    return name;
}

//std::shared_ptr<bim::xstring> get_attribute_value(H5::DataSet &dataset, const std::string &name) {
bim::xstring hdf5_get_attribute_string(H5::H5Object &dataset, const std::string &name, const bim::xstring &def) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return def;
    }

    H5::DataType dt = a.getDataType();
    if (dt.getClass() != H5T_STRING)
        //return std::make_shared<bim::xstring>(def);
        return def;

    hsize_t sz = a.getStorageSize();
    bim::xstring s(sz, 0);
    a.read(dt, &s[0]);
    if (s[sz - 1] == 0) s.resize(sz - 1);
    //return std::make_shared<bim::xstring>(s);
    return s;
}

int hdf5_get_attribute_int(H5::H5Object &dataset, const std::string &name, const int &def = 0) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return def;
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_INTEGER || vsz < 1 || esz < 1)
        return def;

    std::vector<int> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_INT, &v[0]);
    return v[0];
}

std::vector<int> hdf5_get_attribute_int_vector(H5::H5Object &dataset, const std::string &name) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return std::vector<int>();
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_INTEGER || vsz < 1 || esz < 1)
        return std::vector<int>();

    std::vector<int> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_INT, &v[0]);
    return v;
}

double hdf5_get_attribute_double(H5::H5Object &dataset, const std::string &name, const double &def = 0) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return def;
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_FLOAT || vsz < 1 || esz < 1)
        return def;

    std::vector<double> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_DOUBLE, &v[0]);
    return v[0];
}

std::vector<double> hdf5_get_attribute_double_vector(H5::H5Object &dataset, const std::string &name) {
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return std::vector<double>();
    }
    H5::DataType dt = a.getDataType();
    hsize_t vsz = a.getStorageSize();
    size_t esz = dt.getSize();
    if (dt.getClass() != H5T_FLOAT || vsz < 1 || esz < 1)
        return std::vector<double>();

    std::vector<double> v(vsz / esz, 0);
    a.read(H5::PredType::NATIVE_DOUBLE, &v[0]);
    return v;
}

H5::DataSet hdf5_get_referenced_object(H5::H5Object &dataset, const std::string &name) {
    H5::DataSet o;
    H5::Attribute a;
    try {
        a = dataset.openAttribute(name.c_str());
    } catch (...) {
        return o;
    }
    H5::DataType dt = a.getDataType();
    if (dt.getClass() != H5T_REFERENCE)
        return o;

    hid_t v;
    a.read(H5::PredType::STD_REF_OBJ, &v);

    o.dereference(a, &v);
    return o;
}

void hdf5_append_object_attributes(H5::H5Object &obj, TagMap *hash, const xstring &meta_path) {
    for (int i = 0; i < obj.getNumAttrs(); ++i) {
        H5::Attribute a = obj.openAttribute(i);
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = a.getName(&name[0], BIM_STR_SIZE);
        if (sz < 1) continue;
        name.resize(sz);

        H5::DataType dt = a.getDataType();
        hsize_t vsz = a.getStorageSize();
        size_t esz = dt.getSize();
        if (vsz < 1 || esz < 1) continue;
        if (dt.getClass() == H5T_STRING) {
            bim::xstring v(vsz, 0);
            a.read(dt, &v[0]);
            if (v[vsz - 1] == 0) v.resize(vsz - 1);
            hash->set_value(meta_path + "/" + name, v);
        } else if (dt.getClass() == H5T_INTEGER) {
            std::vector<int> v(vsz / esz, 0);
            a.read(H5::PredType::NATIVE_INT, &v[0]);
            if (v.size() == 1)
                hash->set_value(meta_path + "/" + name, v[0]);
            else if (v.size() < 100)
                hash->set_value(meta_path + "/" + name, v);
        } else if (dt.getClass() == H5T_FLOAT) {
            std::vector<double> v(vsz / esz, 0);
            a.read(H5::PredType::NATIVE_DOUBLE, &v[0]);
            if (v.size() == 1)
                hash->set_value(meta_path + "/" + name, v[0]);
            else if (v.size() < 100)
                hash->set_value(meta_path + "/" + name, v);
        }
    }
}

void hdf5_append_attributes(bim::HDF5Params *par, TagMap *hash, const xstring &h5_path, const xstring &meta_path) {
    xstring path = h5_path;
    if (path.size() < 1) path = "/";

    try {
        H5::Group group = par->file->openGroup(path.c_str());
        hdf5_append_object_attributes(group, hash, meta_path);
    } catch (H5::FileIException error) {
        try {
            H5::DataSet dataset = par->file->openDataSet(path.c_str());
            hdf5_append_object_attributes(dataset, hash, meta_path);
        } catch (H5::FileIException error) {
            // pass
        }
    }
}

void hdf5_walker(bim::HDF5Params *par, H5::Group &obj, const xstring &h5_path, std::vector<xstring> &objects, const int &max_objects = -1) {

    // first find all datasets at the current level
    for (int i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::DataSet dataset = par->file->openDataSet(path.c_str());

            // get sizes and types and see if its valid for inclusion
            H5::DataType dt = dataset.getDataType();
            H5T_class_t type_class = dt.getClass();
            if (type_class != H5T_FLOAT && type_class != H5T_INTEGER) continue;

            // Get filespace for rank and dimension
            H5::DataSpace dataspace = dataset.getSpace();
            int rank = dataspace.getSimpleExtentNdims();
            if (rank < 2) continue;

            std::vector<hsize_t> dims(rank);
            rank = dataspace.getSimpleExtentDims(&dims[0]);
            if (rank == 2 && (dims[0] < HDF5_GUESS_MAX_CHANNELS || dims[1] < HDF5_GUESS_MAX_CHANNELS)) continue;

            objects.push_back(path);
            if (max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (...) { //catch (H5::FileIException error) {
        }
    }

    // then explore depth
    for (int i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::Group group = par->file->openGroup(path.c_str());
            hdf5_walker(par, group, path, objects, max_objects);
            if (max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (H5::FileIException error) {
            // do nothing
        }
    }
}

void hdf5_meta_walker(bim::HDF5Params *par, TagMap *hash, const xstring &h5_path, const xstring &meta_path) {
    try {
        H5::Group group = par->file->openGroup(h5_path.c_str());
        hdf5_append_object_attributes(group, hash, meta_path);

        for (int i = 0; i < group.getNumObjs(); ++i) {
            bim::xstring name(BIM_STR_SIZE, 0);
            ssize_t sz = group.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
            name.resize(sz);

            bim::xstring mpath = meta_path + "/" + name;
            bim::xstring hpath = h5_path + "/" + name;
            hdf5_meta_walker(par, hash, hpath, mpath);
        }

    } catch (H5::FileIException error) {
    }
}

//----------------------------------------------------------------------------
// reading
//----------------------------------------------------------------------------

void hdf5_get_palette(bim::HDF5Params *par, H5::DataSet &dataset, ImageInfo *info) {
    H5::DataSet pallette = hdf5_get_referenced_object(dataset, "PALETTE");
    H5::DataType dt = pallette.getDataType();

    bim::xstring tag_class = hdf5_get_attribute_string(pallette, std::string("CLASS"));
    if (tag_class != "PALETTE") return;

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = pallette.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace
    H5::DataSpace memspace(2, &dims[0]);

    // Define memory hyperslab
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = dims[0];
    count_out[1] = dims[1];
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
    count[0] = dims[0];
    count[1] = dims[1];
    dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

    // allocate image
    std::vector<bim::uint8> data(dims[0] * dims[1], 0);

    // Read data from hyperslab in the file into the hyperslab in memory
    pallette.read(&data[0], dt, memspace, dataspace);

    // copy values
    info->lut.count = bim::min<bim::uint>(256, (bim::uint)dims[0]);
    int x = 0;
    for (bim::uint i = 0; i < info->lut.count; ++i) {
        info->lut.rgba[i] = bim::xRGB(data[x + 0], data[x + 1], data[x + 2]);
        x += 3;
    }
}

void hdf5_read_vector(bim::HDF5Params *par, const xstring &h5_path, std::vector<double> &data) {
    H5::DataSet dataset = par->file->openDataSet(h5_path.c_str());
    //H5::DataType dt = dataset.getDataType();

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace
    H5::DataSpace memspace(1, &dims[0]);

    // Define memory hyperslab
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = dims[0];
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
    count[0] = dims[0];
    dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

    // allocate vector
    data.resize(dims[0], 0);

    // Read data from hyperslab in the file into the hyperslab in memory
    //dataset.read(&data[0], dt, memspace, dataspace);
    dataset.read(&data[0], H5::PredType::NATIVE_DOUBLE, memspace, dataspace);
}

void hdf5_read_vector_int(bim::HDF5Params *par, const xstring &h5_path, std::vector<int> &data) {
    H5::DataSet dataset = par->file->openDataSet(h5_path.c_str());
    //H5::DataType dt = dataset.getDataType();

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace
    H5::DataSpace memspace(2, &dims[0]);

    // Define memory hyperslab
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = dims[0];
    count_out[1] = dims[1];
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file
    count[0] = dims[0];
    count[1] = dims[1];
    dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

    // allocate vector
    data.resize(dims[0] * dims[1], 0);

    // Read data from hyperslab in the file into the hyperslab in memory
    //dataset.read(&data[0], dt, memspace, dataspace);
    dataset.read(&data[0], H5::PredType::NATIVE_INT, memspace, dataspace);
}

void viqi2PixelFormat(const std::string &pf, ImageInfo *info) {
    if (pf == "uchar" || pf == "unsigned char" || pf == "uint8" || pf == "uint8_t") {
        info->depth = 8;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "ushort" || pf == "unsigned short" || pf == "unsigned short int" || pf == "uint16" || pf == "uint16_t") {
        info->depth = 16;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "uint" || pf == "unsigned int" || pf == "uint32" || pf == "uint32_t") {
        info->depth = 32;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "ulonglong" || pf == "unsigned long long" || pf == "unsigned long long int" || pf == "uint64" || pf == "uint64_t") {
        info->depth = 64;
        info->pixelType = bim::FMT_UNSIGNED;
    } else if (pf == "signed char" || pf == "int8" || pf == "int8_t") {
        info->depth = 8;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "short" || pf == "short int" || pf == "signed short" || pf == "signed short int" || pf == "int16" || pf == "int16_t") {
        info->depth = 16;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "int" || pf == "signed int" || pf == "int32" || pf == "int32_t") {
        info->depth = 32;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "longlong" || pf == "long long" || pf == "long long int" || pf == "signed long long" || pf == "signed long long int" || pf == "int64" || pf == "int64_t") {
        info->depth = 64;
        info->pixelType = bim::FMT_SIGNED;
    } else if (pf == "float") {
        info->depth = 32;
        info->pixelType = bim::FMT_FLOAT;
    } else if (pf == "double") {
        info->depth = 64;
        info->pixelType = bim::FMT_FLOAT;
    }
}

void setDataConfiguration(FormatHandle *fmtHndl, H5::DataSet &dataset, ImageInfo *info) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    // get data type from a measure, only apply this for sparse and heatmap storage, not dense!
    H5::DataType dt = dataset.getDataType();
    if (fmtHndl->subFormat == FMT_VIQI && par->values_from_measures && par->dimensions_image[bim::DIM_MEASURE]>0 && par->table.hasMeasures()) {
        viqi_init_coordinate(fmtHndl, 0, 0, 0);
        VQITableMeasure *m = par->table.getMeasure(par->positions[bim::DIM_MEASURE]);
        if (m) {
            dt = m->dt; // dima: use measure datatype
        }
    }

    info->depth = (bim::uint32)dt.getSize() * 8;
    info->pixelType = FMT_UNDEFINED;
    H5T_class_t type_class = dt.getClass();
    par->hdf_pixel_format = (int)type_class;
    if (type_class == H5T_FLOAT) {
        info->pixelType = FMT_FLOAT;
    } else if (type_class == H5T_INTEGER) {
        info->pixelType = FMT_UNSIGNED;
        H5::IntType int_type = dataset.getIntType();
        if (int_type.getSign() != H5T_SGN_NONE) {
            info->pixelType = FMT_SIGNED;
        }
    } else if (type_class == H5T_BITFIELD) {
        info->pixelType = FMT_UNSIGNED;
    }

    // get data dimensions
    info->imageMode = IM_GRAYSCALE;
    info->samples = 1;
    info->number_z = 1;
    info->number_t = 1;
    info->number_dims = 2;
    info->number_pages = 1;

    // Get filespace for rank and dimension
    H5::DataSpace dataspace = dataset.getSpace();
    // Get number of dimensions in the file dataspace
    int rank = dataspace.getSimpleExtentNdims();

    // Get the dimension sizes of the file dataspace
    std::vector<hsize_t> dims(rank);
    rank = dataspace.getSimpleExtentDims(&dims[0]);

    // Check if dataset is chunked
    H5::DSetCreatPropList cparms = dataset.getCreatePlist();
    std::vector<hsize_t> chunk_dims(rank, 0);
    int rank_chunk = 0;
    if (cparms.getLayout() == H5D_CHUNKED) {
        rank_chunk = cparms.getChunk(rank, &chunk_dims[0]);
    }

    if (fmtHndl->subFormat == FMT_VIQI) {

        // support for external data blocks
        bim::xstring datatype = hdf5_get_attribute_string(dataset, "viqi_block_external_datatype");
        std::vector<int> block_dims = hdf5_get_attribute_int_vector(dataset, "viqi_block_external_dimensions");
        std::vector<int> block_chunks = hdf5_get_attribute_int_vector(dataset, "viqi_block_external_chunking");

        if (datatype.size() > 0) {
            viqi2PixelFormat(datatype, info);
        }
        if (block_dims.size() > 0) {
            rank = block_dims.size();
            dims.resize(rank);
            for (int i = 0; i < rank; ++i) dims[i] = block_dims[i];
        }
        if (block_chunks.size() > 0) {
            rank_chunk = block_chunks.size();
            chunk_dims.resize(rank_chunk);
            for (int i = 0; i < rank_chunk; ++i) chunk_dims[i] = block_chunks[i];
            chunk_dims[0] = 128;
            chunk_dims[1] = 128;
        }

        // ViQi: [width][height][z][t][c]
        if (rank > 1) {
            par->dimensions_element[bim::DIM_X] = dims[1];
            par->dimensions_element[bim::DIM_Y] = dims[0];
            info->number_dims = 2;
        }

        // do a proper dim test using strings: Y,X,Z,C,...
        for (int d = 0; d < rank; ++d) {
            if (par->dimensions_indices.size() <= d) break;
            int dim_idx = par->dimensions_indices[d];
            par->dimensions_element[dim_idx] = (int)dims[d];
            par->dimensions_tile[dim_idx] = (int)chunk_dims[d];
            if (dim_idx > 2)
                ++info->number_dims;
        }

        if (rank > 1) {
            info->width = par->dimensions_element[bim::DIM_X];
            info->height = par->dimensions_element[bim::DIM_Y];
            info->number_z = par->dimensions_element[bim::DIM_Z];
            info->number_t = par->dimensions_element[bim::DIM_T];
            info->samples = par->dimensions_element[bim::DIM_C];
            par->image_num_samples = par->dimensions_element[bim::DIM_C];
            info->imageMode = info->samples > 1 ? IM_MULTI : IM_GRAYSCALE;
        }

        // figure if the last dim is samples
        //if (par->dimensions_indices[rank - 1] == bim::DIM_C)
        //    par->image_num_samples = dims[rank - 1];

        // deal with spectral case here
        if (par->viqi_image_content == "spectral") {
            par->dimensions_image[bim::DIM_SPECTRA] = info->samples;
            if (par->position_spectrum.size() == 0)
                par->position_spectrum.push_back(info->samples / 2);
            par->image_num_samples = std::max<int>(1, par->position_spectrum.size());
            info->samples = par->image_num_samples;
            info->number_pages = info->number_pages * par->dimensions_image[bim::DIM_SPECTRA];
            info->imageMode = bim::IM_SPECTRAL;
            info->number_dims += 1;
        }

    } else if (fmtHndl->subFormat == FMT_DREAM3D) {
        if (rank == 2) { // Dream3d: [height][width] - GUESS
            info->width = dims[1];
            info->height = dims[0];
            info->number_z = 1;
            info->samples = 1;
            info->number_dims = 2;
            if (rank_chunk > 2) {
                par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[1];
                par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[0];
                par->dimensions_tile[bim::DIM_Z] = 0;
            }
        } else if (rank == 3) { // Dream3d: [z][height][width] - GUESS
            info->width = dims[2];
            info->height = dims[1];
            info->number_z = dims[0];
            info->samples = 1;
            info->number_dims = 3;
            if (rank_chunk > 2) {
                par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[2];
                par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[1];
                par->dimensions_tile[bim::DIM_Z] = (int)chunk_dims[0];
            }
        } else if (rank == 4) { // Dream3d: [z][height][width][pixel components] - FACT
            info->width = dims[2];
            info->height = dims[1];
            info->number_z = dims[0];
            info->samples = (bim::uint32)dims[3];
            info->number_dims = 3;
            if (rank_chunk > 2) {
                par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[2];
                par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[1];
                par->dimensions_tile[bim::DIM_Z] = (int)chunk_dims[0];
            }
        }
    } else if (fmtHndl->subFormat == FMT_IMS) {
        // Imaris: [z][height][width]
        info->width = dims[2];
        info->height = dims[1];
        info->number_z = dims[0];
        if (rank_chunk > 2) {
            par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[2];
            par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[1];
            par->dimensions_tile[bim::DIM_Z] = (int)chunk_dims[0];
        }
    } else {
        // Generic HDF5 case - this has many heuristics, require more tweaking
        if (rank == 2) {
            info->width = dims[1];
            info->height = dims[0];
            if (rank_chunk > 1) {
                //info->tileWidth = chunk_dims[1];
                //info->tileHeight = chunk_dims[0];
                par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[1];
                par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[0];
            }
        } else if (rank == 3) {
            if (dims[2] < bim::HDF5_GUESS_MAX_CHANNELS) { // guess that last dim is radiometric
                info->width = dims[1];
                info->height = dims[0];
                info->samples = (bim::uint32)dims[2];
                if (rank_chunk > 1) {
                    par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[1];
                    par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[0];
                }
                par->interlaced = true;
            } else {
                info->width = dims[2];
                info->height = dims[1];
                info->number_z = dims[0];
                info->number_t = 1;
                info->samples = 1;
                info->number_dims = 3;
                if (rank_chunk > 2) {
                    par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[2];
                    par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[1];
                }
            }
        } else if (rank == 4) {
            if (dims[3] < bim::HDF5_GUESS_MAX_CHANNELS) { // guess that last dim is radiometric
                info->width = dims[2];
                info->height = dims[1];
                info->number_z = dims[0];
                info->samples = (bim::uint32)dims[3];
                info->number_dims = 3;
                if (rank_chunk > 2) {
                    par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[2];
                    par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[1];
                }
                par->interlaced = true;
            } else {
                info->width = dims[3];
                info->height = dims[2];
                info->number_z = dims[1];
                info->number_t = dims[0];
                info->samples = 1;
                info->number_dims = 4;
                if (rank_chunk > 2) {
                    par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[3];
                    par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[2];
                }
            }
        } else if (rank > 4) {
            if (dims[4] < bim::HDF5_GUESS_MAX_CHANNELS) { // guess that last dim is radiometric
                info->width = dims[3];
                info->height = dims[2];
                info->number_z = dims[1];
                info->number_t = dims[0];
                info->samples = (bim::uint32)dims[4];
                info->number_dims = 4;
                par->interlaced = true;
            } else {
                // will be skipping other dims since we don't know what they are
                info->width = dims[3];
                info->height = dims[2];
                info->number_z = dims[1];
                info->number_t = dims[0];
                info->samples = 1;
                info->number_dims = 4;
            }
            if (rank_chunk > 2) {
                par->dimensions_tile[bim::DIM_X] = (int)chunk_dims[3];
                par->dimensions_tile[bim::DIM_Y] = (int)chunk_dims[2];
            }
        }

        par->dimensions_image[bim::DIM_X] = info->width;
        par->dimensions_image[bim::DIM_Y] = info->height;
        par->resolution_levels = 1;
        par->dimensions_image[bim::DIM_Z] = info->number_z;
    }
    info->number_pages = info->number_z * info->number_t;

    // check attributes if dataset is an image and set proper colorspaces
    if (dataset.attrExists("IMAGE_VERSION")) {
        bim::xstring tag_class = hdf5_get_attribute_string(dataset, std::string("CLASS"));
        if (tag_class == "IMAGE") {
            par->image_class = true;

            bim::xstring tag_interlace = hdf5_get_attribute_string(dataset, std::string("INTERLACE_MODE"));
            par->interlaced = (tag_interlace == "INTERLACE_PIXEL");

            bim::xstring tag_subclass = hdf5_get_attribute_string(dataset, std::string("IMAGE_SUBCLASS"));
            if (rank > 2 && tag_subclass == "IMAGE_TRUECOLOR" && par->interlaced) { //	[height][width][pixel components]
                info->width = dims[1];
                info->height = dims[0];
                info->samples = (bim::uint32)dims[2];
            } else if (rank > 2 && tag_subclass == "IMAGE_TRUECOLOR" && !par->interlaced) { // [pixel components][height][width]
                info->width = dims[2];
                info->height = dims[1];
                info->samples = (bim::uint32)dims[0];
            }

            if (tag_subclass == "IMAGE_TRUECOLOR" && info->samples == 3)
                info->imageMode = IM_RGB;
            else if (tag_subclass == "IMAGE_TRUECOLOR" && info->samples == 4)
                info->imageMode = IM_RGBA;

            if (tag_subclass == "IMAGE_INDEXED")
                info->imageMode = IM_INDEXED;
            else if (tag_subclass == "IMAGE_BITMAP")
                info->imageMode = IM_BITMAP;
            else if (tag_subclass == "IMAGE_GRAYSCALE")
                info->imageMode = IM_GRAYSCALE;
        }
    } // if image class

    /*
    par->dimensions_image[bim::DIM_X] = info->width;
    par->dimensions_image[bim::DIM_Y] = info->height;
    par->resolution_levels = 1;
    par->dimensions_image[bim::DIM_Z] = info->number_z;
    */

    // read pallette if referenced
    if (dataset.attrExists("PALETTE")) {
        hdf5_get_palette(par, dataset, info);
    }
}

void hdf5_init_coordinate(FormatHandle *fmtHndl, const bim::uint &page, int &z, int &t) {
    fmtHndl->pageNumber = page;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    z = -1;
    t = -1;
    //int spectrum = 0;
    if (conf) {
        z = conf->getValueInt("-slice-z", -1);
        t = conf->getValueInt("-slice-t", -1);
        //spectrum = conf->getValueInt("-slice-spectrum", 0);
    }

    if (info->number_z > 1 && info->number_t <= 1 && z < 0) {
        z = page;
        t = 0;
    } else if (info->number_z <= 1 && info->number_t > 1 && t < 0) {
        t = page;
        z = 0;
    } else if (info->number_z > 1 && info->number_t > 1 && z < 0 && t < 0) {
        t = (int)floor(page / info->number_z);
        z = (int)(page - t * info->number_z);
    }
    z = bim::max(0, z);
    t = bim::max(0, t);
}

void hdf5_set_offset_count(FormatHandle *fmtHndl, bim::ImageInfo *info, const std::vector<hsize_t> &dims, const int &rank, const int &sample, const int &z, const int &t, std::vector<hsize_t> &offset, std::vector<hsize_t> &count, int x = 0, int y = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    // init sizes to the original volume
    for (int i = 0; i < count.size(); ++i)
        count[i] = dims[i];

    // Imaris case
    if (fmtHndl->subFormat == FMT_IMS) {
        // Imaris: [z][height][width]
        if (rank == 3) {
            offset[0] = z;
            offset[1] = y;
            offset[2] = x;
            count[0] = 1;
            count[1] = info->height;
            count[2] = info->width;
        } else { // unrecognized case, zero everything
            for (int i = 0; i < count.size(); ++i)
                count[i] = 0;
        }

        return;
    }

    // dream3d case
    if (fmtHndl->subFormat == FMT_DREAM3D) {
        if (rank == 2) { // Dream3d: [height][width] - GUESS
            offset[0] = y;
            offset[1] = x;
            count[0] = info->height;
            count[1] = info->width;
        } else if (rank == 3) { // Dream3d: [z][height][width] - GUESS
            offset[0] = bim::max<int>(z, t);
            offset[1] = y;
            offset[2] = x;
            count[0] = 1;
            count[1] = info->height;
            count[2] = info->width;
        } else if (rank == 4 && info->samples >= 1) { // [z][height][width][pixel components]
            offset[0] = bim::max<int>(z, t);
            offset[1] = y;
            offset[2] = x;
            offset[3] = sample;
            count[0] = 1;
            count[1] = info->height;
            count[2] = info->width;
            count[3] = 1;
        } else { // unrecognized case, zero everything
            for (int i = 0; i < count.size(); ++i)
                count[i] = 0;
        }

        return;
    }

    // reset some dimensions to produce 2D planes of channels, z and t
    if (rank == 2) {
        // here we simply use the original dimensionality
    } else if (rank == 3 && info->samples > 1 && par->interlaced) {
        // [height][width][pixel components]
        offset[0] = y;
        offset[1] = x;
        offset[2] = sample;
        count[0] = info->height;
        count[1] = info->width;
        count[2] = 1;
    } else if (rank == 3 && info->samples > 1 && !par->interlaced) {
        // [pixel components][height][width]
        offset[0] = sample;
        offset[1] = y;
        offset[2] = x;
        count[0] = 1;
        count[1] = info->height;
        count[2] = info->width;
    } else if (rank == 3 && info->samples == 1 && par->interlaced) {
        // [height][width][z or t]
        offset[0] = y;
        offset[1] = x;
        offset[2] = bim::max<int>(z, t);
        count[0] = info->height;
        count[1] = info->width;
        count[2] = 1;
    } else if (rank == 3 && info->samples == 1 && !par->interlaced) {
        // [z or t][height][width]
        offset[0] = bim::max<int>(z, t);
        offset[1] = y;
        offset[2] = x;
        count[0] = 1;
        count[1] = info->height;
        count[2] = info->width;
    } else if (rank == 4 && info->samples > 1 && par->interlaced) {
        // [z or t][height][width][pixel components]
        offset[0] = bim::max<int>(z, t);
        offset[1] = y;
        offset[2] = x;
        offset[3] = sample;
        count[0] = 1;
        count[1] = info->height;
        count[2] = info->width;
        count[3] = 1;
    } else if (rank == 4 && info->samples > 1 && !par->interlaced) {
        // [pixel components][z or t][height][width]
        offset[0] = sample;
        offset[1] = bim::max<int>(z, t);
        offset[2] = y;
        offset[3] = x;
        count[0] = 1;
        count[1] = 1;
        count[2] = info->height;
        count[3] = info->width;
    } else if (rank == 4 && info->samples == 1) {
        // [t][z][height][width]
        offset[0] = t;
        offset[1] = z;
        offset[2] = y;
        offset[3] = x;
        count[0] = 1;
        count[1] = 1;
        count[2] = info->height;
        count[3] = info->width;
    } else if (rank == 5 && info->samples > 1 && par->interlaced) {
        // [height][width][z][t][pixel components]
        offset[0] = y;
        offset[1] = x;
        offset[2] = z;
        offset[3] = t;
        offset[4] = sample;
        count[0] = info->height;
        count[1] = info->width;
        count[2] = 1;
        count[3] = 1;
        count[4] = 1;
    } else if (rank == 5 && info->samples > 1 && !par->interlaced) {
        // [pixel components][t][z][height][width]
        offset[0] = sample;
        offset[1] = t;
        offset[2] = z;
        offset[3] = y;
        offset[4] = x;
        count[0] = 1;
        count[1] = 1;
        count[2] = 1;
        count[3] = info->height;
        count[4] = info->width;
    } else { // unrecognized case, zero everything
        for (int i = 0; i < count.size(); ++i)
            count[i] = 0;
    }
}

//****************************************************************************
// sub-formats: Imaris
//****************************************************************************

void imsGetMultiResolutionPyramidalSizes(const std::vector<size_t> &aDataSize, std::vector<double> &zscales) {
    const float mMinVolumeSizeMB = 1.f;
    std::vector<std::vector<size_t>> aResolutionSizes;
    std::vector<size_t> vNewResolution = aDataSize;
    int N = 3;
    float vVolumeMB;
    do {
        aResolutionSizes.push_back(vNewResolution);
        std::vector<size_t> vLastResolution = vNewResolution;
        size_t vLastVolume = vLastResolution[0] * vLastResolution[1] * vLastResolution[2];
        for (int d = 0; d < N; ++d) {
            if ((10 * vLastResolution[d]) * (10 * vLastResolution[d]) > vLastVolume / vLastResolution[d])
                vNewResolution[d] = vLastResolution[d] / 2;
            else
                vNewResolution[d] = vLastResolution[d];
            // make sure we don't have zero-size dimension
            vNewResolution[d] = std::max((size_t)1, vNewResolution[d]);
        }
        vVolumeMB = (vNewResolution[0] * vNewResolution[1] * vNewResolution[2]) / (1024.f * 1024.f);
    } while (vVolumeMB > mMinVolumeSizeMB);

    zscales.resize(aResolutionSizes.size(), 0);
    double z = (double)aResolutionSizes[0][2];
    for (int i = 0; i < aResolutionSizes.size(); ++i) {
        zscales[i] = aResolutionSizes[i][2] / z;
    }
}

void imsGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    *info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    H5::Group image = par->file->openGroup("/DataSetInfo/Image");
    bim::xstring tag_num_c = hdf5_get_attribute_string(image, std::string("Noc"));
    if (tag_num_c.size() < 1) {
        tag_num_c = hdf5_get_attribute_string(image, std::string("NumberOfChannels"));
    }
    bim::xstring tag_num_x = hdf5_get_attribute_string(image, std::string("X"));
    bim::xstring tag_num_y = hdf5_get_attribute_string(image, std::string("Y"));
    bim::xstring tag_num_z = hdf5_get_attribute_string(image, std::string("Z"));

    H5::Group time = par->file->openGroup("/DataSetInfo/TimeInfo");
    bim::xstring tag_num_t = hdf5_get_attribute_string(time, std::string("FileTimePoints"));
    if (tag_num_t.size() < 1) {
        tag_num_t = hdf5_get_attribute_string(time, std::string("DatasetTimePoints"));
    }

    // read the very first image to get reliable X,Y,Z sizes and pixel depths
    H5::DataSet dataset = par->file->openDataSet("/DataSet/ResolutionLevel 0/TimePoint 0/Channel 0/Data");
    setDataConfiguration(fmtHndl, dataset, info);

    info->width = tag_num_x.toInt(0);
    info->height = tag_num_y.toInt(0);
    int z = tag_num_z.toInt(0);
    //if (x > 0) info->width = x; // there are cases with bogus values
    //if (y > 0) info->height = y; // there are cases with bogus values
    if (z > 0) { // attempt to safeguard against bogus value
        info->number_z = z;
    }

    info->samples = tag_num_c.toInt(1);
    info->number_t = tag_num_t.toInt();
    info->number_pages = info->number_z * info->number_t;
    info->number_dims = 2;
    if (info->number_z > 1) ++info->number_dims;
    if (info->number_t > 1) ++info->number_dims;

    par->dimensions_image[bim::DIM_X] = info->width;
    par->dimensions_image[bim::DIM_Y] = info->height;
    par->dimensions_image[bim::DIM_Z] = info->number_z;
    info->tileWidth = par->dimensions_tile[bim::DIM_X];
    info->tileHeight = par->dimensions_tile[bim::DIM_Y];

    par->resolution_levels = 1;
    for (int r = 0; r < 1000; ++r) {
        try {
            bim::xstring path = xstring::xprintf("/DataSet/ResolutionLevel %d", par->resolution_levels);
            H5::Group res = par->file->openGroup(path.c_str());
            ++par->resolution_levels;
        } catch (H5::FileIException error) {
            break;
        }
    }
    info->number_levels = par->resolution_levels;
    par->resolution_levels = (int)info->number_levels;

    std::vector<size_t> aDataSize = { info->width, info->height, info->number_z };
    imsGetMultiResolutionPyramidalSizes(aDataSize, par->zscales);

    info->resUnits = RES_IN;
    info->xRes = 0;
    info->yRes = 0;
}

//----------------------------------------------------------------------------
// Imaris Metadata
//----------------------------------------------------------------------------

void ims_getset_metadata(TagMap *hash, const xstring &hpath, const xstring &mpath) {
    if (!hash->hasKey(hpath)) return;
    xstring s = hash->get_value(hpath);
    hash->set_value(mpath, s);
}

void ims_parse_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    // parse channels
    for (int i = 0; i < (int)info->samples; ++i) {
        xstring hpath = xstring::xprintf("Imaris/Channel %d/", i);
        xstring mpath = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);

        ims_getset_metadata(hash, hpath + "Name", mpath + bim::CHANNEL_INFO_NAME);
        ims_getset_metadata(hash, hpath + "Description", mpath + bim::CHANNEL_INFO_DESCRIPTION);
        ims_getset_metadata(hash, hpath + "GammaCorrection", mpath + bim::CHANNEL_INFO_GAMMA);
        ims_getset_metadata(hash, hpath + "ColorOpacity", mpath + bim::CHANNEL_INFO_OPACITY);
        ims_getset_metadata(hash, hpath + "Pinhole", mpath + bim::CHANNEL_INFO_PINHOLE_RADIUS);
        ims_getset_metadata(hash, hpath + "LSMPinhole", mpath + bim::CHANNEL_INFO_PINHOLE_RADIUS);
        ims_getset_metadata(hash, hpath + "LSMEmissionWavelength", mpath + bim::CHANNEL_INFO_EM_WAVELENGTH);
        ims_getset_metadata(hash, hpath + "LSMExcitationWavelength", mpath + bim::CHANNEL_INFO_EM_WAVELENGTH);

        if (hash->hasKey(hpath + "Color")) {
            xstring s = hash->get_value(hpath + "Color");
            std::vector<double> rgb = s.splitDouble(" ");
            hash->set_value(mpath + bim::CHANNEL_INFO_COLOR, bim::xstring::xprintf("%.2f,%.2f,%.2f", rgb[0], rgb[1], rgb[2]));

            // write old-style tags, to be removed by version 3
            hash->set_value(bim::xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i), bim::xstring::xprintf("%.0f,%.0f,%.0f", rgb[0] * 255, rgb[1] * 255, rgb[2] * 255));
        }

        // write old-style tags, to be removed by version 3
        ims_getset_metadata(hash, hpath + "Name", bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i));
    }

    // objective and resolutioin
    ims_getset_metadata(hash, "Imaris/Image/RecordingDate", bim::DOCUMENT_DATETIME);

    ims_getset_metadata(hash, "Imaris/Image/Lenspower", xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), 0) + bim::OBJECTIVE_NAME);
    ims_getset_metadata(hash, "Imaris/Image/NumericalAperture", xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), 0) + bim::OBJECTIVE_NUMERICAL_APERTURE);

    xstring units = hash->get_value("Imaris/Image/Unit");
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X.c_str(), units);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y.c_str(), units);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z.c_str(), units);

    double min0 = hash->get_value_double("Imaris/Image/ExtMin0", 0);
    double max0 = hash->get_value_double("Imaris/Image/ExtMax0", 0);
    double min1 = hash->get_value_double("Imaris/Image/ExtMin1", 0);
    double max1 = hash->get_value_double("Imaris/Image/ExtMax1", 0);
    double min2 = hash->get_value_double("Imaris/Image/ExtMin2", 0);
    double max2 = hash->get_value_double("Imaris/Image/ExtMax2", 0);

    double xr = (max0 - min0) / info->width;
    double yr = (max1 - min1) / info->height;
    double zr = (max2 - min2) / info->number_z;
    hash->set_value(bim::PIXEL_RESOLUTION_X.c_str(), xr);
    hash->set_value(bim::PIXEL_RESOLUTION_Y.c_str(), yr);
    hash->set_value(bim::PIXEL_RESOLUTION_Z.c_str(), zr);
}

bim::uint ims_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    //bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // append image pyramid meta
    if (par->resolution_levels > 1) {
        std::vector<double> scales;
        double scale = 1.0;
        int level = 0;
        for (int level = 0; level < par->resolution_levels; ++level) {
            scales.push_back(scale);
            scale /= 2.0;
        }

        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_NUM_RES_L, par->resolution_levels);
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(scales, ","));
    }

    // parse all metadata
    if (conf && conf->hasKeyWith("-meta")) {
        xstring h5path = "/DataSetInfo";
        bim::xstring meta_path = "Imaris";
        hdf5_meta_walker(par, hash, h5path, meta_path);

        // parse channels, resolution, etc...
        ims_parse_metadata(fmtHndl, hash);
    }

    return 0;
}

//----------------------------------------------------------------------------------------
// Imaris pixels reader
//----------------------------------------------------------------------------------------

bim::uint ims_read_pixels(FormatHandle *fmtHndl, bim::uint page, int x = 0, int y = 0, int w = 0, int h = 0, int l = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    if (w < 0 || h < 0) return 1;

    int z, t;
    hdf5_init_coordinate(fmtHndl, page, z, t);

    // update scale info
    float scale = (float)(1.0 / pow(2.0, (float)l));
    int plane_width = (int)floor(par->dimensions_image[bim::DIM_X] * scale);
    int plane_height = (int)floor(par->dimensions_image[bim::DIM_Y] * scale);
    if (scale < 1) {
        info->width = plane_width;
        info->height = plane_height;
        // Z is interpolated only after the volume is isotropic
        if (par->zscales.size() > l) {
            info->number_z = (bim::uint64)floor(par->dimensions_image[bim::DIM_Z] * par->zscales[l]);
            z = (int)floor(z * par->zscales[l]);
        }
    }

    // update tile info
    if (w > 0 && h > 0) {
        info->width = bim::min<int>(w, plane_width - x);
        info->height = bim::min<int>(h, plane_height - y);
    }

    // allocate image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = info->height;
    dimsm[1] = info->width;

    // define dimfloat scale = 1.0 / pow(2.0, (float)level);ensions in the dataspace
    int rank = 3;
    std::vector<hsize_t> dims(rank); // Imaris: [z][height][width]
    dims[0] = info->number_z;
    dims[1] = plane_height;
    dims[2] = plane_width;

    // Define memory hyperslab - 2D plane for one channel
    H5::DataSpace memspace(2, &dimsm[0]);
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = info->height;
    count_out[1] = info->width;
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    // read slabs sample by sample
    for (int sample = 0; sample < (int)info->samples; ++sample) {
        xstring path = xstring::xprintf("/DataSet/ResolutionLevel %d/TimePoint %d/Channel %d/Data", l, t, sample);
        H5::DataSet dataset = par->file->openDataSet(path.c_str());
        H5::DataType dt = dataset.getDataType();
        H5::DataSpace dataspace = dataset.getSpace();

        // Define hyperslab in the dataset; implicitly passing stride and block as NULL
        hdf5_set_offset_count(fmtHndl, info, dims, rank, sample, z, t, offset, count, x, y);
        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset.read(bmp->bits[sample], dt, memspace, dataspace);
    }

    return 0;
}

//****************************************************************************
// sub-formats: VQI
//****************************************************************************


bool check_image_node(bim::HDF5Params *par, const xstring &h5_path) {
    try {
        H5::DataSet dataset = par->file->openDataSet(h5_path.c_str());
        bim::xstring h5_class = hdf5_get_attribute_string(dataset, std::string("CLASS"));
        if (h5_class != "IMAGE") return false;
    } catch (...) { //catch (H5::FileIException error) {
        return false;
    }
    return true;
}

void viqi_walker(bim::HDF5Params *par, H5::Group &obj, const xstring &h5_path, std::vector<xstring> &objects, const int &max_objects = -1) {

    // first find all datasets at the current level
    for (int i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::Group group = par->file->openGroup(path.c_str());

            bim::xstring viqi_image_type = hdf5_get_attribute_string(group, std::string("viqi_image_type"));
            if (viqi_image_type.size() < 1) continue;

            objects.push_back(path);
            if (max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (...) { //catch (H5::FileIException error) {
            if (!check_image_node(par, path)) continue;
            objects.push_back(path);
            if (max_objects >= 0 && objects.size() >= max_objects) return;
        }
    }

    // then explore depth
    for (int i = 0; i < obj.getNumObjs(); ++i) {
        bim::xstring name(BIM_STR_SIZE, 0);
        ssize_t sz = obj.getObjnameByIdx(i, &name[0], BIM_STR_SIZE);
        name.resize(sz);

        bim::xstring path = h5_path + "/" + name;
        try {
            H5::Group group = par->file->openGroup(path.c_str());

            bim::xstring viqi_image_type = hdf5_get_attribute_string(group, std::string("viqi_image_type"));
            if (viqi_image_type.size() > 0) continue;

            viqi_walker(par, group, path, objects, max_objects);
            if (max_objects >= 0 && objects.size() >= max_objects) return;
        } catch (H5::FileIException error) {
            // do nothing
        }
    }
}

std::vector<int> viqi_get_item_info(bim::HDF5Params *par, xstring path, int block_id, int item_id) {
    int bboxsz = par->num_spatial_dimensions * 2;
    int indexsz = 2 + bboxsz;
    std::vector<int> item_info(indexsz, 0);
    xstring path_index = path + "_sparse_index";

    std::vector<int> block_index;
    hdf5_read_vector_int(par, path_index.c_str(), block_index);

    for (int i = 0; i < block_index.size(); i += indexsz) {
        if (block_index[i] == item_id) {
            memcpy(&item_info[0], &block_index[i + 0], sizeof(int) * indexsz);
            break;
        }
    }

    return item_info;
}

int viqi_get_item_block(bim::HDF5Params *par, xstring path, int item_id) {
    int block_id = item_id / bim::VIQI_MAX_ITEMS_PER_BLOCK; // quick estimate knowing max number of items per block
    xstring path_level = path + "/level_000/";

    if (item_id >= 0) {
        // find the value in the offsets index
        try {
            H5::Group group = par->file->openGroup(path_level.c_str());
            std::vector<int> item_id_offsets = hdf5_get_attribute_int_vector(group, "viqi_block_item_id_offsets");
            for (int i = (int)item_id_offsets.size() - 1; i >= 0; --i) {
                if (item_id >= item_id_offsets[i])
                    return i;
            }
            return 0;
        } catch (H5::FileIException error) {
        }
    } else {
        // item_id is not defined so find first available block
        for (int i = 0; i < 1000; ++i) {
            xstring path_item = path_level + xstring::xprintf("block_%.4d", i);
            try {
#if H5_VERSION_LE(1, 10, 4)
                H5::DataSet dataset = par->file->openDataSet(path_item.c_str());
                return i;
#else
                if (par->file->nameExists(path_item.c_str()))
                    return i;
#endif
            } catch (H5::FileIException error) {
                // do nothing and try the next one
            }
        } // for i
    }
    return block_id;
}

bool viqiGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return false;
    if (fmtHndl->internalParams == NULL) return false;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    *info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    par->block_id = -1;
    par->item_id = -1;

    // detect first element to list if path is empty
    if (conf) {
        par->path = conf->getValue("-path");
    }
    if (par->path.size() < 1) {
        std::vector<xstring> objects;
        H5::Group root = par->file->openGroup("/");
        viqi_walker(par, root, "", objects, 1);
        par->path = objects[0];
    }

    info->resUnits = RES_IN;
    info->xRes = 0;
    info->yRes = 0;

    // default 2D image configuration, in case no metadata is defined
    par->num_spatial_dimensions = 2;
    par->dimensions_indices[0] = bim::DIM_Y;
    par->dimensions_indices[1] = bim::DIM_X;
    par->dimensions_indices[2] = bim::DIM_C;

    if (conf && conf->keyExists("-slice-spectrum")) {
        //par->position_spectrum = conf->getValueInt("-slice-spectrum");
        par->position_spectrum = conf->getDimensionSlices("-slice-spectrum");
    }

    if (conf && conf->keyExists("-slice-item")) {
        par->item_id = conf->getValueInt("-slice-item");
        // read the first available block in order to define pixel-level meta
        par->block_id = viqi_get_item_block(par, par->path, par->item_id);
    }

    if (conf && conf->keyExists("-interpolation")) {
        par->interpolation = str_to_interpolation(conf->getValue("-interpolation"));
        par->user_given_interpolation = true;
    }

    // try to read provided path, in case it does not exist it must be a paged item, attemp to find
    try {
        H5::Group group = par->file->openGroup(par->path.c_str());
        par->viqi_image_size = hdf5_get_attribute_int_vector(group, "viqi_image_size");
        par->viqi_image_dimensions = hdf5_get_attribute_string(group, "viqi_image_dimensions").replace(" ", "").split(",");
        par->viqi_image_content = hdf5_get_attribute_string(group, "viqi_image_content");
        if (par->viqi_image_content == "spectral") {
            par->spectral_wavelengths = hdf5_get_attribute_int_vector(group, "viqi_spectral_wavelengths");
            par->spectral_wavelength_units = hdf5_get_attribute_string(group, "viqi_spectral_wavelengths_units");
        }

        //par->dimensions_image[bim::DIM_X] = par->viqi_image_size[1];
        //par->dimensions_image[bim::DIM_Y] = par->viqi_image_size[0];
        //par->plane_width = par->dimensions_image[bim::DIM_X];
        //par->plane_height = par->dimensions_image[bim::DIM_Y];

        // default 2D image configuration, in case no metadata is defined
        if (par->viqi_image_size.size() > 1) {
            par->dimensions_image[bim::DIM_Y] = par->viqi_image_size[0];
            par->dimensions_image[bim::DIM_X] = par->viqi_image_size[1];
        }

        // do a proper dim test using strings: Y,X,Z,C,...
        for (int d = 0; d < par->viqi_image_dimensions.size(); ++d) {
            if (d >= par->viqi_image_size.size()) break;
            xstring dim_name = par->viqi_image_dimensions[d].toLowerCase();
            std::map<std::string, int>::const_iterator it = bim::meta::dimension_names.find(dim_name);
            if (it == bim::meta::dimension_names.end()) continue;
            int dim_idx = (*it).second;
            if (dim_idx > 1) { // safeguard for initially wrong strings: XYC while need to be YXC
                par->dimensions_indices[d] = dim_idx;
                par->dimensions_image[dim_idx] = par->viqi_image_size[d];
            }
            if (dim_idx > 2) {
                ++par->num_spatial_dimensions;
            }
        }
        info->width = par->dimensions_image[bim::DIM_X];
        info->height = par->dimensions_image[bim::DIM_Y];
        info->number_z = par->dimensions_image[bim::DIM_Z];
        info->number_t = par->dimensions_image[bim::DIM_T];
        info->samples = par->dimensions_image[bim::DIM_C];
        info->number_dims = par->num_spatial_dimensions;

        par->mask_mode = par->viqi_image_content == "mask"; // && par->hdf_pixel_format == H5T_BITFIELD;
        par->values_from_measures = par->viqi_image_content == "mask" || par->viqi_image_content == "heatmap";

        xstring path = par->path + "/level_000/block_0000";
        H5::DataSet dataset = par->file->openDataSet(path.c_str());
        fmtHndl->subFormat = FMT_VIQI;
        setDataConfiguration(fmtHndl, dataset, info);

        xstring path_level = par->path + xstring::xprintf("/level_%.3d", 0);
        group = par->file->openGroup(path_level.c_str());
        par->num_blocks = hdf5_get_attribute_int(group, "viqi_storage_number_blocks");
        if (par->num_blocks < 1) {
            par->num_blocks = (int)hdf5_get_attribute_double(group, "viqi_storage_number_blocks");
        }
        par->num_items = hdf5_get_attribute_int(group, "viqi_storage_sparse_items");
        par->block_bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");
        par->sparse_mode = par->num_items > 0;
    } catch (H5::FileIException error) {
        if (conf && conf->keyExists("-slice-item")) {
            par->item_id = conf->getValueInt("-slice-item");
        } else {
            // process path containing element within
            std::vector<xstring> p = par->path.split("/");
            if (p.size() >= 2) {
                xstring path_suffix = p[p.size() - 1];
                par->path = par->path.replace(xstring("/") + path_suffix, "");
                par->item_id = path_suffix.toInt();
            }
        }

        // read the first available block in order to define pixel-level meta
        try {
            par->block_id = viqi_get_item_block(par, par->path, par->item_id);
        } catch (...) {
            return false;
        }
    }

    fmtHndl->subFormat = FMT_VIQI;

    // if item is requested
    if (par->item_id >= 0 && par->block_id >= 0) {
        xstring path_item = par->path + "/level_000/" + xstring::xprintf("block_%.4d", par->block_id);
        try {
            H5::DataSet dataset = par->file->openDataSet(path_item.c_str());
            par->sparse_element_read = true;
            setDataConfiguration(fmtHndl, dataset, info);

            xstring block_content = hdf5_get_attribute_string(dataset, "viqi_block_content");
            par->mask_mode = block_content == "mask";
            //par->values_from_measures = block_content == "mask" || block_content == "heatmap";
            par->values_from_measures = false;

            par->sparse_mode = hdf5_get_attribute_string(dataset, "viqi_block_format") == "sparse";
            std::vector<int> item_info = viqi_get_item_info(par, path_item, par->block_id, par->item_id);
            bim::Bbox<int> item_bbox(&item_info[2], par->num_spatial_dimensions);

            info->width = item_bbox.getWidth();
            info->height = item_bbox.getHeight();
            par->dimensions_image[bim::DIM_X] = info->width;
            par->dimensions_image[bim::DIM_Y] = info->height;
            //par->dimensions_tile[bim::DIM_X] = std::min(info->width, par->dimensions_tile[bim::DIM_X]);
            //par->dimensions_tile[bim::DIM_Y] = std::min(info->height, par->dimensions_tile[bim::DIM_Y]);
        } catch (H5::FileIException error) {
            return false;
        }
    }

    // init table info
    par->table.init(par->file, par->path);
    if (par->table.measures.size()>0)
        par->dimensions_image[bim::DIM_MEASURE] = par->table.measures.size();

    //par->plane_width = info->width; // dima: not sure why to set this?
    //par->plane_height = info->height;

    // read resolution levels
    par->resolution_levels = 0;
    for (int r = 0; r < 1000; ++r) {
        try {
            xstring path_res = par->path + xstring::xprintf("/level_%.3d", r);
            H5::Group group = par->file->openGroup(path_res.c_str());
            double scale = hdf5_get_attribute_double(group, "viqi_level_scale");
            if (scale == 0) break;
            par->scales.push_back(scale);
            ++par->resolution_levels;
        } catch (H5::FileIException error) {
            break;
        }
    }
    par->resolution_levels = std::max<int>(1, par->resolution_levels);
    info->number_levels = par->resolution_levels;
    info->tileWidth = par->dimensions_tile[bim::DIM_X] > 0 ? par->dimensions_tile[bim::DIM_X] : 128;
    info->tileHeight = par->dimensions_tile[bim::DIM_Y] > 0 ? par->dimensions_tile[bim::DIM_Y] : 128;

    info->imageMode = IM_MULTI;
    if (info->samples == 1) {
        info->imageMode = IM_GRAYSCALE;
    }
    if (par->viqi_image_content == "spectral") {
        info->imageMode = IM_SPECTRAL;
    } else if (par->viqi_image_content == "mask") {
        info->imageMode = IM_MASK;
    } else if (par->viqi_image_content == "heatmap") {
        info->imageMode = IM_HEATMAP;
    }

    return true;
}

//----------------------------------------------------------------------------
// VQI Metadata
//----------------------------------------------------------------------------

void viqi_parse_json_object(bim::TagMap *hash, Jzon::Node &parent_node, const xstring &path) {
    int i = 0;
    for (Jzon::Node::iterator it = parent_node.begin(); it != parent_node.end(); ++it) {
        std::string name = (*it).first;
        Jzon::Node &node = (*it).second;

        if (node.isValid() && node.isArray() && path.endsWith("tie_points/fovs/")) {
            name = xstring::xprintf("%.5d", i);
            if (node.isArray()) {
                size_t sz = node.getCount();
                std::vector<xstring> coord;
                for (int j = 0; j < sz; ++j) {
                    Jzon::Node vn = node.get(j);
                    coord.push_back(vn.toString());
                }
                hash->set_value(path + name, xstring::join(coord, ","));
            }
        } else {
            if (node.isValid()) {
                if (name.size() < 1 && path.endsWith("channels/")) {
                    name = xstring::xprintf("channel_%.5d", i);
                }
                viqi_parse_json_object(hash, node, path + name + '/');
            }
            std::string value = node.toString();
            if (value.size() > 0)
                hash->set_value(path + name, value);
        }
        ++i;
    }
}

void viqi_parse_json(const xstring &str, TagMap *hash) {
    if (str.size() < 1) return;
    Jzon::Parser jparser;
    Jzon::Node rootNode = jparser.parseString(str);
    if (rootNode.isValid()) {
        std::string path = "";
        viqi_parse_json_object(hash, rootNode, path);
    }
}

void viqi_parse_xml(const xstring &str, TagMap *hash) {
    if (str.size() < 1) return;
}

bim::uint viqi_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    //bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // append image pyramid meta
    if (par->resolution_levels > 1 || par->viqi_image_content == "heatmap") {
        //hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_ARBITRARY);
        hash->set_value(bim::IMAGE_NUM_RES_L, par->resolution_levels);
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(par->scales, ","));
    }

    hash->set_value(bim::IMAGE_NUM_X, (int)par->dimensions_image[bim::DIM_X]);
    hash->set_value(bim::IMAGE_NUM_Y, (int)par->dimensions_image[bim::DIM_Y]);

    // return the number of items, fovs (blocks), resolutions, etc...
    if (par->num_blocks > 0) hash->set_value(bim::IMAGE_NUM_FOV, (int)par->num_blocks);
    if (par->num_items > 0) hash->set_value(bim::IMAGE_NUM_ITEMS, (int)par->num_items);

    if (par->viqi_image_content == "spectral") {
        hash->set_value(bim::IMAGE_NUM_SPECTRA, (int)par->dimensions_image[bim::DIM_SPECTRA]);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_SPECTRAL);
    } else if (par->viqi_image_content == "mask") {
        hash->set_value(bim::IMAGE_MODE, par->viqi_image_content);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_MASK);
    } else if (par->viqi_image_content == "heatmap") {
        hash->set_value(bim::IMAGE_MODE, par->viqi_image_content);
        hash->set_value(bim::ICC_TAGS_COLORSPACE, bim::ICC_TAGS_COLORSPACE_HEATMAP);
    }

    // parse all metadata
    if (!conf || !conf->hasKeyWith("-meta")) return 0;

    if (par->spectral_wavelengths.size() > 0) {
        hash->set_value(bim::IMAGE_SPECTRAL_WAVELENGTHS, par->spectral_wavelengths);
        hash->set_value(bim::IMAGE_SPECTRAL_WAVELENGTH_UNITS, par->spectral_wavelength_units);
    }


    if (par->table.nrows > 0) {
        par->table.append_metadata(hash);
    }

    if (conf && !conf->hasKey("-path")) {
        std::vector<xstring> objects;
        H5::Group root = par->file->openGroup("/");
        viqi_walker(par, root, "", objects, 100);

        // augment with pre-defined internal objects
        for (int i = objects.size() - 1; i >= 0; --i) {
            xstring name = objects[i] + "/correction_flatfield";
            if (check_image_node(par, name)) objects.push_back(name);

            name = objects[i] + "/correction_darknoise";
            if (check_image_node(par, name)) objects.push_back(name);

            name = objects[i] + "/correction_background";
            if (check_image_node(par, name)) objects.push_back(name);
        }

        int num_labels = 0;
        int num_previews = 0;
        for (int i = 0; i < objects.size(); ++i) {
            if (objects[i].endsWith("/label")) ++num_labels;
            if (objects[i].endsWith("/preview")) ++num_previews;
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), i), objects[i]);
        }
        hash->set_value(bim::IMAGE_NUM_SERIES, (int)objects.size());
        hash->set_value(bim::IMAGE_NUM_LABELS, num_labels);
        hash->set_value(bim::IMAGE_NUM_PREVIEWS, num_previews);
    }

    try {
        H5::Group group = par->file->openGroup(par->path.c_str());
        xstring metadata_json = hdf5_get_attribute_string(group, "viqi_metadata_json");
        viqi_parse_json(metadata_json, hash);

        xstring metadata_xml = hdf5_get_attribute_string(group, "viqi_metadata_xml");
        viqi_parse_xml(metadata_xml, hash);
    } catch (H5::FileIException error) {
    }

    // update old schema color tags
    for (int i = 0; i < par->image_num_samples; ++i) {
        xstring new_path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i) + bim::CHANNEL_INFO_COLOR;
        xstring old_path = xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i);
        if (hash->hasKey(old_path)) continue;
        xstring val = hash->get_value(new_path);
        if (!val.contains(".")) { // if values are ints
            hash->set_value(old_path, val);
            continue;
        }
        //if values are new format 0-1
        std::vector<double> vd = val.splitDouble(",");
        std::vector<int> vi(vd.size(), 0);
        for (int p = 0; p < vd.size(); ++p) {
            vi[p] = bim::trim<int>(bim::round<double>(vd[p] * 255.0), 0, 255);
        }
        hash->set_value(old_path, xstring::join(vi, ","));
    }

    return 0;
}

//----------------------------------------------------------------------------------------
// ViQi pixels reader
//----------------------------------------------------------------------------------------

void viqi_init_coordinate(FormatHandle *fmtHndl, int x, int y, int sample) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    std::fill(par->positions.begin(), par->positions.end(), 0);
    par->positions[bim::DIM_X] = x;
    par->positions[bim::DIM_Y] = y;
    par->positions[bim::DIM_C] = sample;
    if (conf) {
        par->positions[bim::DIM_Z] = conf->getValueInt("-slice-z", -1);
        par->positions[bim::DIM_T] = conf->getValueInt("-slice-t", -1);
        par->positions[bim::DIM_SERIE] = conf->getValueInt("-slice-serie", 0);
        par->positions[bim::DIM_FOV] = conf->getValueInt("-slice-fov", 0);
        par->positions[bim::DIM_ROTATION] = conf->getValueInt("-slice-rotation", 0);
        par->positions[bim::DIM_SCENE] = conf->getValueInt("-slice-scene", 0);
        par->positions[bim::DIM_ILLUM] = conf->getValueInt("-slice-illum", 0);
        par->positions[bim::DIM_PHASE] = conf->getValueInt("-slice-phase", 0);
        par->positions[bim::DIM_VIEW] = conf->getValueInt("-slice-view", 0);
        par->positions[bim::DIM_ITEM] = conf->getValueInt("-slice-item", 0);
        par->positions[bim::DIM_SPECTRA] = conf->getValueInt("-slice-spectrum", 0);
        par->positions[bim::DIM_MEASURE] = conf->getValueInt("-slice-measure", 0);
    }

    if (info->number_z > 1 && info->number_t <= 1 && par->positions[bim::DIM_Z] < 0) {
        par->positions[bim::DIM_Z] = fmtHndl->pageNumber;
        par->positions[bim::DIM_T] = 0;
    } else if (info->number_z <= 1 && info->number_t > 1 && par->positions[bim::DIM_T] < 0) {
        par->positions[bim::DIM_T] = fmtHndl->pageNumber;
        par->positions[bim::DIM_Z] = 0;
    } else if (info->number_z > 1 && info->number_t > 1 && par->positions[bim::DIM_Z] < 0 && par->positions[bim::DIM_T] < 0) {
        par->positions[bim::DIM_T] = (int)floor(fmtHndl->pageNumber / info->number_z);
        par->positions[bim::DIM_Z] = (int)(fmtHndl->pageNumber - par->positions[bim::DIM_T] * info->number_z);
    }
    par->positions[bim::DIM_Z] = bim::max(0, par->positions[bim::DIM_Z]);
    par->positions[bim::DIM_T] = bim::max(0, par->positions[bim::DIM_T]);
}

void viqi_set_offset_count(FormatHandle *fmtHndl, bim::ImageInfo *info, const int &rank, std::vector<hsize_t> &offset, std::vector<hsize_t> &count) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    std::fill(offset.begin(), offset.end(), 0);
    std::fill(count.begin(), count.end(), 1);

    // set proper XY
    if (rank >= 2) {
        offset[0] = par->positions[bim::DIM_Y];
        offset[1] = par->positions[bim::DIM_X];
        count[0] = info->height;
        count[1] = info->width;
    }

    // set all other dimensions
    for (int d = 2; d < rank; ++d) { // dima:  <= rnk
        offset[d] = par->positions[par->dimensions_indices[d]];
        count[d] = 1;
    }

    // set channels
    if (info->samples > 1) {
        int rnk = rank - 1;
        offset[rnk] = par->positions[bim::DIM_C];
        count[rnk] = 1;
    }

    // set spectral slices
    int sample = par->positions[bim::DIM_C];
    if (rank > 2 && par->viqi_image_content == "spectral" && par->position_spectrum.size() > sample) {
        int rnk = rank - 1;
        offset[rnk] = par->position_spectrum[sample];
        count[rnk] = 1;
    }
}

template<typename T, typename Tw>
void buf_mult(T *buf, bim::int64 sz, double v) {
    T minv = std::numeric_limits<T>::is_integer ? std::numeric_limits<T>::min() : (T)(-1 * (double)std::numeric_limits<T>::max());
    T maxv = std::numeric_limits<T>::max();
    for (bim::int64 x = 0; x < sz; ++x) {
        buf[x] = bim::trim<T, Tw>((Tw)buf[x] * v, minv, maxv);
    }
}

void buffer_multiply(void *buf, bim::int64 sz, double v, int depth, int pf) {
    if (depth == 8 && pf == FMT_UNSIGNED)
        buf_mult<bim::uint8, double>((bim::uint8 *)buf, sz, v);
    else if (depth == 16 && pf == FMT_UNSIGNED)
        buf_mult<bim::uint16, double>((bim::uint16 *)buf, sz, v);
    else if (depth == 32 && pf == FMT_UNSIGNED)
        buf_mult<bim::uint32, double>((bim::uint32 *)buf, sz, v);
    else if (depth == 8 && pf == FMT_SIGNED)
        buf_mult<bim::int8, double>((bim::int8 *)buf, sz, v);
    else if (depth == 16 && pf == FMT_SIGNED)
        buf_mult<bim::int16, double>((bim::int16 *)buf, sz, v);
    else if (depth == 32 && pf == FMT_SIGNED)
        buf_mult<bim::int32, double>((bim::int32 *)buf, sz, v);
    else if (depth == 32 && pf == FMT_FLOAT)
        buf_mult<bim::float32, double>((bim::float32 *)buf, sz, v);
    else if (depth == 64 && pf == FMT_FLOAT)
        buf_mult<bim::float64, double>((bim::float64 *)buf, sz, v);
}

bim::uint viqi_fill_item(H5::DataSet *dataset, FormatHandle *fmtHndl, int *item_info, bim::Image &item_image, bim::Image &item_mask) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    H5::DataType dt = dataset->getDataType();

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset->getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = item_image.height();
    dimsm[1] = item_image.width();
    H5::DataSpace memspace(2, &dimsm[0]);

    // Define memory hyperslab - 2D plane for one channel
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = item_image.height();
    count_out[1] = item_image.width();
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    bool buffer_in_place = false;
    bim::Image buffer;
    if (item_image.depth() == dt.getSize() * 8) {
        buffer_in_place = true;
        buffer = item_image;
    } else {
        buffer.alloc(item_image.width(), item_image.height(), item_image.samples(), dt.getSize() * 8, dt.getClass() != H5T_FLOAT ? FMT_UNSIGNED : FMT_FLOAT);
    }

    // read slabs sample by sample
    for (int sample = 0; sample < (int)item_image.samples(); ++sample) {
        viqi_init_coordinate(fmtHndl, 0, item_info[1], sample);
        viqi_set_offset_count(fmtHndl, item_image.imageInfo(), rank, offset, count);

        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset->read(buffer.sampleBits(sample), dt, memspace, dataspace);
    }

    if (!buffer_in_place) {
        item_image = buffer.convertToDepth(item_image.depth(), bim::Lut::ltTypecast, item_image.pixelType());
        item_mask = dt.getSize() == 1 ? buffer : buffer.convertToDepth(8, bim::Lut::ltTypecast, FMT_UNSIGNED);
    } else {
        item_mask = dt.getSize() == 1 ? buffer.deepCopy() : buffer.convertToDepth(8, bim::Lut::ltTypecast, FMT_UNSIGNED);
    }
    item_mask.fill(255, 0);

    if (par->values_from_measures && par->table.nrows > 0 && par->table.measures.size()>0 && item_image.samples() == 1) {
        //buffer_multiply(bmp->bits[sample], (bim::int64)(info->width * info->height), 255, info->depth, info->pixelType);
        int object_id = item_info[0];
        int measure_id = par->positions[bim::DIM_MEASURE];
        double v = par->table.get_measure_value(object_id, measure_id);
        item_image.fill(v, 0);
    }

    return 0;
}

bim::uint viqi_read_item(FormatHandle *fmtHndl, bim::uint item_id, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    xstring path_item = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, par->block_id);
    H5::DataSet dataset = par->file->openDataSet(path_item.c_str());
    setDataConfiguration(fmtHndl, dataset, info);

    std::vector<int> item_info = viqi_get_item_info(par, path_item, par->block_id, item_id);
    bim::Bbox<int> item_bbox(&item_info[2], par->num_spatial_dimensions);

    // allocate image
    ImageBitmap *bmp = fmtHndl->image;
    info->width = item_bbox.getWidth();
    info->height = item_bbox.getHeight();
    info->samples = par->image_num_samples;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    bim::Image item_image(bmp);
    //item_image.alloc(item_bbox.getWidth(), item_bbox.getHeight(), info->depth, par->image_num_samples, info->pixelType);
    bim::Image item_mask(item_bbox.getWidth(), item_bbox.getHeight(), 8, 1, bim::FMT_UNSIGNED);
    
    try {
        return viqi_fill_item(&dataset, fmtHndl, &item_info[0], item_image, item_mask);
    } catch (...) {
        return 1;
    }
}

bim::uint viqi_fill_sparse_block(H5::DataSet *dataset, FormatHandle *fmtHndl, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
    bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {

    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    bim::Bbox<int> block_bbox(hdf5_get_attribute_int_vector(*dataset, "viqi_block_bbox"));
    int index_sz = par->num_spatial_dimensions * 2 + 2;
    w = w > 0 ? w : block_bbox.getWidth();
    h = h > 0 ? h : block_bbox.getHeight();
    bim::Bbox<int> region_bbox((int)y, (int)x, (int)h, (int)w); //bim::Bbox<int> region_bbox((int)x, (int)y, (int)w, (int)h);

    // reading sparse block
    std::vector<int> block_index;
    xstring path_index = block_path + "_sparse_index";
    hdf5_read_vector_int(par, path_index.c_str(), block_index);
    for (int i = 0; i < block_index.size(); i += index_sz) {
        bim::Bbox<int> item_bbox(&block_index[i + 2], par->num_spatial_dimensions);
        if (!region_bbox.is_intersecting(item_bbox))
            continue;
        if (item_bbox.getWidth() == 0 || item_bbox.getHeight() == 0)
            continue;

        bim::Image item_image(item_bbox.getWidth(), item_bbox.getHeight(), block_image.depth(), block_image.samples(), block_image.pixelType());
        bim::Image item_mask(item_bbox.getWidth(), item_bbox.getHeight(), 8, 1, bim::FMT_UNSIGNED);

        // write into block image
        try {
            if (viqi_fill_item(dataset, fmtHndl, &block_index[i], item_image, item_mask) == 0) { // dima: properly offset N-D planes, need to find proper Z, T, etc offsets
                block_image.renderROI((double)item_bbox.getX() - x, (double)item_bbox.getY() - y, item_image, item_mask);
                mask_image.renderROI((double)item_bbox.getX() - x, (double)item_bbox.getY() - y, item_mask, item_mask);
            }
        } catch (...) {
            // do nothing
        }
    }

    return 0;
}


bim::uint viqi_fill_dense_block(H5::DataSet *dataset, FormatHandle *fmtHndl, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
                                bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    mask_image.fill(255);

    H5::DataType dt = dataset->getDataType();

    ImageBitmap *bmp = block_image.imageBitmap();
    bim::ImageInfo *info = &bmp->i;

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset->getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = info->height;
    dimsm[1] = info->width;
    H5::DataSpace memspace(2, &dimsm[0]);

    // Define memory hyperslab - 2D plane for one channel
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = info->height;
    count_out[1] = info->width;
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    // read slabs sample by sample
    for (int sample = 0; sample < (int)info->samples; ++sample) {
        viqi_init_coordinate(fmtHndl, x, y, sample);
        viqi_set_offset_count(fmtHndl, info, rank, offset, count); // need to add block_bbox

        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset->read(bmp->bits[sample], dt, memspace, dataspace);
    }

    return 0;
}

bim::uint viqi_fill_external_block(H5::DataSet *dataset, FormatHandle *fmtHndl, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
                                   bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    mask_image.fill(255);

    H5::DataType dt = dataset->getDataType();
    if (dt.getClass() != H5T_STRING) return 0;
    size_t sz = dt.getSize();

    // Get the number of strings
    H5::DataSpace dataspace = dataset->getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    if (rank > 1) return 0;
    std::vector<hsize_t> dims(rank);
    dataspace.getSimpleExtentDims(&dims[0]);
    int N = dims[0];

    std::vector<char> rdata(dims[0] * sz);
    dataset->read(&rdata[0], dt);

    std::vector<std::string> paths(dims[0]);
    for (int i = 0; i < dims[0]; ++i) {
        xstring s(sz, 0);
        memcpy((void *)&s[0], (void *)&rdata[i * sz], sz);
        paths[i] = bim::fspath_fix_slashes(par->file_path + "/" + s);
    }

    Image img(paths[0]); // , 0, 0, x, y, x + w, y + h);
    for (int i = 1; i < N; ++i) {
        Image img2(paths[i]); //, 0, 0, x, y, x + w, y + h);
        img = img.appendChannels(img2);
    }

    // rotate and crop
    std::vector<int> transform = hdf5_get_attribute_int_vector(*dataset, "viqi_block_transform");
    if (transform.size() >= 6 && transform[0] == 0 && transform[1] == -1 && transform[2] == 0 && transform[3] == 1 && transform[4] == 0 && transform[5] == 0) // Clockwise: [[0, -1, 0], [1, 0, 0]]
        img = img.rotate(90);
    else if (transform.size() >= 6 && transform[0] == 0 && transform[1] == 1 && transform[2] == 0 && transform[3] == -1 && transform[4] == 0 && transform[5] == 0) //Counter clockwise : [[0, 1, 0], [-1, 0, 0]]
        img = img.rotate(-90);

    img = img.ROI(x, y, w, h);
    block_image.copy(img);

    return 0;
}

bim::uint viqi_fill_heatmap_block(H5::DataSet *dataset, FormatHandle *fmtHndl, const xstring &block_path, bim::Image &block_image, bim::Image &mask_image,
                                  bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0) {

    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    mask_image.fill(255);

    std::vector<int> block_index;
    xstring path_index = block_path + "_sparse_index";
    hdf5_read_vector_int(par, path_index.c_str(), block_index);

    int index_sz = par->num_spatial_dimensions * 2 + 2;
    int samples_num = block_index.size() / index_sz;

    // dima: we currently only support gridded heatmaps 
    // find the heatmap grid extent
    bim::Bbox<int> bbox1(&block_index[0 * index_sz + 2], par->num_spatial_dimensions);
    int first_x = bbox1.getX();
    int first_y = bbox1.getY();
    std::set<int> samples_x;
    std::set<int> samples_y;
    for (int i = 0; i < block_index.size(); i += index_sz) {
        bim::Bbox<int> item_bbox(&block_index[i + 2], par->num_spatial_dimensions);

        // find the extent using only the first row and col
        //if (first_y == item_bbox.getY()) samples_x.insert(item_bbox.getX());
        //if (first_x == item_bbox.getX()) samples_y.insert(item_bbox.getY());
        
        // we can be resilient to missing samples in the grid but all coordinates must be exactly the same!!!!!
        samples_x.insert(item_bbox.getX());
        samples_y.insert(item_bbox.getY());
    }

    // compute steps
    std::set<int>::iterator it = samples_x.begin();
    int step_x = abs( *(it) - *(++it));
    it = samples_y.begin();
    int step_y = abs(*(it) - *(++it));
    
    int NW = samples_x.size();
    int NH = samples_y.size();
    //int idx_first = block_index[0* index_sz + 0];
    //int idx_last = block_index[(samples_num-1)*index_sz + 0];

    // compute grid size in current pixel size, bboxes may be sized 0
    int heatmap_w = bbox1.getWidth() > 0 ? (NW - 1)*step_x + bbox1.getWidth() : NW * step_x;
    int heatmap_h = bbox1.getHeight() > 0 ? (NH - 1)*step_y + bbox1.getHeight() : NH * step_y;
    
    // create original measure grid
    bim::Image img(NW, NH, block_image.depth(), block_image.samples(), block_image.pixelType());
    img.fill(0);

    viqi_init_coordinate(fmtHndl, 0, 0, 0);
    int measure_id = par->positions[bim::DIM_MEASURE];
    
    for (int i = 0; i < block_index.size(); i += index_sz) {
        bim::Bbox<int> item_bbox(&block_index[i + 2], par->num_spatial_dimensions);
        int object_id = block_index[i + 0];
        
        //int xx = bim::trim<float>(bim::round<float>(item_bbox.getX() / (float)step_x), 0, NW - 1);
        //int yy = bim::trim<float>(bim::round<float>(item_bbox.getY() / (float)step_y), 0, NH - 1);
        int xx = bim::trim<float>(floor(item_bbox.getX() / (float)step_x), 0, NW - 1);
        int yy = bim::trim<float>(floor(item_bbox.getY() / (float)step_y), 0, NH - 1);

        double v = par->table.get_measure_value(object_id, measure_id);
        img.setPixelValue(0, xx, yy, v);
    }

    // resize original measure grid to desired size
    if (!par->user_given_interpolation) {
        VQITableMeasure *m = par->table.getMeasure(measure_id);
        par->interpolation = m->interpolation;
    }
    img = img.resample(heatmap_w, heatmap_h, par->interpolation);
    
    if (x == 0 && y == 0) {
        block_image.setROI(first_x, first_y, img);
    } else {
        double rx = first_x - (double) x;
        double ry = first_y - (double) y;
        block_image.renderROI(rx, ry, img);
    }

    return 0;
}

bim::uint viqi_fill_block(bim::Image &block_image, bim::Image &mask_image, FormatHandle *fmtHndl, bim::uint block_id,
                          bim::uint x = 0, bim::uint y = 0, bim::uint w = 0, bim::uint h = 0, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    block_image.fill(0);
    if (mask_image.depth() != 8)
        mask_image = mask_image.normalize();
    mask_image.fill(0);

    xstring path_block = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, block_id);
    // sparse block may be absent and means an empty image
    H5::DataSet dataset;
    try {
        dataset = par->file->openDataSet(path_block.c_str());
    } catch (H5::FileIException error) {
        return 0;
    }

    //bool sparse_mode = hdf5_get_attribute_string(dataset, "viqi_block_format") == "sparse";
    xstring block_format = hdf5_get_attribute_string(dataset, "viqi_block_format");
    xstring external_mode = hdf5_get_attribute_string(dataset, "viqi_block_type");

    if (external_mode == "libbioimage") {
        return viqi_fill_external_block(&dataset, fmtHndl, path_block, block_image, mask_image, x, y, w, h);
    } else if (block_format == "heatmap") { //|| par->viqi_image_content == "heatmap") { // dima: temporary test
        return viqi_fill_heatmap_block(&dataset, fmtHndl, path_block, block_image, mask_image, x, y, w, h);
    } else if (block_format == "sparse") {
        return viqi_fill_sparse_block(&dataset, fmtHndl, path_block, block_image, mask_image, x, y, w, h);
    } else {
        return viqi_fill_dense_block(&dataset, fmtHndl, path_block, block_image, mask_image, x, y, w, h); // dima: properly offset N-D planes
    }

    // unknown block type
    return 1;
}

bim::uint viqi_read_block(FormatHandle *fmtHndl, bim::uint block_id, bim::uint x, bim::uint y, bim::uint w, bim::uint h, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    int bboxsz = par->num_spatial_dimensions * 2;

    // sparse block may be absent and means an empty image
    xstring path_block = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, block_id);
    H5::DataSet dataset;
    try {
        dataset = par->file->openDataSet(path_block.c_str());
    } catch (H5::FileIException error) {
        bim::Bbox<int> block_bbox(par->num_spatial_dimensions);

        // read bbox from level index if available, otherwise return 1x1 image
        if (par->block_bboxs.size() == 0 || par->loaded_level != level) {
            par->loaded_level = level;
            xstring path_level = par->path + xstring::xprintf("/level_%.3d", level);

            try {
                H5::Group group = par->file->openGroup(path_level.c_str());
                par->num_blocks = hdf5_get_attribute_int(group, "viqi_storage_number_blocks");
                if (par->num_blocks < 1) {
                    par->num_blocks = (int)hdf5_get_attribute_double(group, "viqi_storage_number_blocks");
                }
                par->num_items = hdf5_get_attribute_int(group, "viqi_storage_sparse_items");
                par->block_bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");
            } catch (H5::FileIException error) {
                return 1;
            }
        }

        int offset = block_id * bboxsz;
        if (par->block_bboxs.size() >= offset + bboxsz) {
            block_bbox.set(&par->block_bboxs[offset], par->num_spatial_dimensions);
        }

        info->width = block_bbox.getWidth();
        info->height = block_bbox.getHeight();
        info->samples = par->image_num_samples;
        ImageBitmap *bmp = fmtHndl->image;
        if (allocImg(fmtHndl, info, bmp) != 0) return 1;
        bim::Image block_image(bmp);
        block_image.fill(0);
        return 0;
    }

    // ok, block was found
    par->values_from_measures = hdf5_get_attribute_string(dataset, "viqi_block_format") == "dense" ? false : par->values_from_measures;
    setDataConfiguration(fmtHndl, dataset, info);

    bim::Bbox<int> block_bbox(hdf5_get_attribute_int_vector(dataset, "viqi_block_bbox"));
    info->width = block_bbox.getWidth();
    info->height = block_bbox.getHeight();

    // allocate output image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    bim::Image block_image(bmp);
    //bim::Image mask_image(block_image.width(), block_image.height(), 8, 1, FMT_UNSIGNED);
    bim::Image mask_image; // set empty mask

    return viqi_fill_block(block_image, mask_image, fmtHndl, block_id, x, y, w, h, level);
}

bim::uint viqi_read_region(FormatHandle *fmtHndl, bim::int64 x, bim::int64 y, bim::int64 w, bim::int64 h, int level = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;

    XConf *conf = fmtHndl->arguments;
    if (conf && conf->keyExists("-slice-spectrum")) {
        //par->position_spectrum = conf->getValueInt("-slice-spectrum");
        par->position_spectrum = conf->getDimensionSlices("-slice-spectrum");
    }

    // constrain to image dimensions
    float scale = (float)(1.0 / pow(2.0, (float)level));
    int plane_width = bim::round<int>(par->dimensions_image[bim::DIM_X] * scale);
    int plane_height = bim::round<int>(par->dimensions_image[bim::DIM_Y] * scale);
    w = w > 0 ? w : plane_width;
    h = h > 0 ? h : plane_height;
    w = bim::min<bim::uint64>(w, plane_width - x);
    h = bim::min<bim::uint64>(h, plane_height - y);

    if (par->block_bboxs.size() == 0 || par->loaded_level != level) {
        par->loaded_level = level;
        xstring path_level = par->path + xstring::xprintf("/level_%.3d", level);

        try {
            H5::Group group = par->file->openGroup(path_level.c_str());
            par->num_blocks = hdf5_get_attribute_int(group, "viqi_storage_number_blocks");
            if (par->num_blocks < 1) {
                par->num_blocks = (int)hdf5_get_attribute_double(group, "viqi_storage_number_blocks");
            }
            par->num_items = hdf5_get_attribute_int(group, "viqi_storage_sparse_items");
            par->block_bboxs = hdf5_get_attribute_int_vector(group, "viqi_storage_block_bboxes");
        } catch (H5::FileIException error) {
            return 1;
        }
    }

    xstring path_block = par->path + xstring::xprintf("/level_%.3d/block_%.4d", level, 0);
    H5::DataSet dataset = par->file->openDataSet(path_block.c_str());
    par->values_from_measures = hdf5_get_attribute_string(dataset, "viqi_block_format") == "dense" ? false : par->values_from_measures;
    setDataConfiguration(fmtHndl, dataset, info);

    // allocate output image
    info->width = w;
    info->height = h;

    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;
    bim::Image region_image(bmp);
    region_image.fill(0);

    int bboxsz = par->num_spatial_dimensions * 2;
    int block_id = 0;
    bim::Bbox<int> region_bbox((int)y, (int)x, (int)h, (int)w); //bim::Bbox<int> region_bbox((int)x, (int)y, (int)w, (int)h);
    for (int i = 0, idx = 0; i < par->block_bboxs.size(); i += bboxsz, ++idx) {
        bim::Bbox<int> block_bbox(&par->block_bboxs[i], par->num_spatial_dimensions);
        if (!region_bbox.is_intersecting(block_bbox))
            continue;

        // compute in-block offsets and counts
        double bx = std::max<double>((double)block_bbox.getX() - x, 0);
        double by = std::max<double>((double)block_bbox.getY() - y, 0);
        double sx = fabs(std::min<double>((double)block_bbox.getX() - x, 0));
        double sy = fabs(std::min<double>((double)block_bbox.getY() - y, 0));
        double sw = std::min<double>(block_bbox.getWidth() - sx, w - bx);
        double sh = std::min<double>(block_bbox.getHeight() - sy, h - by);
        if (sw < 1 || sh < 1) continue;

        bim::Image block_image(sw, sh, region_image.depth(), region_image.samples(), region_image.pixelType());
        bim::Image mask_image(sw, sh, 8, 1, FMT_UNSIGNED);
        viqi_fill_block(block_image, mask_image, fmtHndl, idx, sx, sy, sw, sh, level);

        region_image.renderROI(bx, by, block_image, mask_image);
        ++block_id;
    }

    return 0;
}

bim::uint viqi_read_pixels(FormatHandle *fmtHndl, bim::uint page, int x = 0, int y = 0, int w = 0, int h = 0, int l = 0) {
    if (fmtHndl == NULL) return 1;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;

    if (conf && conf->keyExists("-slice-spectrum")) {
        par->position_spectrum = conf->getDimensionSlices("-slice-spectrum");
    }

    // reading blocks
    if (conf && conf->keyExists("-slice-fov")) {
        par->block_id = conf->getValueInt("-slice-fov");
        return viqi_read_block(fmtHndl, par->block_id, x, y, w, h, l);
    }

    if (x == 0 && y == 0 && w == 0 && h == 0 && l == 0 && par->block_id < 0 && par->item_id < 0 && par->dimensions_image[bim::DIM_X] < 1 && par->dimensions_image[bim::DIM_Y] < 1) {
        par->block_id = 0;
        par->item_id = 0;
    }

    // reading only one item
    if (par->block_id >= 0 && par->item_id >= 0) {
        return viqi_read_item(fmtHndl, par->item_id, l);
    }

    // read tiles and levels combining blocks
    return viqi_read_region(fmtHndl, x, y, w, h, l);
}

//****************************************************************************
// sub-formats: Dream 3D
//****************************************************************************

bim::uint dream3d_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    //bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // parse all metadata
    if (conf && conf->hasKeyWith("-meta")) {
        try {
            std::vector<double> spacing;
            hdf5_read_vector(par, "/DataContainers/ImageDataContainer/_SIMPL_GEOMETRY/SPACING", spacing);
            hash->set_value(bim::PIXEL_RESOLUTION_X.c_str(), spacing[1]);
            hash->set_value(bim::PIXEL_RESOLUTION_Y.c_str(), spacing[0]);
            hash->set_value(bim::PIXEL_RESOLUTION_Z.c_str(), spacing[2]);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X.c_str(), bim::PIXEL_RESOLUTION_UNIT_MICRONS);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y.c_str(), bim::PIXEL_RESOLUTION_UNIT_MICRONS);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z.c_str(), bim::PIXEL_RESOLUTION_UNIT_MICRONS);
            hash->set_value("Dream3D/spacing", spacing);
        } catch (...) {
            // do nothing
        }

        try {
            std::vector<double> origin;
            hdf5_read_vector(par, "/DataContainers/ImageDataContainer/_SIMPL_GEOMETRY/ORIGIN", origin);
            hash->set_value("Dream3D/origin", origin);
        } catch (...) {
            // do nothing
        }

        try {
            std::vector<double> dimensions;
            hdf5_read_vector(par, "/DataContainers/ImageDataContainer/_SIMPL_GEOMETRY/DIMENSIONS", dimensions);
            hash->set_value("Dream3D/dimensions", dimensions);
        } catch (...) {
            // do nothing
        }
    }

    return 0;
}

//****************************************************************************
// Base HDF5
//****************************************************************************

void hdf5GetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    *info = initImageInfo();
    XConf *conf = fmtHndl->arguments;

    if (conf)
        par->path_requested = conf->getValue("-path");

    // determine file sub-type
    fmtHndl->subFormat = FMT_HDF5;
    if (par->file->attrExists("ImarisVersion")) {
        fmtHndl->subFormat = FMT_IMS;
        imsGetImageInfo(fmtHndl);
        return;
    } else if (par->file->attrExists("DREAM3D Version")) {
        fmtHndl->subFormat = FMT_DREAM3D;
        //d3dGetImageInfo(fmtHndl);
    } else if (par->file->attrExists("viqi_storage_type")) {
        bool valid = viqiGetImageInfo(fmtHndl);
        if (valid) {
            fmtHndl->subFormat = FMT_VIQI;
            return; // otherwise read as standrd HDF-5
        }
    }

    // detect first element to list if path is empty
    if (conf) {
        par->path = conf->getValue("-path");
    }
    if (par->path.size() < 1) {
        std::vector<xstring> objects;
        H5::Group root = par->file->openGroup("/");
        hdf5_walker(par, root, "", objects, 1);
        par->path = objects[0];
    }

    H5::DataSet dataset = par->file->openDataSet(par->path.c_str());
    setDataConfiguration(fmtHndl, dataset, info);

    info->tileWidth = par->dimensions_tile[bim::DIM_X] > 0 ? par->dimensions_tile[bim::DIM_X] : 128;
    info->tileHeight = par->dimensions_tile[bim::DIM_Y] > 0 ? par->dimensions_tile[bim::DIM_Y] : 128;

    info->resUnits = RES_IN;
    info->xRes = 0;
    info->yRes = 0;
}

void hdf5CloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    if (par == NULL) return;
    //par->close();
    xclose(fmtHndl);
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint hdf5OpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) hdf5CloseImageProc(fmtHndl);
    bim::HDF5Params *par = new bim::HDF5Params();
    fmtHndl->internalParams = (void *)par;

    if (io_mode == IO_WRITE) {
        return 1;
    }

    try {
        par->open(fmtHndl->fileName, io_mode);
        hdf5GetImageInfo(fmtHndl);
    } catch (...) {
        hdf5CloseImageProc(fmtHndl);
        return 1;
    }

    return 0;
}

bim::uint hdf5GetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    return (bim::uint)info->number_pages;
}

bim::ImageInfo hdf5GetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    bim::ImageInfo ii = initImageInfo();
    if (fmtHndl == NULL) return ii;
    fmtHndl->pageNumber = page_num;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;
    if (conf && par->path_requested != conf->getValue("-path")) {
        hdf5GetImageInfo(fmtHndl);
    }
    return par->i;
}


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

bim::uint hdf5_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (!hash) return 1;
    if (isCustomReading(fmtHndl)) return 1;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    // report original tile sizes
    if (par->dimensions_tile[bim::DIM_X] > 0 && par->dimensions_tile[bim::DIM_Y] > 0) {
        hash->set_value("tile_num_original_x", par->dimensions_tile[bim::DIM_X]);
        hash->set_value("tile_num_original_y", par->dimensions_tile[bim::DIM_Y]);
        if (par->dimensions_tile[bim::DIM_Z]) {
            hash->set_value("tile_num_original_z", par->dimensions_tile[bim::DIM_Z]);
        }
    }

    /*
    // append image pyramid meta
    if (o->scales.size() > 1) {
        std::vector<double> scales(o->scales.begin(), o->scales.end());
        std::sort(scales.begin(), scales.end(), std::greater<double>());
        hash->set_value(bim::IMAGE_NUM_RES_L_ACTUAL, (int)scales.size());
        hash->set_value(bim::IMAGE_RES_L_SCALES_ACTUAL, xstring::join(scales, ","));

        // dima: virtual structure
        hash->set_value(bim::IMAGE_RES_STRUCTURE, bim::IMAGE_RES_STRUCTURE_HIERARCHICAL);
        hash->set_value(bim::IMAGE_NUM_RES_L, (int)o->scales_virtual.size());
        hash->set_value(bim::IMAGE_RES_L_SCALES, xstring::join(o->scales_virtual, ","));
    }


    hash->set_value(bim::IMAGE_NUM_SERIES, bim::max<int>(1, o->scenes.size()));
    */

    if (fmtHndl->subFormat == FMT_IMS) {
        return ims_append_metadata(fmtHndl, hash);
    } else if (fmtHndl->subFormat == FMT_VIQI) {
        return viqi_append_metadata(fmtHndl, hash);
    } else if (fmtHndl->subFormat == FMT_DREAM3D) {
        dream3d_append_metadata(fmtHndl, hash);
    }

    if (par->path.size() > 0) {
        hash->set_value(bim::IMAGE_CURRENT_PATH, par->path);
    }

    // parse the defined path and add attribute of each parent
    if (conf && conf->hasKeyWith("-meta")) {
        xstring p = xstring(" ") + par->path;
        std::vector<xstring> paths = p.split("/");
        paths[0] = "";
        xstring h5path = "";
        bim::xstring meta_path = "HDF5";

        for (int i = 0; i < paths.size(); ++i) {
            if (paths[i].size() > 0) {
                h5path += "/";
                h5path += paths[i];
                meta_path += "/";
                meta_path += paths[i];
            }
            hdf5_append_attributes(par, hash, h5path, meta_path);
        }
    }

    // parse at most 10 paths to compatible matrices
    if (conf && conf->hasKeyWith("-meta")) {
        std::vector<xstring> objects;
        xstring h5path = "";
        H5::Group root = par->file->openGroup("/");
        hdf5_walker(par, root, h5path, objects, 10); // dima: max of 10 paths
        // append objects into meta
        for (int i = 0; i < objects.size(); ++i) {
            hash->set_value(xstring::xprintf("%s/%.5d", bim::IMAGE_SERIES_PATHS.c_str(), i), objects[i]);
        }
    }

    return 0;
}

//----------------------------------------------------------------------------
// READ
//----------------------------------------------------------------------------

bim::uint hdf5_read_pixels(FormatHandle *fmtHndl, bim::uint page, int x = 0, int y = 0, int w = 0, int h = 0) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    bim::ImageInfo *info = &par->i;
    //XConf *conf = fmtHndl->arguments;

    H5::DataSet dataset = par->file->openDataSet(par->path.c_str());
    H5::DataType dt = dataset.getDataType();
    int z, t;
    hdf5_init_coordinate(fmtHndl, page, z, t);
    if (w > 0 && h > 0) {
        info->width = w;
        info->height = h;
    }

    // Get the number of dimensions in the dataspace
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    std::vector<hsize_t> dims(rank);
    int ndims = dataspace.getSimpleExtentDims(&dims[0]);

    // allocate image
    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    // Define the memory dataspace - 2D plane for one channel
    std::vector<hsize_t> dimsm(2, 1);
    dimsm[0] = info->height;
    dimsm[1] = info->width;
    H5::DataSpace memspace(2, &dimsm[0]);

    // Define memory hyperslab - 2D plane for one channel
    std::vector<hsize_t> offset_out(rank, 0); // hyperslab offset in memory
    std::vector<hsize_t> count_out(rank, 1);  // size of the hyperslab in memory
    count_out[0] = info->height;
    count_out[1] = info->width;
    memspace.selectHyperslab(H5S_SELECT_SET, &count_out[0], &offset_out[0]);

    std::vector<hsize_t> offset(rank, 0); // hyperslab offset in the file
    std::vector<hsize_t> count(rank, 1);  // size of the hyperslab in the file

    // read slabs sample by sample
    for (int sample = 0; sample < (int)info->samples; ++sample) {

        // Define hyperslab in the dataset; implicitly passing stride and block as NULL
        hdf5_set_offset_count(fmtHndl, info, dims, rank, sample, z, t, offset, count, x, y);
        dataspace.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0]);

        // Read data from hyperslab in the file into the hyperslab in memory
        dataset.read(bmp->bits[sample], dt, memspace, dataspace);
    }

    return 0;
}

bim::uint hdf5ReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    try {
        if (fmtHndl->subFormat == FMT_IMS) {
            return ims_read_pixels(fmtHndl, page, 0, 0, 0, 0, 0);
        } else if (fmtHndl->subFormat == FMT_VIQI) {
            return viqi_read_pixels(fmtHndl, page, 0, 0, par->dimensions_image[bim::DIM_X], par->dimensions_image[bim::DIM_Y], 0);
        }
        return hdf5_read_pixels(fmtHndl, page);
    } catch (...) {
        // do nothing
    }
    return 1;
}

bim::uint hdf5ReadImageLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;

    try {
        if (fmtHndl->subFormat == FMT_IMS) {
            return ims_read_pixels(fmtHndl, page, 0, 0, 0, 0, level);
        } else if (fmtHndl->subFormat == FMT_VIQI) {
            float scale = (float)(1.0 / pow(2.0, (float)level));
            int plane_width = (int)floor(par->dimensions_image[bim::DIM_X] * scale);
            int plane_height = (int)floor(par->dimensions_image[bim::DIM_Y] * scale);
            return viqi_read_pixels(fmtHndl, page, 0, 0, plane_width, plane_height, level);
        } else if (fmtHndl->subFormat != FMT_IMS && level == 0) {
            return hdf5_read_pixels(fmtHndl, page);
        }
    } catch (...) {
        // do nothing
    }
    return 1;
}

bim::uint hdf5ReadImageTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    bim::HDF5Params *par = (bim::HDF5Params *)fmtHndl->internalParams;
    fmtHndl->pageNumber = page;
    ImageInfo *info = &par->i;

    float scale = (float)(1.0 / pow(2.0, (float)level));
    int plane_width = (int)floor(par->dimensions_image[bim::DIM_X] * scale);
    int plane_height = (int)floor(par->dimensions_image[bim::DIM_Y] * scale);
    int x = (int)(xid * info->tileWidth);
    int y = (int)(yid * info->tileHeight);
    int w = bim::min<int>((int)info->tileWidth, plane_width - x);
    int h = bim::min<int>((int)info->tileHeight, plane_height - y);

    try {
        if (fmtHndl->subFormat == FMT_IMS) {
            return ims_read_pixels(fmtHndl, page, x, y, w, h, level);
        } else if (fmtHndl->subFormat == FMT_VIQI) {
            return viqi_read_pixels(fmtHndl, page, x, y, w, h, level);
        } else if (fmtHndl->subFormat != FMT_IMS && level == 0) {
            return hdf5_read_pixels(fmtHndl, page, x, y, w, h);
        }
    } catch (...) {
        // do nothing
    }
    return 1;
}

bim::uint hdf5ReadImageRegionProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    fmtHndl->pageNumber = page;
    try {
        if (fmtHndl->subFormat == FMT_VIQI) {
            return viqi_read_region(fmtHndl, x1, y1, x2 - x1 + 1, y2 - y1 + 1, level);
        }
    } catch (...) {
        // do nothing
    }
    return 1;
}

//****************************************************************************
// exported
//****************************************************************************

#define BIM_HDF5_NUM_FORMATS 4

FormatItem hdf5Items[BIM_HDF5_NUM_FORMATS] = {
    {                               //0
      "HDF5",                       // short name, no spaces
      "HDF-5",                      // Long format name
      "hdf|h5|he2|hdf5|he5|h5ebsd", // pipe "|" separated supported extension list
      1,                            //canRead;      // 0 - NO, 1 - YES
      0,                            //canWrite;     // 0 - NO, 1 - YES
      1,                            //canReadMeta;  // 0 - NO, 1 - YES
      0,                            //canWriteMeta; // 0 - NO, 1 - YES
      0,                            //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {             //1
      "IMARIS5",  // short name, no spaces
      "Imaris 5", // Long format name
      "ims",      // pipe "|" separated supported extension list
      1,          //canRead;      // 0 - NO, 1 - YES
      0,          //canWrite;     // 0 - NO, 1 - YES
      1,          //canReadMeta;  // 0 - NO, 1 - YES
      0,          //canWriteMeta; // 0 - NO, 1 - YES
      0,          //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {            //2
      "DREAM3D", // short name, no spaces
      "Dream3D", // Long format name
      "dream3d", // pipe "|" separated supported extension list
      1,         //canRead;      // 0 - NO, 1 - YES
      0,         //canWrite;     // 0 - NO, 1 - YES
      1,         //canReadMeta;  // 0 - NO, 1 - YES
      0,         //canWriteMeta; // 0 - NO, 1 - YES
      0,         //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } },
    {                      // 3
      "ViQi",              // short name, no spaces
      "ViQi Image Format", // Long format name
      "hdf|h5|hdf5|vqi",   // pipe "|" separated supported extension list
      1,                   //canRead;      // 0 - NO, 1 - YES
      0,                   //canWrite;     // 0 - NO, 1 - YES
      1,                   //canReadMeta;  // 0 - NO, 1 - YES
      0,                   //canWriteMeta; // 0 - NO, 1 - YES
      0,                   //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } }
};


FormatHeader hdf5Header = {

    sizeof(FormatHeader),
    "1.10.5",
    "HDF5",
    "HDF-5",

    BIM_FORMAT_HDF5_MAGIC_SIZE,
    { 1, BIM_HDF5_NUM_FORMATS, hdf5Items },

    hdf5ValidateFormatProc,
    // begin
    hdf5AquireFormatProc, //AquireFormatProc
    // end
    hdf5ReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    hdf5OpenImageProc,  //OpenImageProc
    hdf5CloseImageProc, //CloseImageProc

    // info
    hdf5GetNumPagesProc,  //GetNumPagesProc
    hdf5GetImageInfoProc, //GetImageInfoProc

    // read/write
    hdf5ReadImageProc,       //ReadImageProc
    NULL,                    //WriteImageProc
    hdf5ReadImageTileProc,   //ReadImageTileProc
    NULL,                    //WriteImageTileProc
    hdf5ReadImageLevelProc,  //ReadImageLevelProc
    NULL,                    //WriteImageLineProc
    NULL,                    //ReadImageThumbProc
    NULL,                    //WriteImageThumbProc
    NULL,                    //ReadImagePreviewProc
    hdf5ReadImageRegionProc, //ReadImageRegionProc
    NULL,                    //WriteImageRegionProc

    // meta data
    NULL,                 //ReadMetaDataAsTextProc
    hdf5_append_metadata, //AppendMetaDataProc

    NULL,
    NULL,
    ""

};

extern "C" {

FormatHeader *hdf5GetFormatHeader(void) {
    return &hdf5Header;
}

} // extern C
