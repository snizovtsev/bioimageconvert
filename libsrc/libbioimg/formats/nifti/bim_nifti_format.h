/*****************************************************************************
  NIFTI support 
  Copyright (c) 2015, Center for Bio-Image Informatics, UCSB
  Copyright (c) 2015, Dmitry Fedorov <www.dimin.net> <dima@dimin.net>
  
  Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
    2013-01-12 14:13:40 - First creation
        
  ver : 1
*****************************************************************************/

#ifndef BIM_NIFTI_FORMAT_H
#define BIM_NIFTI_FORMAT_H

#include <cstdio>
#include <fstream>
#include <map>
#include <string>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *niftiGetFormatHeader(void);
}

#include <nifti2.h>
#include <nifti2_io.h>

namespace bim {

//----------------------------------------------------------------------------
// internal format defs
//----------------------------------------------------------------------------

class NIFTIParams {
public:
    static std::map<int, xstring> intents;
    static std::map<int, xstring> xforms;

public:
    NIFTIParams();
    ~NIFTIParams();

    ImageInfo i;
    nifti_image *nim = NULL;
};

} // namespace bim

#endif // BIM_NIFTI_FORMAT_H
