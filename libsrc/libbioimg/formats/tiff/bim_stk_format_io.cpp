/*****************************************************************************
STK file format

Author: Dmitry Fedorov <www.dimin.net> <dima@dimin.net>
Copyright (C) 2020 ViQi Inc

History:
04/20/2004 23:33 - First creation
2020-03-27 23:46 - Complete rewrite and support for new formats

ver: 2
*****************************************************************************/

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include <string>

#include <bim_format_misc.h>
#include <bim_img_format_utils.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>

//#include "bim_stk_format.h"

#include "bim_tiff_format.h"
#include "bim_tiny_tiff.h"
#include "memio.h"
#include "xtiffio.h"

// Disables Visual Studio 2005 warnings for deprecated code
#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
#pragma warning(disable : 4996)
#endif

using namespace bim;

void invertSample(ImageBitmap *img, const int &sample);

//----------------------------------------------------------------------------
// misc
//----------------------------------------------------------------------------

inline double rational2float(StkRational v) {
    if (v.den != 0)
        return v.num / (double)v.den;
    return 0;
}

void appendRational(std::vector<StkRational> &v, const std::string &tag_name, TagMap *hash) {
    int N = v.size();
    if (N < 1) return;
    std::vector<double> vec(N, 0);
    for (bim::uint i = 0; i < N; ++i) {
        if (v[i].den != 0) {
            vec[i] = v[i].num / (double)v[i].den;
        }
    }
    hash->set_value(tag_name, vec);
}

void appendPosition(std::vector<StkPosition> &v, const std::string &tag_name, TagMap *hash) {
    std::vector<bim::Coord2f> vec(v.size());
    for (int i = 0; i < v.size(); ++i) {
        if (v[i].x.den != 0 && v[i].y.den != 0) {
            vec[i] = std::make_tuple<double, double>(v[i].x.num / (double)v[i].x.den, v[i].y.num / (double)v[i].y.den);
        } else {
            vec[i] = std::make_tuple<double, double>(0, 0);
        }
    }
    hash->set_value(tag_name, vec);
}

//----------------------------------------------------------------------------
// STK class
//----------------------------------------------------------------------------

StkInfo::StkInfo() {
    init();
}

StkInfo::~StkInfo() {}

void StkInfo::init(bim::int32 N) {
    this->N = N;
    this->strips_per_plane = 0;
}

//----------------------------------------------------------------------------
// STK MISC FUNCTIONS
//----------------------------------------------------------------------------

#define HOURS_PER_DAY 24
#define MINS_PER_HOUR 60
#define SECS_PER_MIN 60
#define MSECS_PER_SEC 1000
#define MINS_PER_DAY (HOURS_PER_DAY * MINS_PER_HOUR)
#define SECS_PER_DAY (MINS_PER_DAY * SECS_PER_MIN)
#define MSECS_PER_DAY (SECS_PER_DAY * MSECS_PER_SEC)

void JulianToYMD(bim::uint32 julian, unsigned short &year, unsigned char &month, unsigned char &day) {
    year = 0;
    month = 0;
    day = 0;
    if (julian == 0) return;

    bim::int32 a, b, c, d, e, alpha;
    bim::int32 z = julian + 1;

    // dealing with Gregorian calendar reform
    if (z < 2299161L) {
        a = z;
    } else {
        alpha = (bim::int32)((z - 1867216.25) / 36524.25);
        a = z + 1 + alpha - alpha / 4;
    }

    b = (a > 1721423L ? a + 1524 : a + 1158);
    c = (bim::int32)((b - 122.1) / 365.25);
    d = (bim::int32)(365.25 * c);
    e = (bim::int32)((b - d) / 30.6001);

    day = (unsigned char)(b - d - (bim::int32)(30.6001 * e));
    month = (unsigned char)((e < 13.5) ? e - 1 : e - 13); // -1 dima: sub 1 to match Date/Time TIFF tag, dima: old fix, not needed for new
    year = (short)((month > 2.5) ? (c - 4716) : c - 4715);
}

bim::uint32 YMDToJulian(unsigned short year, unsigned char month, unsigned char day) {
    short a, b = 0;
    short work_year = year;
    short work_month = month;
    short work_day = day;

    // correct for negative year
    if (work_year < 0) {
        work_year++;
    }

    if (work_month <= 2) {
        work_year--;
        work_month += (short)12;
    }

    // deal with Gregorian calendar
    if (work_year * 10000. + work_month * 100. + work_day >= 15821015.) {
        a = (short)(work_year / 100.);
        b = (short)(2 - a + a / 4);
    }

    return (bim::int32)(365.25 * work_year) +
           (bim::int32)(30.6001 * (work_month + 1)) +
           work_day + 1720994L + b;
}

void divMod(int Dividend, int Divisor, int &Result, int &Remainder) {
    Result = (int)(((double)Dividend) / ((double)Divisor));
    Remainder = Dividend % Divisor;
}

// this convert miliseconds since midnight into hour/minute/second
void MiliMidnightToHMS(bim::uint32 ms, unsigned char &hour, unsigned char &minute, unsigned char &second) {
    hour = 0;
    minute = 0;
    second = 0;
    if (ms == 0) return;
    int h, m, s, fms, mc, msc;
    divMod(ms, SECS_PER_MIN * MSECS_PER_SEC, mc, msc);
    divMod(mc, MINS_PER_HOUR, h, m);
    divMod(msc, MSECS_PER_SEC, s, fms);
    hour = h;
    minute = m;
    second = s;
}

// String versions
std::string JulianToAnsi(bim::uint32 julian) {
    unsigned short y = 0;
    unsigned char m = 0, d = 0;
    JulianToYMD(julian, y, m, d);
    return xstring::xprintf("%.4d-%.2d-%.2d", y, m, d);
}

std::string MiliMidnightToAnsi(bim::uint32 ms) {
    unsigned char h = 0, mi = 0, s = 0;
    MiliMidnightToHMS(ms, h, mi, s);
    return xstring::xprintf("%.2d:%.2d:%.2d", h, mi, s);
}

std::string StkDateTimeToAnsi(bim::uint32 julian, bim::uint32 ms) {
    std::string s = JulianToAnsi(julian);
    s += " ";
    s += MiliMidnightToAnsi(ms);
    return s;
}

xstring stk_date_time_string(bim::uint32 date, bim::uint32 time) {
    unsigned short y;
    unsigned char m, d, h, mi, s;
    JulianToYMD(date, y, m, d);
    MiliMidnightToHMS(time, h, mi, s);
    return xstring::xprintf("%.4d-%.2d-%.2dT%.2d:%.2d:%.2d", y, m, d, h, mi, s);
}

//----------------------------------------------------------------------------
// STK UTIL FUNCTIONS
//----------------------------------------------------------------------------

bim::int32 stkGetNumPlanes(TIFF *tif) {
    double *d_list = NULL;
    bim::int32 *l_list = NULL;
    bim::int16 d_list_count[4];
    int res[4] = { 0, 0, 0, 0 };

    if (tif == 0) return 0;

    res[0] = TIFFGetField(tif, TIFFTAG_STK_UIC2, &d_list_count[0], &d_list);
    res[1] = TIFFGetField(tif, TIFFTAG_STK_UIC3, &d_list_count[1], &d_list);
    res[2] = TIFFGetField(tif, TIFFTAG_STK_UIC4, &d_list_count[2], &l_list);
    // Mario Emmenlauer, 2015.03.25:
    // The value of the UIC1 tag was not reliable for some images, ignored:
    //res[3] = TIFFGetField(tif, TIFFTAG_STK_UIC1, &d_list_count[3], &l_list);

    // if tag 33629 exists then the file is valid STK file
    if (res[0] == 1) return d_list_count[0];
    if (res[1] == 1) return d_list_count[1];
    if (res[2] == 1) return d_list_count[2];
    if (res[3] == 1) return d_list_count[3];

    return 1;
}

bool stkIsTiffValid(TIFF *tif) {
    if (tif->tif_flags & TIFF_BIGTIFF) return false;
    double *d_list = NULL;
    bim::int32 *l_list = NULL;
    bim::int16 d_list_count;
    int res[4] = { 0, 0, 0, 0 };

    if (tif == 0) return false;

    res[0] = TIFFGetField(tif, TIFFTAG_STK_UIC2, &d_list_count, &d_list);
    res[1] = TIFFGetField(tif, TIFFTAG_STK_UIC3, &d_list_count, &d_list);
    res[2] = TIFFGetField(tif, TIFFTAG_STK_UIC4, &d_list_count, &l_list);
    //res[3] = TIFFGetField(tif, TIFFTAG_STK_UIC1, &d_list_count, &d_list);

    // if tag 33629 exists then the file is valid STK file
    if (res[0] == 1) return true;
    if (res[1] == 1) return true;
    if (res[2] == 1) return true;
    //if (res[3] == 1) return true;

    return false;
}

bool stkIsTiffValid(TiffParams *pars) {
    if (!pars) return false;
    if (pars->tiff->tif_flags & TIFF_BIGTIFF) return false;

    // if tag 33629 exists then the file is valid STAK file
    //if (pars->ifds.tagPresentInFirstIFD(TIFFTAG_STK_UIC1)) return true;
    if (pars->ifds.tagPresentInFirstIFD(TIFFTAG_STK_UIC2)) return true;
    if (pars->ifds.tagPresentInFirstIFD(TIFFTAG_STK_UIC3)) return true;
    if (pars->ifds.tagPresentInFirstIFD(TIFFTAG_STK_UIC4)) return true;

    return false;
}

template<typename Tb, typename To>
void copy_tag_buffer(const std::vector<bim::uint8> &buffer, std::vector<To> &out) {
    bim::uint64 size_in_bytes = buffer.size();
    bim::uint64 n = size_in_bytes / sizeof(Tb);
    bim::uint64 out_size_in_bytes = n * sizeof(To);
    out.resize(n);

    if (size_in_bytes == out_size_in_bytes) { // same datatypes are used in both cases
        memcpy(&out[0], &buffer[0], size_in_bytes);
    } else { // different datatypes
        Tb *buf = (Tb *)&buffer[0];
        for (unsigned int i = 0; i < n; ++i)
            out[i] = buf[i];
    }
}

void stkGetOffsets(TIFF *tif, TiffParams *pars) {
    /*
    the default libtiff strip reading is not good enough since it only gives us strip offsets for
    the first plane although all the offsets for all the planes are actually stored
    so for uncompressed images use default and for LZW the custom methods
    */

    StkInfo *stk = &pars->stkInfo;
    stk->strips_per_plane = TIFFNumberOfStrips(tif);
    stk->strip_offsets.resize(stk->strips_per_plane, 0);
    stk->strip_bytecounts.resize(stk->strips_per_plane, 0);
    TinyTiff::IFD *ifd = pars->ifds.firstIfd();

    // read Strip Offsets
    bim::uint16 tag = 273;
    if (ifd->tagPresent(tag)) {
        // needs to convert into libtiff4 uint64 offsets from uint32 in the original file
        std::vector<bim::uint8> buffer;
        ifd->readTag(tag, buffer);
        copy_tag_buffer<bim::uint32, tiff_offs_t>(buffer, stk->strip_offsets);
    } // strip offsets

    // read strip_bytecounts also
    tag = 279;
    if (ifd->tagPresent(tag)) {
        // needs to convert into libtiff4 uint64 offsets from uint32 in the original file
        std::vector<bim::uint8> buffer;
        ifd->readTag(tag, buffer);
        copy_tag_buffer<bim::uint32, tiff_bcnt_t>(buffer, stk->strip_bytecounts);
    } // strip_bytecounts
}


//----------------------------------------------------------------------------
// PARSE UIC TAGS
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UIC2Tag - 33629
//----------------------------------------------------------------------------
//At the indicated offset, there is a table of 6*N LONG values
//indicating for each plane in order:
//Z distance (numerator), Z distance (denominator),
//creation date, creation time, modification date, modification time.
void stkParseUIC2Tag(TiffParams *pars) {
    if (!pars) return;
    if (!pars->tiff) return;
    if (pars->subType != tstStk) return;

    StkInfo *stk = &pars->stkInfo;
    TinyTiff::IFD *ifd = pars->ifds.firstIfd();
    if (!ifd) return;

    // read UIC2
    bim::uint16 tag = TIFFTAG_STK_UIC2;
    if (!ifd->tagPresent(tag)) return;

    std::vector<StkEntryUIC2> uic2(stk->N);
    ifd->readTagNoAlloc(tag, stk->N * sizeof(StkEntryUIC2), bim::DataType::TAG_LONG, (bim::uint8 *)&uic2[0]);

    std::vector<double> zDistance(uic2.size(), 0);
    std::vector<bim::xstring> creationDate;
    std::vector<bim::xstring> modificationDate;
    for (int i = 0; i < uic2.size(); ++i) {
        zDistance[i] = rational2float(uic2[i].zDistance);
        creationDate.push_back(stk_date_time_string(uic2[i].creationDate, uic2[i].creationTime));
        modificationDate.push_back(stk_date_time_string(uic2[i].modificationDate, uic2[i].modificationTime));
    }
    stk->tags.set_value("creationDate", creationDate);
    stk->tags.set_value("modificationDate", modificationDate);
    stk->tags.set_value("zDistance", zDistance);

    if (uic2.size() > 1) {
        unsigned char h1, m1, s1, h2, m2, s2;
        MiliMidnightToHMS(uic2[0].creationTime, h1, m1, s1);
        MiliMidnightToHMS(uic2[1].creationTime, h2, m2, s2);
        double delta_s = (fabs((float)h2 - h1) * 60.0 + fabs((float)m2 - m1)) * 60.0 + fabs((float)s2 - s1);
        stk->tags.set_value(bim::PIXEL_RESOLUTION_T, delta_s);
    }
}

//----------------------------------------------------------------------------
// UIC3Tag - 33630
//----------------------------------------------------------------------------
//A table of 2*N LONG values indicating for each plane in order:
//wavelength (numerator), wavelength (denominator).
void stkParseUIC3Tag(TiffParams *pars) {
    if (!pars) return;
    if (!pars->tiff) return;
    if (pars->subType != tstStk) return;

    StkInfo *stk = &pars->stkInfo;
    TinyTiff::IFD *ifd = pars->ifds.firstIfd();
    if (!ifd) return;

    // read UIC3
    bim::uint16 tag = TIFFTAG_STK_UIC3;
    if (!ifd->tagPresent(tag)) return;

    std::vector<StkRational> wavelength(stk->N);
    ifd->readTagNoAlloc(tag, stk->N * sizeof(bim::StkRational), bim::DataType::TAG_LONG, (bim::uint8 *)&wavelength[0]);
    appendRational(wavelength, "wavelengths", &stk->tags);
}

//----------------------------------------------------------------------------
// UIC4Tag - 33631 (TIFFTAG_STK_UIC4)
//----------------------------------------------------------------------------
/*
UIC4Tag
Tag	= 33631 (835F.H)
Type	= LONG
N	= Number of planes

At the indicated offset, there is a series of pairs consisting of an ID code
of size SHORT and a variable-sized (ID-dependent) block of data. Pairs should
be read until an ID of 0 is encountered. The possible tags and their values
are defined in the 'Tag ID Codes' section below. The 'AutoScale' tag never
appears in this table (because its ID is used to terminate the table.)
*/
void stkParseUIC4Tag(TiffParams *pars) {
    if (!pars) return;
    if (!pars->tiff) return;
    if (pars->subType != tstStk) return;

    StkInfo *stk = &pars->stkInfo;
    TinyTiff::IFD *ifd = pars->ifds.firstIfd();
    if (!ifd) return;

    // read UIC4
    bim::uint16 tag = TIFFTAG_STK_UIC4;
    if (!ifd->tagPresent(tag)) return;

    short tag_id;
    bim::int64 data_offset = ifd->tagOffset(tag);
    while (data_offset != 0) {
        // first read key for a tuple
        if (ifd->readBufNoAlloc(data_offset, 2, TIFF_SHORT, (bim::uint8 *)&tag_id) != 0) break;
        if (tag_id == 0) // || tag_id > BIM_STK_CameraChipOffset) //safeguard, currently i only know of keys <= 45, stop otherwise
            break;
        data_offset += 2;

        if (tag_id == BIM_STK_StagePosition) {
            std::vector<StkPosition> StagePosition(stk->N);
            ifd->readBufNoAlloc(data_offset, stk->N * sizeof(StkPosition), TIFF_LONG, (bim::uint8 *)&StagePosition[0]);
            appendPosition(StagePosition, "StagePosition", &stk->tags);
        } else if (tag_id == BIM_STK_CameraChipOffset) {
            std::vector<StkPosition> CameraChipOffset(stk->N);
            ifd->readBufNoAlloc(data_offset, stk->N * sizeof(StkPosition), TIFF_LONG, (bim::uint8 *)&CameraChipOffset[0]);
            appendPosition(CameraChipOffset, "CameraChipOffset", &stk->tags);
        }
    } // while
}

//----------------------------------------------------------------------------
// UIC1Tag - 33628
//----------------------------------------------------------------------------

//If this tag exists and is of type LONG, then at the indicated offset
//there is a series of N pairs consisting of an ID code of size LONG and a
//variable-sized (ID-dependent) block of data. The possible tags and their
//values are defined in the "Tag ID Codes" section below. These values are
//used internally by the Meta Imaging Series applications, and may not be
//useful to other applications. To replicate the behavior of MetaMorph,
//this table should be read and its values stored after the table
//indicated by UIC4Tag is read.
// NOTE: difference form tag UIC4 is that this one contains exactly N pairs
// and long organization of data... We're not reading tag UIC4 at all at the moment

void stkReadStringEntry(bim::int32 offset, TiffParams *pars, std::string &s) {
    if (offset <= 0) return;
    TinyTiff::IFD *ifd = pars->ifds.firstIfd();
    if (!ifd) return;

    bim::int32 size;
    ifd->readBufNoAlloc((toff_t)offset, (bim::uint64)sizeof(bim::int32), TAG_LONG, (uchar *)&size);
    s.resize(size + 1, 0);
    ifd->readBufNoAlloc((toff_t)offset + sizeof(bim::int32), (bim::uint64)size, TAG_LONG, (bim::uint8 *)&s[0]);
}

void stkParseIDEntry(bim::int32 *pair, bim::int32 offset, TiffParams *pars) {
    StkInfo *stk = &pars->stkInfo;
    bim::TagMap *tags = &stk->tags;
    TinyTiff::IFD *ifd = pars->ifds.firstIfd();
    if (!ifd) return;
    StkRational r;
    std::string s;
    StkDateTime dt;
    //bim::int32 i;

    if (pair[0] == BIM_STK_AutoScale) {
        tags->set_value("AutoScale", (int)pair[1]);
    } else if (pair[0] == BIM_STK_MinScale) {
        tags->set_value("MinScale", (int)pair[1]);
    } else if (pair[0] == BIM_STK_MaxScale) {
        tags->set_value("MaxScale", (int)pair[1]);
    } else if (pair[0] == BIM_STK_SpatialCalibration) {
        tags->set_value("SpatialCalibration", (int)pair[1]);
    } else if (pair[0] == BIM_STK_XCalibration) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)sizeof(StkRational), TAG_LONG, (uchar *)&r);
        tags->set_value("XCalibration", rational2float(r));
    } else if (pair[0] == BIM_STK_YCalibration) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)sizeof(StkRational), TAG_LONG, (uchar *)&r);
        tags->set_value("YCalibration", rational2float(r));
    } else if (pair[0] == BIM_STK_CalibrationUnits) {
        stkReadStringEntry(pair[1], pars, s);
        tags->set_value("CalibrationUnits", s);
    } else if (pair[0] == BIM_STK_Name) {
        stkReadStringEntry(pair[1], pars, s);
        tags->set_value("Name", s);
    } else if (pair[0] == BIM_STK_ThreshState) {
        tags->set_value("ThreshState", (int)pair[1]);
    } else if (pair[0] == BIM_STK_ThreshStateRed) {
        tags->set_value("ThreshStateRed", (unsigned int)pair[1]);
    } else if (pair[0] == BIM_STK_ThreshStateGreen) {
        tags->set_value("ThreshStateGreen", (unsigned int)pair[1]);
    } else if (pair[0] == BIM_STK_ThreshStateBlue) {
        tags->set_value("ThreshStateBlue", (unsigned int)pair[1]);
    } else if (pair[0] == BIM_STK_ThreshStateLo) {
        tags->set_value("ThreshStateLo", (unsigned int)pair[1]);
    } else if (pair[0] == BIM_STK_ThreshStateHi) {
        tags->set_value("ThreshStateHi", (unsigned int)pair[1]);
    } else if (pair[0] == BIM_STK_Zoom) {
        tags->set_value("Zoom", (int)pair[1]);
    } else if (pair[0] == BIM_STK_CreateTime) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)sizeof(StkDateTime), TAG_LONG, (uchar *)&dt);
        tags->set_value("CreateTime", stk_date_time_string(dt.date, dt.time));
    } else if (pair[0] == BIM_STK_LastSavedTime) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)sizeof(StkDateTime), TAG_LONG, (uchar *)&dt);
        tags->set_value("LastSavedTime", stk_date_time_string(dt.date, dt.time));
    } else if (pair[0] == BIM_STK_currentBuffer) {
        tags->set_value("currentBuffer", (int)pair[1]);
    } else if (pair[0] == BIM_STK_grayFit) {
        tags->set_value("grayFit", (int)pair[1]);
    } else if (pair[0] == BIM_STK_grayPointCount) {
        tags->set_value("grayPointCount", (int)pair[1]);
    } else if (pair[0] == BIM_STK_grayX) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)&r);
        tags->set_value("grayX", rational2float(r));
    } else if (pair[0] == BIM_STK_grayY) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)&r);
        tags->set_value("grayY", rational2float(r));
    } else if (pair[0] == BIM_STK_grayMin) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)&r);
        tags->set_value("grayMin", rational2float(r));
    } else if (pair[0] == BIM_STK_grayMax) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)&r);
        tags->set_value("grayMax", rational2float(r));
    } else if (pair[0] == BIM_STK_grayUnitName) {
        stkReadStringEntry(pair[1], pars, s);
        tags->set_value("grayUnitName", s);
    } else if (pair[0] == BIM_STK_StandardLUT) {
        tags->set_value("StandardLUT", (int)pair[1]);
    } else if (pair[0] == BIM_STK_wavelength) {
        tags->set_value("UIC1wavelength", (int)pair[1]);
    } else if (pair[0] == BIM_STK_AutoScaleLoInfo) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)&r);
        tags->set_value("AutoScaleLoInfo", rational2float(r));
    } else if (pair[0] == BIM_STK_AutoScaleHiInfo) {
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)&r);
        tags->set_value("AutoScaleHiInfo", rational2float(r));
    } else if (pair[0] == BIM_STK_Gamma) {
        //ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64) sizeof(bim::int32), TAG_LONG, (uchar *)&i);
        //tags->set_value("Gamma", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GammaRed) {
        //ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64) sizeof(bim::int32), TAG_LONG, (uchar *)&i);
        //tags->set_value("GammaRed", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GammaGreen) {
        //ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64) sizeof(bim::int32), TAG_LONG, (uchar *)&i);
        //tags->set_value("GammaGreen", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GammaBlue) {
        //ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64) sizeof(bim::int32), TAG_LONG, (uchar *)&i);
        //tags->set_value("GammaBlue", (int)pair[1]);
    } else if (pair[0] == BIM_STK_CameraBin) {
        //ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64) 2 * sizeof(bim::int32), TAG_LONG, (uchar *)&i);
        tags->set_value("CameraBin", (int)pair[1]);
    } else if (pair[0] == BIM_STK_NewLUT) {
        tags->set_value("NewLUT", (int)pair[1]);
    } else if (pair[0] == BIM_STK_ImagePropertyEx) {
        tags->set_value("ImagePropertyEx", (int)pair[1]);
    } else if (pair[0] == BIM_STK_UserLutTable) {
        tags->set_value("UserLutTable", (int)pair[1]);
    } else if (pair[0] == BIM_STK_RedAutoScaleInfo) {
        tags->set_value("RedAutoScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_RedAutoScaleLoInfo) {
        tags->set_value("RedAutoScaleLoInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_RedAutoScaleHiInfo) {
        tags->set_value("RedAutoScaleHiInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_RedMinScaleInfo) {
        tags->set_value("RedMinScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_RedMaxScaleInfo) {
        tags->set_value("RedMaxScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GreenAutoScaleInfo) {
        tags->set_value("GreenAutoScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GreenAutoScaleLoInfo) {
        tags->set_value("GreenAutoScaleLoInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GreenAutoScaleHiInfo) {
        tags->set_value("GreenAutoScaleHiInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GreenMinScaleInfo) {
        tags->set_value("GreenMinScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_GreenMaxScaleInfo) {
        tags->set_value("GreenMaxScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_BlueAutoScaleInfo) {
        tags->set_value("BlueAutoScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_BlueAutoScaleLoInfo) {
        tags->set_value("BlueAutoScaleLoInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_BlueAutoScaleHiInfo) {
        tags->set_value("BlueAutoScaleHiInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_BlueMinScaleInfo) {
        tags->set_value("BlueMinScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_BlueMaxScaleInfo) {
        tags->set_value("BlueMaxScaleInfo", (int)pair[1]);
    } else if (pair[0] == BIM_STK_OverlayPlaneColor) {
        tags->set_value("OverlayPlaneColor", (int)pair[1]);
    } else if (pair[0] == BIM_STK_StagePosition) {      // only the first page value can be red here
        std::vector<StkPosition> StagePosition(stk->N); // should be 1
        ifd->readBufNoAlloc(pair[1], stk->N * sizeof(StkPosition), TIFF_LONG, (bim::uint8 *)&StagePosition[0]);
        appendPosition(StagePosition, "StagePosition", tags);
    } else if (pair[0] == BIM_STK_CameraChipOffset) {
        std::vector<StkPosition> CameraChipOffset(stk->N); // should be 1
        ifd->readBufNoAlloc(pair[1], stk->N * sizeof(StkPosition), TIFF_LONG, (bim::uint8 *)&CameraChipOffset[0]);
        appendPosition(CameraChipOffset, "CameraChipOffset", tags);
    } else if (pair[0] == BIM_STK_AbsoluteZ) {
        std::vector<StkRational> AbsoluteZ(stk->N);
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)stk->N * sizeof(StkRational), TAG_LONG, (uchar *)&AbsoluteZ[0]);
        appendRational(AbsoluteZ, "AbsoluteZ", tags);
    } else if (pair[0] == BIM_STK_AbsoluteZValid) {
        std::vector<bim::int32> AbsoluteZValid(stk->N);
        ifd->readBufNoAlloc((toff_t)pair[1], (bim::uint64)stk->N * sizeof(bim::int32), TAG_LONG, (uchar *)&AbsoluteZValid[0]);
        //appendPosition(AbsoluteZValid, "AbsoluteZValid", tags);
    } else if (pair[0] == BIM_STK_PlaneProperty) {
        toff_t readpos = pair[1];
        bim::int8 prop_key_len = 0;
        readpos += 4;
        ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)sizeof(bim::int8), TAG_BYTE, (uchar *)&prop_key_len);
        readpos += 1;

        xstring prop_key(prop_key_len, 0);
        ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)prop_key_len, TAG_BYTE, (uchar *)&prop_key[0]);
        readpos += prop_key_len + 4;

        bim::int8 prop_key_type = 0;
        ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)sizeof(bim::int8), TAG_BYTE, (uchar *)&prop_key_type);
        readpos += 1;

        if (prop_key_type == 1) {
            ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)2 * sizeof(bim::int32), TAG_BYTE, (uchar *)&r);
            tags->set_value(prop_key, rational2float(r));
        } else if (prop_key_type == 2) {
            bim::int8 prop_val_len = 0;
            ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)sizeof(bim::int8), TAG_BYTE, (uchar *)&prop_val_len);

            if (prop_val_len == 0) {
                readpos += 4;
                ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)sizeof(bim::int8), TAG_BYTE, (uchar *)&prop_val_len);
            }

            xstring prop_val(prop_val_len, 0);
            readpos += 1;
            ifd->readBufNoAlloc((toff_t)readpos, (bim::uint64)prop_val_len, TAG_BYTE, (uchar *)&prop_val[0]);
            tags->set_value(prop_key, prop_val);
        } else {
            std::cerr << "stkParseIDEntry(): Could not parse property type '" << prop_key_type << "' for property '" << prop_key << "'." << std::endl;
        }
    }
}

void stkParseUIC1Tag(TiffParams *pars) {
    if (!pars) return;
    if (!pars->tiff) return;
    //if (pars->subType != tstStk) return;

    TinyTiff::IFD *ifd = pars->ifds.firstIfd();
    if (!ifd) return;

    bim::uint16 tag = TIFFTAG_STK_UIC1;
    if (!ifd->tagPresent(tag)) return;

    bim::uint64 offset = ifd->tagOffset(tag);
    if (offset == -1) return;
    bim::uint64 num_ids = ifd->tagCount(tag);

    // now read and parse ID table
    for (bim::uint64 i = 0; i < num_ids; i++) {
        bim::int32 pair[2];
        ifd->readBufNoAlloc((toff_t)offset, (bim::uint64)2 * sizeof(bim::int32), TAG_LONG, (uchar *)pair);
        stkParseIDEntry(pair, (bim::uint32)offset, pars);
        offset += 2 * sizeof(bim::int32);
    }
}

void stkParseUICTags(TiffParams *pars) {
    if (!pars) return;
    if (!pars->tiff) return;
    if (pars->subType != tstStk) return;

    stkParseUIC2Tag(pars);
    stkParseUIC3Tag(pars);
    stkParseUIC4Tag(pars);
    stkParseUIC1Tag(pars);
}


//----------------------------------------------------------------------------
// STK INFO
//----------------------------------------------------------------------------

int stkGetInfo(TiffParams *pars) {
    if (!pars) return 1;
    if (!pars->tiff) return 1;
    if (!pars->ifds.isValid()) return 1;

    pars->stkInfo.init(stkGetNumPlanes(pars->tiff));
    stkGetOffsets(pars->tiff, pars);
    stkParseUICTags(pars);

    //---------------------------------------------------------------
    // define dims
    //---------------------------------------------------------------
    StkInfo *stk = &pars->stkInfo;
    ImageInfo *info = &pars->info;

    info->number_pages = stk->N;
    info->number_z = 1;
    info->number_t = info->number_pages;

    std::vector<double> zDistance = stk->tags.get_value_vector_double("zDistance");
    if (zDistance.size() > 0 && zDistance[0] > 0) {
        info->number_z = info->number_pages;
        info->number_t = 1;
    }

    if (info->number_z > 1) {
        info->number_dims = 4;
        info->dimensions[3].dim = DIM_Z;
    }

    if (info->number_t > 1) {
        info->number_dims = 4;
        info->dimensions[3].dim = DIM_T;
    }

    if ((info->number_z > 1) && (info->number_t > 1)) {
        info->number_dims = 5;
        info->dimensions[3].dim = DIM_Z;
        info->dimensions[4].dim = DIM_T;
    }

    return 0;
}

//----------------------------------------------------------------------------
// READ/WRITE FUNCTIONS
//----------------------------------------------------------------------------

bim::uint stkReadPlane(TiffParams *pars, int plane, ImageBitmap *img, FormatHandle *fmtHndl) {
    if (!pars) return 1;
    if (!img) return 1;
    if (!pars->tiff) return 1;

    StkInfo *stk = &pars->stkInfo;
    TIFF *tif = pars->tiff;

    bim::uint16 photometric = PHOTOMETRIC_MINISWHITE;
    bim::uint16 compress_tag;
    bim::uint16 bitspersample = 8;
    bim::uint16 samplesperpixel = 1;
    bim::uint32 height = 0;
    bim::uint32 width = 0;
    toff_t old_pos = tif->tif_curoff;
    bim::uint32 rowsperstrip = 1;

    TIFFGetFieldDefaulted(tif, TIFFTAG_COMPRESSION, &compress_tag);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);

    ImageInfo *info = &img->i;
    bim::uint Bpp = (unsigned int)ceil(((double)info->depth) / 8.0);
    bim::int32 size = (bim::uint32)(info->width * info->height * Bpp) * samplesperpixel;

    tiff_offs_t file_plane_offset = stk->strip_offsets[0] + (size * plane);

    if (compress_tag != COMPRESSION_NONE)
        file_plane_offset = stk->strip_offsets[plane * stk->strips_per_plane];

    // position file position into begining of the plane
    if (tif->tif_seekproc((thandle_t)tif->tif_fd, file_plane_offset, SEEK_SET) != 0) {

        //---------------------------------------------------
        // load the buffer with decompressed plane data
        //---------------------------------------------------

        if (compress_tag == COMPRESSION_NONE && samplesperpixel == 1) {
            xprogress(fmtHndl, 0, 10, "Reading STK");

            tiff_size_t read_size = tif->tif_readproc((thandle_t)tif->tif_fd, img->bits[0], size);
            if (read_size != size) return 1;
            // now swap bytes if needed
            tif->tif_postdecode(tif, (tidata_t)img->bits[0], size);
        } else if (compress_tag == COMPRESSION_NONE && samplesperpixel > 1) {
            xprogress(fmtHndl, 0, 10, "Reading STK");
            std::vector<unsigned char> buffer(size);
            tiff_size_t read_size = tif->tif_readproc((thandle_t)tif->tif_fd, &buffer[0], size);
            if (read_size != size) return 1;
            // now swap bytes if needed
            tif->tif_postdecode(tif, (tidata_t)&buffer[0], size);

            // now copy over to speratae image channels
            for (int s = 0; s < samplesperpixel; ++s) {
                if (info->depth == 8) {
                    copy_sample_interleaved_to_planar<bim::uint8>(info->width, info->height, info->samples, s, &buffer[0], img->bits[s]);
                } else if (info->depth == 16) {
                    copy_sample_interleaved_to_planar<bim::uint16>(info->width, info->height, info->samples, s, &buffer[0], img->bits[s]);
                } else if (info->depth == 32) {
                    copy_sample_interleaved_to_planar<bim::uint32>(info->width, info->height, info->samples, s, &buffer[0], img->bits[s]);
                } else if (info->depth == 64) {
                    copy_sample_interleaved_to_planar<bim::uint64>(info->width, info->height, info->samples, s, &buffer[0], img->bits[s]);
                }
            } // for sample
        } else {
            int strip_size_plane = width * Bpp * rowsperstrip;
            int strip_size = strip_size_plane * samplesperpixel;
            std::vector<unsigned char> buffer(strip_size);
            unsigned char *buf = &buffer[0];

            // ---------------------------------------------------
            // let's tweak libtiff and change offsets and bytecounts for correct values for this plane
            // dima: in 4.1.0 may have to use TIFFGetStrileOffset and TIFFGetStrileByteCount, currently not using TIFF_LAZYSTRILELOAD
            TIFFDirectory *td = &tif->tif_dir;

#if (TIFFLIB_VERSION >= 20191103)
            tiff_offs_t *plane_strip_offsets = &stk->strip_offsets[0] + (plane * stk->strips_per_plane);
            _TIFFmemcpy(td->td_stripoffset_p, plane_strip_offsets, stk->strips_per_plane * sizeof(tiff_offs_t));

            tiff_bcnt_t *plane_strip_bytecounts = &stk->strip_bytecounts[0] + (plane * stk->strips_per_plane);
            _TIFFmemcpy(td->td_stripbytecount_p, plane_strip_bytecounts, stk->strips_per_plane * sizeof(tiff_bcnt_t));
#else
            tiff_offs_t *plane_strip_offsets = &stk->strip_offsets[0] + (plane * stk->strips_per_plane);
            _TIFFmemcpy(td->td_stripoffset, plane_strip_offsets, stk->strips_per_plane * sizeof(tiff_offs_t));

            tiff_bcnt_t *plane_strip_bytecounts = &stk->strip_bytecounts[0] + (plane * stk->strips_per_plane);
            _TIFFmemcpy(td->td_stripbytecount, plane_strip_bytecounts, stk->strips_per_plane * sizeof(tiff_bcnt_t));
#endif
            // ---------------------------------------------------

            int strip_num = plane * stk->strips_per_plane;
            tiff_offs_t strip_offset = 0;
            while (strip_offset < stk->strips_per_plane) {
                xprogress(fmtHndl, strip_offset, stk->strips_per_plane, "Reading STK");
                if (xtestAbort(fmtHndl) == 1) break;

                tiff_size_t read_size = TIFFReadEncodedStrip(tif, strip_offset, (tiff_data_t)buf, (tiff_size_t)strip_size);
                if (read_size == -1) return 1;
                if (samplesperpixel == 1) {
                    memcpy(((unsigned char *)img->bits[0]) + strip_offset * strip_size, buf, read_size);
                } else {
                    // now copy over to speratae image channels
                    for (int s = 0; s < samplesperpixel; ++s) {
                        if (info->depth == 8) {
                            copy_sample_interleaved_to_planar<bim::uint8>(width, rowsperstrip, info->samples, s, &buffer[0], (unsigned char *)img->bits[s] + strip_offset * strip_size_plane);
                        } else if (info->depth == 16) {
                            copy_sample_interleaved_to_planar<bim::uint16>(width, rowsperstrip, info->samples, s, &buffer[0], (unsigned char *)img->bits[s] + strip_offset * strip_size_plane);
                        } else if (info->depth == 32) {
                            copy_sample_interleaved_to_planar<bim::uint32>(width, rowsperstrip, info->samples, s, &buffer[0], (unsigned char *)img->bits[s] + strip_offset * strip_size_plane);
                        } else if (info->depth == 64) {
                            copy_sample_interleaved_to_planar<bim::uint64>(width, rowsperstrip, info->samples, s, &buffer[0], (unsigned char *)img->bits[s] + strip_offset * strip_size_plane);
                        }
                    } // for sample
                }
                ++strip_offset;
            }
        }

        // invert it if we got negative
        TIFFGetField(pars->tiff, TIFFTAG_PHOTOMETRIC, &photometric);
        if (photometric == PHOTOMETRIC_MINISWHITE) {
            int sample = 0;
            invertSample(img, sample);
        }
    }

    tif->tif_seekproc((thandle_t)tif->tif_fd, old_pos, SEEK_SET);

    return 0;
}


//----------------------------------------------------------------------------
// METADATA
//----------------------------------------------------------------------------

std::string stk_readline(const std::string &str, int &pos) {
    std::string line;
    std::string::const_iterator it = str.begin() + pos;
    while (it < str.end() && *it != 0xA) {
        if (*it != 0xD)
            line += *it;
        else
            ++pos;
        ++it;
    }
    pos += (int)line.size();
    if (it < str.end() && *it == 0xA) ++pos;
    return line;
}

bim::uint append_metadata_stk(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    TiffParams *tiffpar = (TiffParams *)fmtHndl->internalParams;
    StkInfo *stk = &tiffpar->stkInfo;
    ImageInfo *info = &tiffpar->info;
    TinyTiff::IFD *ifd = tiffpar->ifds.firstIfd();
    if (!ifd) return 1;

    // is this STK?
    if (!ifd->tagPresent(33628)) return 0;

    hash->set_value(bim::IMAGE_NUM_Z, (int)info->number_z);
    hash->set_value(bim::IMAGE_NUM_T, (int)info->number_t);

    hash->append_tags(stk->tags, "MetaMorph/");

    //---------------------------------------------------------------------------
    // UIC2
    //---------------------------------------------------------------------------
    hash->set_value_from_old_key("MetaMorph/creationDate", bim::COORDINATES_POSITIONS_DATETIME);

    hash->set_value_from_old_key("MetaMorph/XCalibration", bim::PIXEL_RESOLUTION_X);
    hash->set_value_from_old_key("MetaMorph/YCalibration", bim::PIXEL_RESOLUTION_Y);
    hash->set_value_from_old_key("MetaMorph/Z Step", bim::PIXEL_RESOLUTION_Z);
    hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_X);
    hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_Y);
    hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_Z);

    //hash->set_value_from_old_key("MetaMorph/ApplicationName", bim::DOCUMENT_APPLICATION);
    //hash->set_value_from_old_key("MetaMorph/ApplicationVersion", bim::DOCUMENT_APPLICATION_VERSION);
    //hash->set_value_from_old_key("MetaMorph/MetaDataVersion", bim::DOCUMENT_VERSION);
    hash->set_value_from_old_key("MetaMorph/CreateTime", bim::DOCUMENT_DATETIME);

    // objective
    hash->set_value_from_old_key("MetaMorph/ImageXpress Micro Objective", bim::OBJECTIVE_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/_MagSetting_", bim::OBJECTIVE_DESCRIPTION);
    hash->set_value_from_old_key("MetaMorph/_MagNA_", bim::OBJECTIVE_NUM_APERTURE);
    hash->set_value_from_old_key("MetaMorph/_MagRI_", bim::OBJECTIVE_REF_INDEX);
    bim::parse_objective_from_string(hash->get_value(bim::OBJECTIVE_DESCRIPTION), hash);

    // channel info
    bim::xstring channel_path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
    hash->set_value_from_old_key("MetaMorph/IXConfocal Module Dichroic Wheel", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key("MetaMorph/IXConfocal Module Emission Wheel", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key("MetaMorph/_IllumSetting_", channel_path + bim::CHANNEL_INFO_NAME);
    hash->set_value_from_old_key(channel_path + bim::CHANNEL_INFO_NAME, channel_path + bim::CHANNEL_INFO_FLUOR);
    hash->set_value_from_old_key(channel_path + bim::CHANNEL_INFO_NAME, channel_path + bim::CHANNEL_INFO_DYE);
    hash->set_value_from_old_key("MetaMorph/UIC1wavelength", channel_path + bim::CHANNEL_INFO_EM_WAVELENGTH);
    hash->set_value_from_old_key("MetaMorph/Gamma", channel_path + bim::CHANNEL_INFO_GAMMA);
    bim::parse_exposure_from_string(hash->get_value("MetaMorph/Description/Exposure"), channel_path, hash);
    bim::parse_pinhole_from_string(hash->get_value("MetaMorph/IXConfocal Module Disk"), channel_path, hash);


    //---------------------------------------------------------------------------
    // get the pixel size in microns
    //---------------------------------------------------------------------------

    double xr = hash->get_value_double("MetaMorph/XCalibration", 0);
    if (xr > 0) {
        hash->set_value_from_old_key("MetaMorph/XCalibration", bim::PIXEL_RESOLUTION_X);
        hash->set_value_from_old_key("MetaMorph/YCalibration", bim::PIXEL_RESOLUTION_Y);
    }

    bim::xstring units = hash->get_value("MetaMorph/CalibrationUnits");
    if (units.size() > 1) {
        hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_X);
        hash->set_value_from_old_key("MetaMorph/CalibrationUnits", bim::PIXEL_RESOLUTION_UNIT_Y);
    }

    //---------------------------------------------------------------------------
    // add 3'd dimension
    //---------------------------------------------------------------------------

    std::vector<double> zDistance = stk->tags.get_value_vector_double("zDistance");
    if (info->number_z > 1 && zDistance.size() > 0) {
        hash->set_value(bim::PIXEL_RESOLUTION_Z, zDistance[0]);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    if (info->number_t > 1) {
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_T, bim::PIXEL_RESOLUTION_UNIT_SECONDS);
        hash->set_value_from_old_key("MetaMorph/" + bim::PIXEL_RESOLUTION_T, bim::PIXEL_RESOLUTION_T);
    }

    //------------------------------------------------------------
    // Long arrays
    //------------------------------------------------------------
    hash->set_value_from_old_key("MetaMorph/StagePosition", bim::COORDINATES_POSITIONS_STAGE);
    hash->set_value_from_old_key("MetaMorph/CameraChipOffset", bim::COORDINATES_POSITIONS_SENSOR);

    if (info->number_z > 1) {
        hash->set_value_from_old_key("MetaMorph/AbsoluteZ", bim::COORDINATES_POSITIONS_Z);
    }

    //------------------------------------------------------------
    // Add tags from tiff
    //------------------------------------------------------------
    TIFF *tif = tiffpar->tiff;
    xstring tag_305 = ifd->readTagString(TIFFTAG_SOFTWARE);

    hash->set_value("MetaMorph/Software", tag_305);
    //xstring tag_306 = read_tag_as_string(tif, ifd, TIFFTAG_DATETIME );

    xstring tag_270 = ifd->readTagString(TIFFTAG_IMAGEDESCRIPTION);
    if (!tag_270.contains("<OME")) { // stop from parsing OME XML
        hash->parse_ini(tag_270, ":", "MetaMorph/");

        int p = 0;
        xstring line = stk_readline(tag_270, p);
        if (line.startsWith("Acquired from ")) {
            hash->set_value("MetaMorph/AcquiredFrom", line.right("Acquired from "));
        }
    }

    //------------------------------------------------------------
    // parse known
    //------------------------------------------------------------

    // channel info
    channel_path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), 0);
    bim::parse_exposure_from_string(hash->get_value("MetaMorph/Exposure"), channel_path, hash);
    hash->set_value_from_old_key("MetaMorph/Wavelength", channel_path + bim::CHANNEL_INFO_EX_WAVELENGTH);
    hash->set_value_from_old_key("MetaMorph/Gamma", channel_path + bim::CHANNEL_INFO_GAMMA);
    hash->set_value_from_old_key("MetaMorph/Gain", channel_path + bim::CHANNEL_INFO_GAIN);

    // DECLARE_STR(CHANNEL_INFO_COLOR_RANGE, "range")
    //MetaMorph/MaxScale: 1500
    //MetaMorph/MinScale: 576

    return 0;
}
