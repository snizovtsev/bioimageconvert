/*****************************************************************************
 Tag names for metadata

 Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>
 Copyright (c) 2010 Vision Research Lab, UCSB <http://vision.ece.ucsb.edu>

 History:
   2010-07-29 17:18:22 - First creation

 Ver : 1
*****************************************************************************/

#ifndef BIM_IMG_META_TAGS
#define BIM_IMG_META_TAGS

#include <string>

#include <bim_img_format_interface.h>
#include <xcolor.h>
#include <xstring.h>

// define the declaration macro in order to get external const vars in the namespace
#ifndef DECLARE_STR
#define DECLARE_STR(S, V) extern const std::string S;
#endif

namespace bim {

#include "bim_metatags.def.h" // include actual string data

namespace meta {

static const std::vector<std::string> pixel_format_strings = {
    "undefined",
    "unsigned integer",
    "signed integer",
    "floating point",
    "complex"
};

// to be deprecated as of v3 in favor of bim::Color::default()
static const std::vector<bim::Color<float>> channel_colors_default = {
    bim::Color<float>(1, 0, 0),
    bim::Color<float>(0, 1, 0),
    bim::Color<float>(0, 0, 1),
    bim::Color<float>(1, 1, 1),
    bim::Color<float>(1, 0, 1),
    bim::Color<float>(0, 1, 1),
    bim::Color<float>(1, 1, 0),
    bim::Color<float>(1, 1, 0)
};

static const std::vector<std::string> image_mode_strings = {
    "monochrome",
    "grayscale",
    "indexed",
    "RGB",
    "",
    "HLS",
    "HSV",
    "RGBA",
    "",
    "CMYK",
    "",
    "",
    "multichannel",
    "RGBE",
    "YUV",
    "XYZ",
    "Lab",
    "CMY",
    "Luv",
    "YCbCr",
    "spectral",
    "mask",
    "heatmap"
};


static const std::map<std::string, int> dimension_names = {
    std::make_pair("x", 0),
    std::make_pair("y", 1),
    std::make_pair("c", 2),
    std::make_pair("z", 3),
    std::make_pair("t", 4),
    std::make_pair("serie", 5),
    std::make_pair("fov", 6),
    std::make_pair("rotation", 7),
    std::make_pair("scene", 8),
    std::make_pair("illumination", 9),
    std::make_pair("phase", 10),
    std::make_pair("view", 11),
    std::make_pair("item", 12),
    std::make_pair("spectra", 13),
    std::make_pair("unknown", 14),
};

} // namespace meta

} // namespace bim

#endif // BIM_IMG_META_TAGS
