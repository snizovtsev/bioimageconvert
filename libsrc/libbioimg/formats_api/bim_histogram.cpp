/*******************************************************************************

  histogram and lutf

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>
  Copyright (c) 2018, ViQi Inc

  History:
    10/20/2006 20:21 - First creation
    2007-06-26 12:18 - added lut class
    2010-01-22 17:06 - changed interface to support floating point data

  ver: 3

*******************************************************************************/

#include "bim_histogram.h"
#include "xstring.h"
#include "xtypes.h"

#include <algorithm>
#include <cmath>
#include <cstring>
#include <limits>

using namespace bim;

//******************************************************************************
// Histogram
//******************************************************************************

Histogram::Histogram(const unsigned int &bpp, const DataFormat &fmt) {
    default_size = Histogram::defaultSize;
    init(bpp, fmt);
}

Histogram::Histogram(const unsigned int &bpp, void *data, const unsigned int &num_data_points, const DataFormat &fmt, unsigned char *mask) {
    default_size = Histogram::defaultSize;
    newData(bpp, data, num_data_points, fmt, mask);
}

Histogram::~Histogram() {
}

void Histogram::init(const unsigned int &bpp, const DataFormat &fmt) {
    d.data_bpp = bpp;
    d.data_fmt = fmt;

    d.shift = 0;
    d.scale = 1;
    d.value_min = 0;
    d.value_max = 0;
    reversed_min_max = false;
    hist.resize(0);

    if (bpp == 0)
        return;
    else if (bpp <= 16 && (d.data_fmt == FMT_UNSIGNED || d.data_fmt == FMT_SIGNED)) {
        hist.resize((unsigned int)pow(2.0, fabs((double)bpp)), 0);
    } else if (bpp > 16 || d.data_fmt == FMT_FLOAT) {
        hist.resize(default_size);
    }
    initStats();
}

void Histogram::clear() {
    hist = std::vector<Histogram::StorageType>(hist.size(), 0);
}

void Histogram::newData(const unsigned int &bpp, void *data, const unsigned int &num_data_points, const DataFormat &fmt, unsigned char *mask) {
    init(bpp, fmt);
    clear();
    getStats(data, num_data_points, mask);
    addData(data, num_data_points, mask);
}

template<typename T>
void Histogram::init_stats() {
    d.value_min = bim::lowest<T>();
    d.value_max = bim::highest<T>();
    reversed_min_max = false;
    recompute_shift_scale();
}

void Histogram::initStats() {
    if (d.data_bpp == 8 && d.data_fmt == FMT_UNSIGNED)
        init_stats<uint8>();
    else if (d.data_bpp == 16 && d.data_fmt == FMT_UNSIGNED)
        init_stats<uint16>();
    else if (d.data_bpp == 32 && d.data_fmt == FMT_UNSIGNED)
        init_stats<uint32>();
    else if (d.data_bpp == 8 && d.data_fmt == FMT_SIGNED)
        init_stats<int8>();
    else if (d.data_bpp == 16 && d.data_fmt == FMT_SIGNED)
        init_stats<int16>();
    else if (d.data_bpp == 32 && d.data_fmt == FMT_SIGNED)
        init_stats<int32>();
    else if (d.data_bpp == 32 && d.data_fmt == FMT_FLOAT)
        init_stats<float32>();
    else if (d.data_bpp == 64 && d.data_fmt == FMT_FLOAT)
        init_stats<float64>();
    else if (d.data_bpp == 80 && d.data_fmt == FMT_FLOAT)
        init_stats<float80>();
}

template<typename T>
void Histogram::update_data_stats(T *data, const unsigned int &num_data_points, unsigned char *mask) {
    if (!data) return;
    if (num_data_points == 0) return;
    if (!reversed_min_max) {
        d.value_min = bim::highest<T>();
        d.value_max = bim::lowest<T>();
        reversed_min_max = true;
    }

    T *p = (T *)data;
    if (mask == 0) {
        for (unsigned int i = 0; i < num_data_points; ++i) {
            if (*p < d.value_min) d.value_min = *p;
            if (*p > d.value_max) d.value_max = *p;
            ++p;
        }
    } else {
        for (unsigned int i = 0; i < num_data_points; ++i) {
            if (mask[i] > 128) {
                if (*p < d.value_min) d.value_min = *p;
                if (*p > d.value_max) d.value_max = *p;
            }
            ++p;
        }
    }

    recompute_shift_scale();
}

template<typename T>
void Histogram::get_data_stats(T *data, const unsigned int &num_data_points, unsigned char *mask) {
    init_stats<T>();
    d.value_min = bim::highest<T>();
    d.value_max = bim::lowest<T>();
    reversed_min_max = true;
    update_data_stats(data, num_data_points, mask);
}

void Histogram::getStats(void *data, const unsigned int &num_data_points, unsigned char *mask) {
    if (d.data_fmt == FMT_UNSIGNED) {
        if (d.data_bpp == 32) get_data_stats<uint32>((uint32 *)data, num_data_points, mask);
    } else if (d.data_fmt == FMT_SIGNED) {
        if (d.data_bpp == 32) get_data_stats<int32>((int32 *)data, num_data_points, mask);
    } else if (d.data_fmt == FMT_FLOAT) {
        if (d.data_bpp == 32)
            get_data_stats<float32>((float32 *)data, num_data_points, mask);
        else if (d.data_bpp == 64)
            get_data_stats<float64>((float64 *)data, num_data_points, mask);
        else if (d.data_bpp == 80)
            get_data_stats<float80>((float80 *)data, num_data_points, mask);
    }
}

template<typename T>
void Histogram::add_from_data(T *data, const unsigned int &num_data_points, unsigned char *mask) {
    if (!data) return;
    if (num_data_points == 0) return;

    T *p = (T *)data;
    if (mask == 0) {
        for (unsigned int i = 0; i < num_data_points; ++i) {
            ++hist[*p];
            ++p;
        }
    } else {
        for (unsigned int i = 0; i < num_data_points; ++i) {
            if (mask[i] > 128) ++hist[*p];
            ++p;
        }
    }
}

template<typename T>
void Histogram::add_from_data_scale(T *data, const unsigned int &num_data_points, unsigned char *mask) {
    if (!data) return;
    if (num_data_points == 0) return;
    //get_data_stats( data, num_data_points );

    T *p = (T *)data;
    unsigned int bn = bin_number();
    if (mask == 0) {
        for (unsigned int i = 0; i < num_data_points; ++i) {
            double v = (((double)*p) - d.shift) * d.scale;
            unsigned int bin = bim::trim<unsigned int, double>(v, 0, bn - 1);
            ++hist[bin];
            ++p;
        }
    } else {
        for (unsigned int i = 0; i < num_data_points; ++i) {
            if (mask[i] > 128) {
                double v = (((double)*p) - d.shift) * d.scale;
                unsigned int bin = bim::trim<unsigned int, double>(v, 0, bn - 1);
                ++hist[bin];
            }
            ++p;
        }
    }
}

void Histogram::updateStats(void *data, const unsigned int &num_data_points, unsigned char *mask) {
    if (d.data_fmt == FMT_UNSIGNED) {
        if (d.data_bpp == 32) update_data_stats<uint32>((uint32 *)data, num_data_points, mask);
    } else if (d.data_fmt == FMT_SIGNED) {
        if (d.data_bpp == 32) update_data_stats<int32>((int32 *)data, num_data_points, mask);
    } else if (d.data_fmt == FMT_FLOAT) {
        if (d.data_bpp == 32)
            update_data_stats<float32>((float32 *)data, num_data_points, mask);
        else if (d.data_bpp == 64)
            update_data_stats<float64>((float64 *)data, num_data_points, mask);
        else if (d.data_bpp == 80)
            update_data_stats<float80>((float80 *)data, num_data_points, mask);
    }
}

void Histogram::addData(void *data, const unsigned int &num_data_points, unsigned char *mask) {

    if (d.data_fmt == FMT_UNSIGNED) {
        if (d.data_bpp == 8)
            add_from_data<uint8>((uint8 *)data, num_data_points, mask);
        else if (d.data_bpp == 16)
            add_from_data<uint16>((uint16 *)data, num_data_points, mask);
        else if (d.data_bpp == 32)
            add_from_data_scale<uint32>((uint32 *)data, num_data_points, mask);
    } else if (d.data_fmt == FMT_SIGNED) {
        if (d.data_bpp == 8)
            add_from_data_scale<int8>((int8 *)data, num_data_points, mask);
        else if (d.data_bpp == 16)
            add_from_data_scale<int16>((int16 *)data, num_data_points, mask);
        else if (d.data_bpp == 32)
            add_from_data_scale<int32>((int32 *)data, num_data_points, mask);
    } else if (d.data_fmt == FMT_FLOAT) {
        if (d.data_bpp == 32)
            add_from_data_scale<float32>((float32 *)data, num_data_points, mask);
        else if (d.data_bpp == 64)
            add_from_data_scale<float64>((float64 *)data, num_data_points, mask);
        else if (d.data_bpp == 80)
            add_from_data_scale<float80>((float80 *)data, num_data_points, mask);
    }
}

int Histogram::bin_of_last_nonzero() const {
    for (int i = (int)hist.size() - 1; i >= 0; --i)
        if (hist[i] != 0)
            return i;
    return 0;
}

int Histogram::bin_of_first_nonzero() const {
    for (unsigned int i = 0; i < hist.size(); ++i)
        if (hist[i] != 0)
            return i;
    return 0;
}

int Histogram::bin_of_max_value() const {
    int bin = 0;
    Histogram::StorageType val = hist[0];
    for (unsigned int i = 0; i < hist.size(); ++i)
        if (hist[i] > val) {
            val = hist[i];
            bin = i;
        }
    return bin;
}

int Histogram::bin_of_min_value() const {
    int bin = 0;
    Histogram::StorageType val = hist[0];
    for (unsigned int i = 0; i < hist.size(); ++i)
        if (hist[i] < val) {
            val = hist[i];
            bin = i;
        }
    return bin;
}

double Histogram::max_value() const {
    if (d.data_fmt == FMT_UNSIGNED && d.data_bpp <= 16)
        return bin_of_last_nonzero();
    else
        return d.value_max;
}

double Histogram::min_value() const {
    if (d.data_fmt == FMT_UNSIGNED && d.data_bpp <= 16)
        return bin_of_first_nonzero();
    else
        return d.value_min;
}

int Histogram::bin_number_nonzero() const {
    int unique = 0;
    for (unsigned int i = 0; i < hist.size(); ++i)
        if (hist[i] != 0) ++unique;
    return unique;
}

double Histogram::average() const {
    double a = 0.0, s = 0.0;
    for (unsigned int i = 0; i < hist.size(); ++i)
        if (hist[i] > 0) {
            a += i * hist[i];
            s += hist[i];
        }
    double mu = a / s;
    return (mu / d.scale) + d.shift;
}

double Histogram::median() const {
    double medfreq = cumsum(hist.size() - 1) / 2.0;
    unsigned int sum = 0;
    for (unsigned int i = 0; i < hist.size(); ++i) {
        sum += hist[i];
        if (sum >= medfreq) return (i / d.scale) + d.shift;
    }
    return -1;
}

double Histogram::std() const {
    double mu = average();
    double a = 0.0, s = 0.0;
    for (unsigned int i = 0; i < hist.size(); ++i)
        if (hist[i] > 0) {
            s += hist[i];
            a += (((double)i) - mu) * (((double)i) - mu) * hist[i];
        }
    double sig_sq = sqrt(a / (s - 1));
    return (sig_sq / d.scale) + d.shift;
}

Histogram::StorageType Histogram::cumsum(const unsigned int &bin) const {
    unsigned int b = bin;
    if (b >= hist.size()) b = (unsigned int)hist.size() - 1;
    Histogram::StorageType sum = 0;
    for (unsigned int i = 0; i <= b; ++i)
        sum += hist[i];
    return sum;
}

double Histogram::range() const {
    return max_value() - min_value();
}

Histogram::StorageType Histogram::get_value(const unsigned int &bin) const {
    if (bin < hist.size())
        return hist[bin];
    else
        return 0;
}

void Histogram::set_value(const unsigned int &bin, const Histogram::StorageType &val) {
    if (bin < hist.size())
        hist[bin] = val;
}

void Histogram::append_value(const unsigned int &bin, const Histogram::StorageType &val) {
    if (bin < hist.size())
        hist[bin] += val;
}

//------------------------------------------------------------------------------
// I/O
//------------------------------------------------------------------------------

/*
Histogram binary content:
0x00 'BIM1' - 4 bytes header
0x04 'HST1' - 4 bytes spec
0x07        - XX bytes HistogramInternal
0xXX NUM    - 1xUINT32 number of elements in histogram vector
0xXX        - histogram vector Histogram::StorageType * NUM
*/

const char Histogram_mgk[4] = { 'B', 'I', 'M', '1' };
const char Histogram_spc[4] = { 'H', 'S', 'T', '1' };

bool Histogram::to(const std::string &fileName) {
    std::ofstream f(fileName.c_str(), std::ios_base::binary);
    return this->to(&f);
}

bool Histogram::to(std::ostream *s) {
    // write header
    s->write(Histogram_mgk, sizeof(Histogram_mgk));
    s->write(Histogram_spc, sizeof(Histogram_spc));
    s->write((const char *)&d, sizeof(bim::HistogramInternal));

    // write data
    uint32 sz = this->hist.size();
    s->write((const char *)&sz, sizeof(uint32));
    s->write((const char *)&this->hist[0], sizeof(Histogram::StorageType) * this->hist.size());
    s->flush();
    return true;
}

bool Histogram::from(const std::string &fileName) {
    std::ifstream f(fileName.c_str(), std::ios_base::binary);
    return this->from(&f);
}

bool Histogram::from(std::istream *s) {
    // read header
    char hts_hdr[sizeof(Histogram_mgk)];
    char hts_spc[sizeof(Histogram_spc)];

    s->read(hts_hdr, sizeof(Histogram_mgk));
    if (memcmp(hts_hdr, Histogram_mgk, sizeof(Histogram_mgk)) != 0) return false;

    s->read(hts_spc, sizeof(Histogram_spc));
    if (memcmp(hts_spc, Histogram_spc, sizeof(Histogram_spc)) != 0) return false;

    s->read((char *)&d, sizeof(bim::HistogramInternal));

    // read data
    uint32 sz;
    s->read((char *)&sz, sizeof(uint32));
    this->hist.resize(sz);

    s->read((char *)&this->hist[0], sizeof(Histogram::StorageType) * this->hist.size());
    return true;
}

inline void write_string(std::ostream *s, const std::string &str) {
    s->write(str.c_str(), str.size());
}

inline std::string stringPixelType(const bim::DataFormat &pixelType) {
    if (pixelType == bim::FMT_SIGNED) return "signed";
    if (pixelType == bim::FMT_FLOAT) return "float";
    return "unsigned";
}

bool Histogram::toXML(const std::string &fileName) {
    std::ofstream f(fileName.c_str(), std::ios_base::binary);
    write_string(&f, "<histogram name=\"channel\" value=\"0\">");
    this->toXML(&f);
    write_string(&f, "</histogram>");
    return true;
}

bool Histogram::toXML(std::ostream *s) {
    // write header
    write_string(s, xstring::xprintf("<tag name=\"data_bits_per_pixel\" value=\"%d\" />", this->d.data_bpp));
    write_string(s, xstring::xprintf("<tag name=\"data_format\" value=\"%s\" />", stringPixelType((bim::DataFormat)this->d.data_fmt).c_str()));
    write_string(s, xstring::xprintf("<tag name=\"data_value_min\" value=\"%f\" />", this->min_value()));
    write_string(s, xstring::xprintf("<tag name=\"data_value_max\" value=\"%f\" />", this->max_value()));
    write_string(s, xstring::xprintf("<tag name=\"data_scale\" value=\"%f\" />", this->d.scale));
    write_string(s, xstring::xprintf("<tag name=\"data_shift\" value=\"%f\" />", this->d.shift));

    // write data
    write_string(s, xstring::xprintf("<value>%d", this->hist[0]));
    for (uint32 i = 1; i < this->hist.size(); ++i) {
        write_string(s, xstring::xprintf(",%d", this->hist[i]));
    }
    write_string(s, "</value>");

    s->flush();
    return true;
}

//******************************************************************************
// Lut
//******************************************************************************

Lut::Lut() {
    lut.resize(0);
    this->generator = linear_full_range_generator;
    type = ltLinearFullRange;
}

Lut::Lut(const Histogram &in, const Histogram &out, LutParameters *args) {
    init(in, out, args);
}

Lut::Lut(const Histogram &in, const Histogram &out, const LutType &type, LutParameters *args) {
    init(in, out, type, args);
}

Lut::Lut(const Histogram &in, const Histogram &out, LutGenerator custom_generator, LutParameters *args) {
    init(in, out, custom_generator, args);
}

Lut::~Lut() {
}

void Lut::init(const Histogram &in, const Histogram &out, LutParameters *args) {
    lut.resize(in.size());
    clear();
    h_in = in;
    h_out = out;

    if (args && args->in_min != args->in_max) {
        d.in_min = args->in_min;
        d.in_max = args->in_max;
        d.shift = d.in_min;
        d.scale = ((double)h_in.bin_number() - 1) / (d.in_max - d.in_min);
    }
    if (args) {
        d.gamma = args->gamma;
    }

    if (this->d.out_min == this->d.out_max) {
        this->d.out_min = 0;
        this->d.out_max = out.size() - 1;
    }

    if (generator) generator(in, lut, &this->d);
}

void Lut::init(const Histogram &in, const Histogram &out, const LutType &type, LutParameters *args) {
    this->type = type;
    if (type == ltLinearFullRange)
        generator = linear_full_range_generator;
    else if (type == ltLinearDataRange)
        generator = linear_data_range_generator;
    else if (type == ltLinearDataTolerance)
        generator = linear_data_tolerance_generator;
    else if (type == ltEqualize)
        generator = equalized_generator;
    else if (type == ltTypecast)
        generator = typecast_generator;
    else if (type == ltFloat01)
        generator = linear_float01_generator;
    else if (type == ltMinMaxGamma)
        generator = min_max_gamma_generator;
    init(in, out, args);
}

void Lut::init(const Histogram &in, const Histogram &out, LutGenerator custom_generator, LutParameters *args) {
    this->type = ltCustom;
    generator = custom_generator;
    init(in, out, args);
}

void Lut::clear() {
    lut = std::vector<Lut::StorageType>(lut.size(), 0);
}

template<typename Tl>
void Lut::set_lut(const std::vector<Tl> &new_lut) {
    lut.assign(new_lut.begin(), new_lut.end());
}
template void Lut::set_lut<unsigned char>(const std::vector<unsigned char> &new_lut);
template void Lut::set_lut<unsigned int>(const std::vector<unsigned int> &new_lut);
template void Lut::set_lut<double>(const std::vector<double> &new_lut);

Lut::StorageType Lut::get_value(const unsigned int &pos) const {
    if (pos < lut.size())
        return lut[pos];
    else
        return 0;
}

void Lut::set_value(const unsigned int &pos, const Lut::StorageType &val) {
    if (pos < lut.size())
        lut[pos] = val;
}

template<typename Ti, typename To>
void Lut::apply_lut(const Ti *ibuf, To *obuf, const unsigned int &num_data_points) const {
    if (!ibuf || !obuf) return;
    if (this->type == ltTypecast) {
        this->apply_typecast(ibuf, obuf, num_data_points);
        return;
    }

#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_data_points > BIM_OMP_FOR1)
    for (bim::int64 i = 0; i < num_data_points; ++i)
        obuf[i] = bim::trim<To, bim::Lut::StorageType>(lut[ibuf[i]]);
}

template<typename Ti, typename To>
void Lut::apply_lut_scale_from(const Ti *ibuf, To *obuf, const unsigned int &num_data_points) const {
    if (!ibuf || !obuf) return;
    if (this->type == ltTypecast) {
        this->apply_typecast(ibuf, obuf, num_data_points);
        return;
    }
    double scale = d.scale != 0 ? d.scale : h_in.d.scale;
    double shift = d.scale != 0 ? d.shift : h_in.d.shift;
    double max_val = (double)lut.size() - 1;

#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_data_points > BIM_OMP_FOR1)
    for (bim::int64 i = 0; i < num_data_points; ++i) {
        double vpx = ((double)ibuf[i] - shift) * scale;
        unsigned int idx = bim::trim<unsigned int, double>(bim::round<double>(vpx), 0, max_val);
        obuf[i] = bim::trim<To, bim::Lut::StorageType>(lut[idx]);
    }
}

template<typename Ti, typename To>
void Lut::apply_typecast(const Ti *ibuf, To *obuf, const unsigned int &num_data_points) const {
    if (!ibuf || !obuf) return;

#pragma omp parallel for default(shared) BIM_OMP_SCHEDULE if (num_data_points > BIM_OMP_FOR1)
    for (bim::int64 i = 0; i < num_data_points; ++i)
        obuf[i] = (To)ibuf[i];
}

//------------------------------------------------------------------------------------
// ok, follows crazy code bloat of instantiations, will change this eventually
//------------------------------------------------------------------------------------

// this guy instantiates real method based on input template
template<typename Ti>
inline void Lut::do_apply_lut(const Ti *ibuf, const void *obuf, const unsigned int &num_data_points) const {

    if (h_out.dataBpp() == 8 && h_out.dataFormat() != FMT_FLOAT)
        apply_lut<Ti, uint8>(ibuf, (uint8 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 16 && h_out.dataFormat() != FMT_FLOAT)
        apply_lut<Ti, uint16>(ibuf, (uint16 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 32 && h_out.dataFormat() != FMT_FLOAT)
        apply_lut<Ti, uint32>(ibuf, (uint32 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 32 && h_out.dataFormat() == FMT_FLOAT)
        apply_lut<Ti, float32>(ibuf, (float32 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 64 && h_out.dataFormat() == FMT_FLOAT)
        apply_lut<Ti, float64>(ibuf, (float64 *)obuf, num_data_points);
}

// this guy instantiates real method based on input template
template<typename Ti>
inline void Lut::do_apply_lut_scale_from(const Ti *ibuf, const void *obuf, const unsigned int &num_data_points) const {
    if (h_out.dataBpp() == 8 && h_out.dataFormat() != FMT_FLOAT)
        apply_lut_scale_from<Ti, uint8>(ibuf, (uint8 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 16 && h_out.dataFormat() != FMT_FLOAT)
        apply_lut_scale_from<Ti, uint16>(ibuf, (uint16 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 32 && h_out.dataFormat() != FMT_FLOAT)
        apply_lut_scale_from<Ti, uint32>(ibuf, (uint32 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 32 && h_out.dataFormat() == FMT_FLOAT)
        apply_lut_scale_from<Ti, float32>(ibuf, (float32 *)obuf, num_data_points);
    else if (h_out.dataBpp() == 64 && h_out.dataFormat() == FMT_FLOAT)
        apply_lut_scale_from<Ti, float64>(ibuf, (float64 *)obuf, num_data_points);
}

void Lut::apply(void *ibuf, const void *obuf, const unsigned int &num_data_points) const {
    if (lut.size() <= 0) return;

    // uint
    if (h_in.dataBpp() == 8 && h_in.dataFormat() == FMT_UNSIGNED)
        do_apply_lut<uint8>((uint8 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 16 && h_in.dataFormat() == FMT_UNSIGNED)
        do_apply_lut<uint16>((uint16 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 32 && h_in.dataFormat() == FMT_UNSIGNED)
        do_apply_lut_scale_from<uint32>((uint32 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 64 && h_in.dataFormat() == FMT_UNSIGNED)
        do_apply_lut_scale_from<uint64>((uint64 *)ibuf, obuf, num_data_points);
    else // int
        if (h_in.dataBpp() == 8 && h_in.dataFormat() == FMT_SIGNED)
        do_apply_lut_scale_from<int8>((int8 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 16 && h_in.dataFormat() == FMT_SIGNED)
        do_apply_lut_scale_from<int16>((int16 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 32 && h_in.dataFormat() == FMT_SIGNED)
        do_apply_lut_scale_from<int32>((int32 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 64 && h_in.dataFormat() == FMT_SIGNED)
        do_apply_lut_scale_from<int64>((int64 *)ibuf, obuf, num_data_points);
    else
        // float: current implementation would provide poor quality for float2float because of the LUT binning size
        // should look into doing this by applying generator function for each element of the data ignoring LUT at all...
        if (h_in.dataBpp() == 32 && h_in.dataFormat() == FMT_FLOAT)
        do_apply_lut_scale_from<float32>((float32 *)ibuf, obuf, num_data_points);
    else if (h_in.dataBpp() == 64 && h_in.dataFormat() == FMT_FLOAT)
        do_apply_lut_scale_from<float64>((float64 *)ibuf, obuf, num_data_points);
}

void Lut::apply(const Histogram &in, Histogram &out) const {
    if (lut.size() <= 0) return;
    out.clear();

    if (this->type == ltTypecast) {
        out = in;
        return;
    }

    for (unsigned int i = 0; i < in.size(); ++i)
        out.append_value((unsigned int)lut[i], in[i]);
}

//******************************************************************************
// Generators
//******************************************************************************

template<typename Tl>
void bim::linear_full_range_generator(const Histogram &, std::vector<Tl> &lut, LutParameters *args) {
    double out_phys_range = args->out_max - args->out_min;
    double lut_range = lut.size() - 1.0;

    // simple linear mapping for full range
    for (unsigned int x = 0; x < lut.size(); ++x) {
        double vpx = (x * out_phys_range) / lut_range;
        lut[x] = bim::trim<Tl, double>(bim::round<double>(vpx), 0, out_phys_range);
    }
}
template void bim::linear_full_range_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::linear_full_range_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::linear_full_range_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);

template<typename Tl>
void bim::linear_data_range_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args) {
    double out_phys_range = args->out_max - args->out_min;
    // simple linear mapping for actual range
    double b = in.first_pos();
    double e = in.last_pos();
    double range = e - b;
    if (range < 1) range = out_phys_range;
    for (unsigned int x = 0; x < lut.size(); ++x) {
        double vpx = (((double)x) - b) * out_phys_range / range;
        lut[x] = bim::trim<Tl, double>(bim::round<double>(vpx), 0, out_phys_range);
    }
}
template void bim::linear_data_range_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::linear_data_range_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::linear_data_range_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);

template<typename Tl>
void bim::linear_data_tolerance_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args) {
    double out_phys_range = args->out_max - args->out_min;
    double tolerance = 1.0;
    if (args)
        tolerance = args->tolerance;

    // simple linear mapping cutting elements with small appearence
    // get 1% threshold
    unsigned int th = (unsigned int)(in[in.bin_of_max_value()] * tolerance / 100.0);
    double b = 0;
    double e = in.size() - 1;
    for (unsigned int x = 0; x < in.size(); ++x)
        if (in[x] > th) {
            b = x;
            break;
        }
    for (int x = in.size() - 1; x >= 0; --x)
        if (in[x] > th) {
            e = x;
            break;
        }

    double range = e - b;
    if (range < 1) range = out_phys_range;
    for (unsigned int x = 0; x < lut.size(); ++x) {
        double vpx = (((double)x) - b) * out_phys_range / range;
        lut[x] = bim::trim<Tl, double>(bim::round<double>(vpx), 0, out_phys_range);
    }
}
template void bim::linear_data_tolerance_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::linear_data_tolerance_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::linear_data_tolerance_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);

template<typename Tl>
void bim::equalized_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args) {
    double out_phys_range = args->out_max - args->out_min;

    // equalize
    std::vector<double> map(lut.size(), 0);
    map[0] = (double)in[0];
    for (unsigned int x = 1; x < lut.size(); ++x)
        map[x] = map[x - 1] + in[x];

    double div = map[lut.size() - 1] - map[0];
    if (div > 0)
        for (unsigned int x = 0; x < lut.size(); ++x) {
            double vpx = (out_phys_range - 0.0) * ((map[x] - map[0]) / div);
            lut[x] = bim::trim<Tl, double>(bim::round<double>(vpx), 0, out_phys_range);
        }
}
template void bim::equalized_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::equalized_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::equalized_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);

// typecast_generator simply indicates that application of the LUT will produce typecasted output
template<typename Tl>
void bim::typecast_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args) {
    // nothing here really, this is just a place holder for typcasted copy
}
template void bim::typecast_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::typecast_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::typecast_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);

// linear_float01 generates LUT to produce float in the range of 0-1
template<typename Tl>
void bim::linear_float01_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args) {
    double out_phys_range = 1.0;
    double b = in.first_pos();
    double e = in.last_pos();
    double range = e - b;
    if (range < 1) range = out_phys_range;
    for (unsigned int x = 0; x < lut.size(); ++x) {
        double vpx = (((double)x) - b) * out_phys_range / range;
        lut[x] = bim::trim<Tl, double>(bim::round<double>(vpx), 0, out_phys_range);
    }
}
template void bim::linear_float01_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::linear_float01_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::linear_float01_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);


template<typename Tl>
void bim::min_max_gamma_generator(const Histogram &in, std::vector<Tl> &lut, LutParameters *args) {
    double gamma = bim::trim<double, double>(args->gamma, 0.01, 16.0);
    gamma = 1.0 / gamma;
    double out_phys_range = args->out_max - args->out_min;
    double b = pow((double)in.first_pos(), gamma);
    double e = pow((double)in.last_pos(), gamma);
    if (in.dataBpp() <= 16 && in.dataFormat() == bim::FMT_UNSIGNED) {
        b = pow((double)args->in_min, gamma);
        e = pow((double)args->in_max, gamma);
    }
    double range = e - b;
    if (range < 1) range = out_phys_range;
    for (unsigned int x = 0; x < lut.size(); ++x) {
        double vpx = (pow((double)x, gamma) - b) * out_phys_range / range;
        lut[x] = bim::trim<Tl, double>(bim::round<double>(vpx), 0, out_phys_range);
    }
}
template void bim::min_max_gamma_generator<unsigned char>(const Histogram &in, std::vector<unsigned char> &lut, LutParameters *args);
template void bim::min_max_gamma_generator<unsigned int>(const Histogram &in, std::vector<unsigned int> &lut, LutParameters *args);
template void bim::min_max_gamma_generator<double>(const Histogram &in, std::vector<double> &lut, LutParameters *args);


//******************************************************************************
// Lut2D or palette used to map single channel into RGBA
//******************************************************************************

Lut2D::Lut2D() {
}

Lut2D::~Lut2D() {
}

void Lut2D::add_point(const Lut2DPoint &p) {
    this->lut.push_back(p);
}

template<typename T>
void Lut2D::densify(const int &range_from, std::vector<bim::Lut2DColor<T>> *map) const {
    T min_to = bim::lowest<T>();
    double range_to = (double)std::numeric_limits<T>::max() - min_to;
    map->resize(range_from);
    int rng_in = range_from - 1;
    for (int i = 0; i < this->lut.size() - 1; ++i) {
        Lut2DPoint p1 = this->lut[i];
        Lut2DPoint p2 = this->lut[i + 1];
        int i1 = bim::trim<int, double>(bim::round<double>(rng_in * p1.pos), 0, rng_in);
        int i2 = bim::trim<int, double>(bim::round<double>(rng_in * p2.pos), 0, rng_in);
        double n = i2 - i1;
        double r_rng = p2.R - p1.R;
        double g_rng = p2.G - p1.G;
        double b_rng = p2.B - p1.B;
        double a_rng = p2.A - p1.A;
        int p = 0;
        for (int j = i1; j < i2 + 1; ++j) {
            double v = p / n;
            T R = bim::round<T>((v * r_rng + p1.R) * range_to) + min_to;
            T G = bim::round<T>((v * g_rng + p1.G) * range_to) + min_to;
            T B = bim::round<T>((v * b_rng + p1.B) * range_to) + min_to;
            T A = bim::round<T>((v * a_rng + p1.A) * range_to) + min_to;
            map->at(j) = bim::Lut2DColor<T>({ R, G, B, A });
            ++p;
        }
    }
}

template void Lut2D::densify<bim::uint8>(const int &range_from, std::vector<bim::Lut2DColor<bim::uint8>> *map) const;
template void Lut2D::densify<bim::uint16>(const int &range_from, std::vector<bim::Lut2DColor<bim::uint16>> *map) const;

template<typename To>
void Lut2D::interpolate(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<To> &out) const {
}

template void Lut2D::interpolate<bim::uint8>(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<bim::uint8> &out) const;
template void Lut2D::interpolate<bim::uint16>(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<bim::uint16> &out) const;
// the following instantiations probably don't make much sense
//template void Lut2D::interpolate<bim::uint32>(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<bim::uint32> &out) const;
//template void Lut2D::interpolate<bim::uint64>(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<bim::uint64> &out) const;
//template void Lut2D::interpolate<bim::int32>(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<bim::int32> &out) const;
//template void Lut2D::interpolate<bim::int64>(const int &min_from, const int &max_from, const int &range_from, const double &in, Lut2DColor<bim::int64> &out) const;

// MATLAB like LUTs
Lut2D Lut2D::from_name(const std::string &n) {
    xstring name = n;
    if (name.toLowerCase() == "jet") return Lut2D::jet();
    if (name.toLowerCase() == "jet64") return Lut2D::jet64();
    if (name.toLowerCase() == "heatmap") return Lut2D::heatmap();
    if (name.toLowerCase() == "colormap") return Lut2D::colormap();
    if (name.toLowerCase() == "parula") return Lut2D::parula();
    if (name.toLowerCase() == "hsv") return Lut2D::hsv();
    if (name.toLowerCase() == "hot") return Lut2D::hot();
    if (name.toLowerCase() == "cool") return Lut2D::cool();
    if (name.toLowerCase() == "spring") return Lut2D::spring();
    if (name.toLowerCase() == "summer") return Lut2D::summer();
    if (name.toLowerCase() == "autumn") return Lut2D::autumn();
    if (name.toLowerCase() == "winter") return Lut2D::winter();
    if (name.toLowerCase() == "bone") return Lut2D::bone();
    if (name.toLowerCase() == "copper") return Lut2D::copper();
    if (name.toLowerCase() == "pink") return Lut2D::pink();
    if (name.toLowerCase() == "fluorescence") return Lut2D::fluorescence();
    if (name.toLowerCase() == "lsm") return Lut2D::lsm();
    if (name.toLowerCase() == "rainbow") return Lut2D::rainbow();
    if (name.toLowerCase() == "greentored") return Lut2D::greentored();
    if (name.toLowerCase() == "bluetored") return Lut2D::bluetored();
    if (name.toLowerCase() == "gold") return Lut2D::gold();
    if (name.toLowerCase() == "blackbody") return Lut2D::blackbody();
    if (name.toLowerCase() == "endoscopy") return Lut2D::endoscopy();
    if (name.toLowerCase() == "flow") return Lut2D::flow();
    if (name.toLowerCase() == "ired") return Lut2D::ired();
    if (name.toLowerCase() == "perfusion") return Lut2D::perfusion();
    if (name.toLowerCase() == "pet") return Lut2D::pet();
    if (name.toLowerCase() == "ratio") return Lut2D::ratio();
    if (name.toLowerCase() == "spectrum") return Lut2D::spectrum();
    if (name.toLowerCase() == "vr_bones") return Lut2D::vr_bones();
    if (name.toLowerCase() == "vr_muscles_bones") return Lut2D::vr_muscles_bones();
    if (name.toLowerCase() == "vr_red_vessels") return Lut2D::vr_red_vessels();

    if (name.toLowerCase() == "black") return Lut2D::black();
    if (name.toLowerCase() == "gray") return Lut2D::gray();
    if (name.toLowerCase() == "red") return Lut2D::red();
    if (name.toLowerCase() == "green") return Lut2D::green();
    if (name.toLowerCase() == "blue") return Lut2D::blue();
    if (name.toLowerCase() == "yellow") return Lut2D::yellow();
    if (name.toLowerCase() == "magenta") return Lut2D::magenta();
    if (name.toLowerCase() == "cyan") return Lut2D::cyan();

    return Lut2D::black();
    //return Lut2D();
}

Lut2D Lut2D::from_color(const double &r, const double &g, const double &b, const double &a) {
    Lut2D l;
    if (r < 0 || r > 1 || g < 0 || g > 1 || b < 0 || b > 1) return l;
    l.add_point({ 0, 0.0, 0.0, 0.0, a });
    l.add_point({ 1.0, r, g, b, a });
    return l;
}

Lut2D Lut2D::from_color(const std::string &hex_color) { // in this case RGB weights should be in the 0-255 range in html hex notation
    double r = 0;
    double g = 0;
    double b = 0;
    double a = 1.0;
    xstring s = hex_color;

    return Lut2D::from_color(r, g, b, a);
}

Lut2D Lut2D::black() {
    return Lut2D::from_color(0.0, 0.0, 0.0, 1.0);
}

Lut2D Lut2D::gray() {
    return Lut2D::from_color(1.0, 1.0, 1.0, 1.0);
}

Lut2D Lut2D::red() {
    return Lut2D::from_color(1.0, 0.0, 0.0, 1.0);
}

Lut2D Lut2D::green() {
    return Lut2D::from_color(0.0, 1.0, 0.0, 1.0);
}

Lut2D Lut2D::blue() {
    return Lut2D::from_color(0.0, 0.0, 1.0, 1.0);
}

Lut2D Lut2D::yellow() {
    return Lut2D::from_color(1.0, 1.0, 0.0, 1.0);
}

Lut2D Lut2D::magenta() {
    return Lut2D::from_color(1.0, 0.0, 1.0, 1.0);
}

Lut2D Lut2D::cyan() {
    return Lut2D::from_color(0.0, 1.0, 1.0, 1.0);
}

Lut2D Lut2D::heatmap() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.5, 1.0 });
    l.add_point({ 0.13, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 0.38, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 0.63, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.88, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 1.0, 0.5, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::colormap() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.05, 0.0, 0.0, 0.5, 1.0 });
    l.add_point({ 0.13, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 0.38, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 0.63, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.88, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 1.0, 0.5, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::jet() {
    Lut2D l;
    //            POS      R      G     B     A
    l.add_point({ 0.0, 0.0, 0.0, 0.5156, 1.0 });
    l.add_point({ 0.1255, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 0.3765, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 0.6274, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.8784, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 1.0, 0.5, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::jet64() {
    Lut2D l;
    l.add_point({ 0.0, 0.0, 0.0, 0.5625, 1.0 });
    l.add_point({ 0.125, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 0.375, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 0.625, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.875, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 1.0, 0.5, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::parula() {
    Lut2D l;
    l.add_point({ 0, 0.25, 0.15, 0.65, 1.0 });
    l.add_point({ 0.14, 0.28, 0.3, 1.0, 1.0 });
    l.add_point({ 0.24, 0.23, 0.45, 1.0, 1.0 });
    l.add_point({ 0.47, 0.0, 0.72, 0.78, 1.0 });
    l.add_point({ 0.64, 0.4, 0.8, 0.43, 1.0 });
    l.add_point({ 0.77, 0.84, 0.75, 0.16, 1.0 });
    l.add_point({ 0.83, 0.98, 0.73, 0.24, 1.0 });
    l.add_point({ 0.86, 1.0, 0.77, 0.21, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::hsv() {
    Lut2D l;
    l.add_point({ 0, 1.0, 0.0, 0.5, 1.0 });
    l.add_point({ 0.18, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.35, 0.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.51, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 0.68, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 0.84, 1.0, 0.0, 1.0, 1.0 });
    l.add_point({ 1.0, 1.0, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::hot() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.37, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.74, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::cool() {
    Lut2D l;
    l.add_point({ 0, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 1.0, 1.0, 0.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::spring() {
    Lut2D l;
    l.add_point({ 0, 1.0, 0.0, 1.0, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::summer() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.5, 0.4, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 0.4, 1.0 });
    return l;
}

Lut2D Lut2D::autumn() {
    Lut2D l;
    l.add_point({ 0, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::winter() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 1.0, 0.0, 1.0, 0.5, 1.0 });
    return l;
}

Lut2D Lut2D::bone() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.37, 0.32, 0.32, 0.44, 1.0 });
    l.add_point({ 0.74, 0.65, 0.77, 0.77, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::copper() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.8, 1.0, 0.63, 0.4, 1.0 });
    l.add_point({ 1.0, 1.0, 0.78, 0.5, 1.0 });
    return l;
}

Lut2D Lut2D::pink() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.05, 0.27, 0.17, 0.16, 1.0 });
    l.add_point({ 0.11, 0.4, 0.26, 0.25, 1.0 });
    l.add_point({ 0.37, 0.76, 0.5, 0.5, 1.0 });
    l.add_point({ 0.74, 0.9, 0.9, 0.7, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::fluorescence() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.02, 0.0, 0.3, 1.0, 1.0 });
    l.add_point({ 0.14, 0.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.5, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 1.0, 1.0, 0.0, 0.0, 1.0 });
    return l;
}

// Zeiss Zen like LUTs

Lut2D Lut2D::lsm() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.3, 1.0, 1.0 });
    l.add_point({ 0.14, 0.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.5, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 1.0, 1.0, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::rainbow() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.1, 1.0 });
    l.add_point({ 0.10, 0.0, 0.0, 0.5, 1.0 });
    l.add_point({ 0.20, 0.0, 0.5, 1.0, 1.0 });
    l.add_point({ 0.25, 0.0, 1.0, 1.0, 1.0 });
    l.add_point({ 0.55, 1.0, 1.0, 0.0, 1.0 });
    l.add_point({ 0.75, 1.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.9, 1.0, 0.7, 1.0, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::greentored() {
    Lut2D l;
    l.add_point({ 0, 0.0, 1.0, 0.0, 1.0 });
    l.add_point({ 1.0, 1.0, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::bluetored() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 1.0, 1.0 });
    l.add_point({ 1.0, 1.0, 0.0, 0.0, 1.0 });
    return l;
}

Lut2D Lut2D::gold() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.5, 1.0, 0.5, 0.0, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 0.0, 1.0 });
    return l;
}

// Osirix like LUTs

Lut2D Lut2D::blackbody() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.35, 0.9, 0.2, 0.0, 1.0 });
    l.add_point({ 0.70, 1.0, 1.0, 0.4, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::endoscopy() {
    Lut2D l;
    l.add_point({ 0, 0.9, 0.75, 0.75, 1.0 });
    l.add_point({ 1.0, 0.9, 0.5, 0.5, 1.0 });
    return l;
}

Lut2D Lut2D::flow() {
    Lut2D l;
    l.add_point({ 0, 0.36, 0.78, 0.98, 1.0 });
    l.add_point({ 0.24, 0.0, 0.18, 0.96, 1.0 });
    l.add_point({ 0.5, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.76, 0.92, 0.2, 0.14, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::ired() {
    Lut2D l;
    l.add_point({ 0, 1.0, 1.0, 1.0, 1.0 });
    l.add_point({ 1.0, 0.92, 0.2, 0.14, 1.0 });
    return l;
}

Lut2D Lut2D::perfusion() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.2, 0.92, 0.35, 0.16, 1.0 });
    l.add_point({ 0.5, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 1.0, 0.76, 0.98, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::pet() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.5, 0.92, 0.2, 0.14, 1.0 });
    l.add_point({ 0.75, 1.0, 0.96, 0.33, 1.0 });
    l.add_point({ 1.0, 1.0, 1.0, 1.0, 1.0 });
    return l;
}

Lut2D Lut2D::ratio() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.16, 0.96, 1.0 });
    l.add_point({ 0.5, 1.0, 1.0, 1.0, 1.0 });
    l.add_point({ 1.0, 0.92, 0.2, 0.14, 1.0 });
    return l;
}

Lut2D Lut2D::spectrum() {
    Lut2D l;
    l.add_point({ 0, 0.92, 0.26, 0.97, 1.0 });
    l.add_point({ 0.2, 0.0, 0.16, 0.97, 1.0 });
    l.add_point({ 0.4, 0.45, 0.98, 0.99, 1.0 });
    l.add_point({ 0.6, 0.46, 0.98, 0.3, 1.0 });
    l.add_point({ 0.8, 1.0, 0.99, 0.3, 1.0 });
    l.add_point({ 1.0, 0.92, 0.2, 0.14, 1.0 });
    return l;
}

Lut2D Lut2D::vr_bones() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.95, 1.0, 0.96, 0.4, 1.0 });
    l.add_point({ 1.0, 1.0, 0.99, 0.88, 1.0 });
    return l;
}

Lut2D Lut2D::vr_muscles_bones() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.4, 0.93, 0.26, 0.18, 1.0 });
    l.add_point({ 0.9, 1.0, 0.97, 0.65, 1.0 });
    l.add_point({ 1.0, 1.0, 0.99, 0.97, 1.0 });
    return l;
}

Lut2D Lut2D::vr_red_vessels() {
    Lut2D l;
    l.add_point({ 0, 0.0, 0.0, 0.0, 1.0 });
    l.add_point({ 0.8, 0.92, 0.38, 0.25, 1.0 });
    l.add_point({ 1.0, 1.0, 0.95, 0.94, 1.0 });
    return l;
}
