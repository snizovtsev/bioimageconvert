/*******************************************************************************

  Tests for bim::TagMap

  This file is part of BioImageConvert.

  *******************************************************************************/

#include <tag_map.h>

#include <gtest/gtest.h>

TEST(TagMapTest, filterByPrefix) {
    bim::TagMap vTagMap;
    vTagMap.append_tag("NONOME/TestKey01", 1234);
    vTagMap.append_tag("OME/TestKey02", "hello");
    vTagMap.append_tag("OME/TestKey03", "world");
    vTagMap.append_tag("OME/TestKey04", "test");
    vTagMap.append_tag("NoPrefixKey05", 3.14158);

    bim::TagMap vFilteredTagMap = filterByPrefix(&vTagMap, "OME/");
    EXPECT_EQ(vFilteredTagMap.size(), 3);
    EXPECT_EQ(vFilteredTagMap["TestKey02"].as_string(), "hello");
    EXPECT_EQ(vFilteredTagMap["TestKey03"].as_string(), "world");
    EXPECT_EQ(vFilteredTagMap["TestKey04"].as_string(), "test");
}